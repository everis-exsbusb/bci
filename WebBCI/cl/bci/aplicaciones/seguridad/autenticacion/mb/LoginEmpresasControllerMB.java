package cl.bci.aplicaciones.seguridad.autenticacion.mb;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.model.seguridad.Canal;
import wcorp.model.seguridad.SessionBCI;
import wcorp.serv.autenticacion.ServiciosAutenticacion;
import wcorp.serv.bciexpress.BCIExpress;
import wcorp.serv.bciexpress.BCIExpressHome;
import wcorp.serv.bciexpress.BELPerfilUsu;
import wcorp.serv.seguridad.ListaConvenios;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.serv.seguridad.ValidaPinCorp;
import wcorp.util.EnhancedServiceLocator;
import wcorp.util.ErroresUtil;
import wcorp.util.GeneralException;
import wcorp.util.NumerosUtil;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.mensajeria.ControlErrorMsg;

import cl.bci.aplicaciones.cliente.mb.ClienteMB;
import cl.bci.aplicaciones.cliente.mb.UsuarioModelMB;
import cl.bci.aplicaciones.seguridad.autenticacion.utils.SegundaClave;
import cl.bci.infraestructura.utilitarios.error.mb.MensajesErrorUtilityMB;
import cl.bci.infraestructura.web.journal.Journalist;
import cl.bci.infraestructura.web.portal.mb.PortalMB;
import cl.bci.infraestructura.web.seguridad.login.AbstractLoginController;
import cl.bci.infraestructura.web.seguridad.mb.SesionMB;
import cl.bci.infraestructura.web.seguridad.to.CanalTO;
import cl.bci.infraestructura.web.seguridad.to.DatosAutenticacionTO;
import cl.bci.infraestructura.web.seguridad.to.UsuarioConectadoTO;
import cl.bci.infraestructura.web.seguridad.util.ConectorStruts;

/**
 * Support ManagedBean
 * 
 * <pre>
 * <b>Login Empresas</b>
 *
 * Componente encargado de logear un cliente al sitio Empresas. 
 *
 * </pre>
 *
 * Registro de versiones:
 * <ul>
 * <LI>1.0 08/02/2013, Gonzalo Cofre Guzman. (SEnTRA) - Versi�n inicial</li>
 * <li>1.1 26/09/2013, Gonzalo Cofre Guzman (SEnTRA) - Se agrega logica para capturar el error de claveBloqueada.
 * <LI>1.2 25/11/2013, Rodolfo Kafack Ghinelli. (SEnTRA) - se agrega logica para validacion de canal movil en el
 * metodo {@link #validaClaveInternet(DatosAutenticacionTO)}</li>
 * <LI>1.2 27/12/2013, Rodolfo Kafack Ghinelli. (SEnTRA) - Oliver Hidalgo Leal (BCI) - se encapsula logica de
 * seleccionar en el metodo {@link #seleccionConvenvenioEmpresa(ListaConvenios[])} y se modifica metodo
 * {@link #despachoLogin(ListaConvenios[])}</li>
 * <LI>1.3 10/04/2014, Oliver Hidalgo Leal. (BCI) - Se controla excepcion clave incorrecta para movil
 * {@link #validaClaveInternet(DatosAutenticacionTO)}
 * <li>1.4 12/08/2014, Rodolfo Mora (IMAGEMAKER)- Se agrega aributo para diferenciar laas llamadas a servicios
 * desde movil (canalMovil). se modifica {@link #validaClaveInternet(DatosAutenticacionTO)} y
 * {@link #despachoLoginEmpresas(String, String, String, String, String)}
 * <li>1.5 26/02/2015 Eric Mart�nez F. (TINet): Se modifica l�gica para autentificar a un usuario que accede a la 
 * aplicaci�n m�vil de empresas, por medio de la invocaci�n de un servicio REST �nico para empresas y pyme. Se 
 * modifican los m�todos {@link #validaClaveInternet(DatosAutenticacionTO)} y 
 * {@link #despachoLoginEmpresas(String, String, String, String, String)}</li>
 * <li>1.6 02/07/2015 Rodrigo Troncoso Balboa (ImageMaker SA) - Ivan Carvajal (BCI): se agrega m�todo 
 *         {@link #journalizaSeleccionConvenio(long, String, long, String, String, String, String, String)} 
 *         encargado de journalizar seleccion de convenio y 
 *         se modifica {@link #despachoLogin(ListaConvenios[])} que lo llama cuando el cliente posee solo un convenio.</li>
 * <li>1.7 25/09/2015 Eduardo Villagr�n Morales (Imagemaker): se modifica el m�todo
 *      {@link #validaClaveInternet(DatosAutenticacionTO)} para que se valide la clave internet antes de asignar
 *      valores a los objetos de sesi�n y otros.</li>
 * <li>1.8 05/07/2016, Cristian Diaz. (SEnTRA) - Marcelo Avenda�o. (Ing. Soft. BCI):  Se modifica metodo
 * {@link #validaClaveInternet(DatosAutenticacionTO)}</li> 
 * <li>1.9 15/12/2016 Alvaro Fuentes (TINet) - Juan Bustos (Ing. Soft. BCI): Se modifica m�todo
 * {@link #validaClaveInternet(DatosAutenticacionTO)}.</li>
  * <li>2.0 15/12/2016 Test Git (everis): se realiza un test Git.</li>
 * </ul>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 */

@ManagedBean
@RequestScoped
public class LoginEmpresasControllerMB extends AbstractLoginController implements Serializable  {

    /**
     * Atributo serializable.
     */

    private static final long serialVersionUID = 3L;

    /**
     * Atributo que representa el valor del PINID establecido para el canal EMPRESAS.
     */
    private static final int PINID_EMPRESAS        = 130;

    /**
     * Atributo que representa el valor del origen de la consulta.
     */
    private static final String EXTRANET  = "1";

    /**
     * Atributo que representa el valor del evento para Login.
     */
    private static final String EVE_LOGIN = "LOGIN";

    /**
     * Atributo que representa el valor del evento para seleccion convenio.
     */
    private static final String EVE_LOGINCOV = "LOGINCOV";

    /**
     * Atributo que representa el valor del subevento para Login.
     */
    private static final String SUB_COD_OK                = "OK";

    /**
     * Atributo que representa el valor del id producto token.
     */
    private static final String ID_PRODTOKEN              = "TOK";

    /**
     * Largo m�nimo de caracteres para la clave.
     */
    private static final int LARGO_MIN_CLAVE=8;

    /**
     * Cantidad m�nima de letras en la clave.
     */
    private static final int MIN_LETRAS_CLAVE=2;

    /**
     * Atributo que representa el valor del tiempo a considerar.
     */
    private static final int INDICADOR_TIEMPO = 60;

    /**
     * Atributo representa el delta de minutos que se incluye al maximo de expiracion de sesion.
     */
    private static final int DELTA_MINUTOS_EXPIRACION_SESION=3;

    /**
     * Atributo que representa el valor del largo del rut.
     */
    private static final int LARGO_RUT = 8;
    
    /**
     * Atributo que representa la tabla de parametros para filtro de convenios.
     */
    private static final String TABLA_FILTRO_CONVENIOS = "BancaMovil.parametros";
    
    /**
     * String para los salida SIN_CONVENIO.
     */
    private static final String SIN_CONVENIO = "sinConvenio";

    /**
     * Constante que define el c�digo del canal para Movil.
     */
    private boolean canalMovil;

    /**
     * Atributo que representa la clave Multipass.
     */
    private String claveToken;

    /**
     * Atributo que representa la instancia de los servicios de autenticaci�n.
     */
    private ServiciosAutenticacion serviciosAutenticacion;

    /**
     * Atributo que representa el despacho a pagina de autenticacion segunda clave.
     */
    private String autenticaSegundaClave = 
        "/cl/bci/aplicaciones/seguridad/autenticacion/autenticacionSegundaClave.jsf";

    /**
     * Atributo que representa el log de la clase.
     */
    private transient Logger logger = (Logger) Logger.getLogger(this.getClass());

    /**
     * Atributo que representa la instancia del Interface de BCI Express. 
     */
    private BCIExpress bciexpress;

    /**
     * Atributo que representa el canal.
     */
    private String canal;

    /**
     * Atributo que representa el rut del cliente.
     */
    private String rutCliente;

    /**
     * Atributo que representa el digito verificador.
     */
    private char dvCliente;

    /**
     * Atributo que representa la clave del cliente.
     */
    private String clave;

    /**
     * Atributo que representa el servicio inicial de ingreso.
     */
    private String servicioInicial;

    /**
     * Atributo que representa el despacho a realizar.
     */
    private String despacho;

    /**
     * Atributo que representa el rut de la empresa.
     */
    private String rutEmpresa;

    /**
     * Atributo que representa el digito verificador de la empresa.
     */
    private char digitoVerificador;

    /**
     * Atributo que representa el cambio de Clave.
     */
    private String cambiaClave;

    /**
     * Atributo que representa el despacho a enrolamiento.
     */
    private String vistaEnrolamiento = "/cl/bci/aplicaciones/seguridad/autenticacion/vista/vistaEnrolamiento.jsf";

    /**
     * Codigo Error de clave bloqueada.
     */
    private String codClaveBloq = "0432";

    /**
     * Atributo representa Managed Bean inyectado PortalMB.
     */
    @ManagedProperty(value = "#{portalMB}")
    private PortalMB portal;

    /**
     * Atributo representa Managed Bean inyectado SegundaClaveMB.
     */
    @ManagedProperty(value = "#{segundaClaveMB}")
    private SegundaClaveMB segundaClaveMB;

    /**
     * Atributo representa Managed Bean inyectado SesionMB.
     */
    @ManagedProperty(value = "#{sesionMB}")
    private SesionMB sesion;

    /**
     * Atributo representa Managed Bean inyectado ClienteMB.
     */
    @ManagedProperty(value = "#{clienteMB}")
    private ClienteMB clienteMB;

    /**
     * Atributo representa Managed Bean inyectado UsuarioModelMB.
     */
    @ManagedProperty(value = "#{usuarioModelMB}")
    private UsuarioModelMB usuarioModelMB;

    /**
     * Atributo que representa la Inyecci�n de Managed Bean MensajesErrorUtilityMB.
     */
    @ManagedProperty(value = "#{mensajesErrorUtilityMB}")
    private MensajesErrorUtilityMB mensajes;

    /**
     * Atributo que representa la Inyecci�n de Managed Bean EnrolamientoClaveUtilityMB.
     */
    @ManagedProperty(value = "#{enrolamientoClaveUtilityMB}")
    private EnrolamientoClaveUtilityMB enrolamiento;

    /**
     * M�todo que realiza la validaci�n de la clave internet del cliente.
     * <p>
     * Registro de versiones:
     * <ul>
     * <LI>1.0 08/02/2013, Gonzalo Cofre Guzman. (SEnTRA) - Versi�n inicial
     * <LI>1.1 19/03/2013, Gonzalo Cofre Guzman. (SEnTRA) - Se modifica la firma por el m�todo heredado de la clase
     * AbstractLoginController, debido a la nueva estructura.
     * <LI>1.2 25/11/2013, Rodolfo Kafack Ghinelli. (SEnTRA) - Se setea getMensajes().setErrorAplicativo(true) para
     * validaciones de la aplicacion Movil empresas.
     * <LI>1.3 13/01/2014, Oliver Hidalgo Leal. (BCI) - Se setea c�digo canal sin definci�n y se controla.
     * <LI>1.4 10/04/2014, Oliver Hidalgo Leal. (BCI) - Se setea c�digo error clave incorrecta en mensaje utility.
     * <li>1.5 08/08/2014 Rodolfo Mora (IMAGEMAKER) - validaci�n para el canal m�vil empresas, en cuyo caso, se
     * evita realizar el redireccionamiento al servlet wcorp.servletcorp.seguridad.Login, retornando inmediatamente
     * el String sinConvenio.
     * <li>1.6 26/02/2015 Eric Mart�nez F. (TINet): Se agrega filtro de convenios, solo para m�viles, para que
     * solo se consideren convenios con marca empresa y rol usuario distinto a supervisor. Se elimina mensaje para
     * m�viles en caso de llegar a un error de SeguridadException.
     * <li>1.7 25/09/2015 Eduardo Villagr�n Morales (Imagemaker): Se modifica para verificar clave antes de
     *          asingar valores a objetos.</li>
     * <li>1.8 05/07/2016, Cristian Diaz. (SEnTRA) - Marcelo Avenda�o. (Ing. Soft. BCI):  Se setean datos basicos para 
     * clientes sin convenios (clienteMB.setDatosBasicos())</li> 
     * <li>1.9 15/12/2016 Alvaro Fuentes (TINet) - Juan Bustos (Ing. Soft. BCI): Se modifica el m�todo para 
     * alterar orden de las validaciones en dispositivos m�viles.</li>
     * </ul>
     * <p>
     * 
     * @param datosAutenticacion Transfer Object con los datos de autenticaci�n.
     * @return String con URL de despacho, tras proceso de autenticaci�n.
     * @since 1.0
     */
    public String validaClaveInternet(DatosAutenticacionTO datosAutenticacion) {

        if (logger.isEnabledFor(Level.INFO)) {
            logger.info("[validaClaveInternet] [" + datosAutenticacion.getRut() + "][BCI_INI] inicio dv["
                + datosAutenticacion.getDv() + "]");
        }
        this.rutCliente = String.valueOf(datosAutenticacion.getRut());
        this.dvCliente = datosAutenticacion.getDv();
        this.canal = datosAutenticacion.getCanal();
        this.clave = datosAutenticacion.getClave();
        this.servicioInicial = datosAutenticacion.getServicioInicial();
        if (debeCambiarClave(datosAutenticacion.getRut(), datosAutenticacion.getClave())){
            cambiaClave="S";
        }
        else{
            cambiaClave="N";
        }
        this.setCambiaClave(cambiaClave);
        String userIdrut    = null;
        String userIdAux    = null;
        String pin          = null;
        Canal canalMB       = null; 
        CanalTO canalCliente = null;
        SessionBCI sessionBCI = null;
        try{
            this.registraLogIngreso(rutCliente, canal, servicioInicial);
            canalCliente = this.iniciaInfraestructuraCanal(canal);
            if(getLogger().isEnabledFor(Level.DEBUG)){
                getLogger().debug(
                    "[validaClaveInternet][" + this.rutCliente + "] canal: " + this.canal + " PinID: "
                        + canalCliente.getPinID() + " GrupoCanal: " + canalCliente.getGrupoCanal());
            }
            sesion.setIdServicio(servicioInicial);
            int pid = Integer.parseInt(canalCliente.getPinID());
            try{
                serviciosAutenticacion = createEJB();
            }
            catch(Exception e){
                if(getLogger().isEnabledFor(Level.ERROR)){
                    getLogger().error(
                        "[validaClaveInternet][" + this.rutCliente + "] Create EJB [" + obtenerRutUsuario()
                            +"] Error al crear Ejb, " + e.getMessage());
                }
                this.getMensajes().setMensajeError(TablaValores.getValor("errores.codigos", "BADMSG", "Desc"));
                this.getMensajes().setErrorAplicativo(true); 
                return this.getMensajes().obtenerPaginaError();
            }
            userIdrut = rutCliente;
            userIdAux = String.valueOf(dvCliente);
            pin = clave;

            userIdrut=userIdrut!=null?userIdrut.trim():userIdrut;
            userIdAux=userIdAux!=null?userIdAux.trim():userIdAux;
            validaRut(userIdrut, userIdAux);
            long userRUT = (new Long(userIdrut)).longValue();
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[validaClaveInternet] pid [" + pid + "]");
            }
            if(pid == PINID_EMPRESAS){
                sessionBCI = this.iniciaSesionBciPersonas(canal, userIdrut, userIdAux);
                if (getLogger().isEnabledFor(Level.DEBUG)) {
                    getLogger().debug(
                        "[validaClaveInternet][" + this.rutCliente + "] sessionBCI [" + sessionBCI + "]");
                }
                ConectorStruts.setAttribHttp("sessionBci", sessionBCI);
                this.registraServicioInicial(servicioInicial);


                ListaConvenios[] listaConvenios = serviciosAutenticacion.obtenerConveniosEmpresas(userRUT,
                    EXTRANET);
                
                Boolean filtroConvenio = Boolean.valueOf(TablaValores.getValor(TABLA_FILTRO_CONVENIOS,
                    "filtroConveniosLogin", datosAutenticacion.getCanal()));
                
                if (getLogger().isEnabledFor(Level.INFO)) {
                    getLogger().info("[validaClaveInternet] Filtro convenios m�viles: " + filtroConvenio);
                }

                if (filtroConvenio) {
                    listaConvenios = filtraListaConveniosEmpresa(listaConvenios);
                }
                if (listaConvenios == null && sesion.getCanalTO().getDispositivo().equals("browsermovil")) {
                    listaConvenios = new ListaConvenios[0];
                }
                if (getLogger().isDebugEnabled()){							
                    getLogger().debug( "[validaClaveInternet] lista convenios"+listaConvenios);
                }
                usuarioModelMB.setListaConvenios(listaConvenios);
                if ((listaConvenios == null || listaConvenios.length == 0) &&
                    !sesion.getCanalTO().getDispositivo().equals("browsermovil")) {
                    clienteMB.setDatosBasicos();
                    if (getLogger().isEnabledFor(Level.INFO)) {
                        getLogger().info(
                            "[validaClaveInternet] [" + userIdrut
                                + "] [SPK_FINOK] No posee Convenios asociados --> Banco en L�nea.");
                    }
                    String indCambClave = String.valueOf(sessionBCI.indCambioClave);
                    return this.despachoLoginEmpresas(canal, pin, userIdrut, userIdAux, indCambClave);                   
                }
                canalMB = new Canal(canal);
                if (getLogger().isDebugEnabled()){
                    getLogger().debug(
                        "[validaClaveInternet][" + this.rutCliente + "] getPinID()[" + canalCliente.getPinID()
                            + "] userIdrut[" + userIdrut + "] userIdAux[" + userIdAux.charAt(0) + "]");
                }

                ValidaPinCorp vp = this.validaPinCorporativo(canalCliente, Long.parseLong(userIdrut), userIdAux,
                    pin);
                clienteMB.setDatosBasicos();

                UsuarioConectadoTO usuario = new UsuarioConectadoTO();
                usuario.setIdUsuarioConectado(userIdrut + userIdAux);
                sesion.setUsuarioConectado(usuario);
                if (vp == null ){
                    if(getLogger().isEnabledFor(Level.INFO)){ 
                        getLogger().info(
                            "[validaClaveInternet] [" + userIdrut + "] [SPK_FINOK] ValidaPinCorp nulo ["
                                + despacho + "]");
                    }
                    return despacho;
                }
                seteaEnSessionBCI(vp);
                int tu = canalMB.getTimeOutUso();
                if (getLogger().isEnabledFor(Level.DEBUG)) {
                    getLogger().debug("[validaClaveInternet][" + this.rutCliente + "] tu: " + tu);
                }
                if (tu > 0){
                    ConectorStruts.setMaxInactiveInterval((tu + DELTA_MINUTOS_EXPIRACION_SESION)
                        * INDICADOR_TIEMPO);
                }
                ConectorStruts.setAttribHttp("sessionBci", sessionBCI);
                ConectorStruts.setAttribBCI("listaConvenios", listaConvenios);
                ConectorStruts.setAttribBCI("claveWLS", pin); 
                ConectorStruts.setAttribBCI("convWLS" , canal);

                if (getLogger().isEnabledFor(Level.DEBUG)) {
                    getLogger().debug(
                        "[validaClaveInternet][" + this.rutCliente + "] primeraVez[" + sessionBCI.primeraVez
                            + "] indCambioClave[" + sessionBCI.indCambioClave
                            + "]largo de la lista de convenios [" + listaConvenios.length + "] getCambiaClave["
                            + getCambiaClave()+ "]");
                }
                if (this.despachaCambiarClave(vp, pin)){
                    if (getLogger().isEnabledFor(Level.INFO)) {
                        getLogger().info(
                            "[validaClaveInternet] [" + userIdrut + "] [SPK_FINOK] debe Cambiar clave");
                    }
                    return despacho;
                }
                String despachoLogin = despachoLogin(listaConvenios);
                if (this.despachaValidacionSegundaClave(userIdrut, userIdAux, canalCliente)){
                    if(getLogger().isEnabledFor(Level.INFO)){ 
                        getLogger().info(
                            "[validaClaveInternet] [" + obtenerRutUsuario()
                                + "] [SPK_FINOK] despacha a validacion Segunda clave [" + despacho +"]"); 
                    }
                    this.getSegundaClaveMB().setDespachoDespuesValidar(despachoLogin);
                    return despacho;
                }
                if (getLogger().isEnabledFor(Level.INFO)) {
                    getLogger().info(
                        "[validaClaveInternet] [" + this.rutCliente + "] [SPK_FINOK] NO debe validar Token");
                }
                sesion.activar();
                return despachoLogin;

            }
            else{
                if (getLogger().isEnabledFor(Level.ERROR)) {
                    getLogger().error(
                        "[validaClaveInternet] [" + this.rutCliente
                            + "] [BCI_FINEX][GeneralException] Canal sin definici�n");
                }
                throw new wcorp.util.GeneralException("0435","Canal sin definici�n");
            }
        }
        catch(wcorp.serv.clientes.ClientesException ex){
            getLogger().error(
                "[validaClaveInternet] [" + this.rutCliente
                    + "] [BCI_FINEX][controlLoginClientesException] Error " + ex.getMessage(), ex);
            return this.controlLoginClientesException(ex);
        }
        catch(wcorp.serv.seguridad.SeguridadException ex){ 
            getLogger().error(
                "[validaClaveInternet] [" + this.rutCliente + "] [BCI_FINEX][SeguridadException] Error "
                    + ex.getMessage(), ex);

            ex = (SeguridadException) ControlErrorMsg.modificaCodigoError((Exception) ex,
                canalCliente.getCanalID());
            if (ConectorStruts.getSessionBCI() != null){
                if((codClaveBloq).equals(ex.getCodigo())){
                    this.getMensajes().setInformacionAdicional("ClaveBloqueada");
                    this.getMensajes().setErrorSecundario("");
                    this.getMensajes().setPaginaRetorno(
                        TablaValores.getValor("DesbloqueoClaveInternet.parametros", "urlDesbloqueoClave-"
                            +this.getSesion().getCanalId(), "url"));
                }
            }
            return this.controlLoginSeguridadException(ex);
        }
        catch(wcorp.util.GeneralException ex){
            ex = (GeneralException) ControlErrorMsg.modificaCodigoError((Exception) ex, canalCliente.getCanalID());
            String retorno = this.controlLoginGeneralException(ex);
            getLogger().error(
                "[validaClaveInternet] [" + this.rutCliente + "] [BCI_FINEX][GeneralException] Error "
                    + ex.getMessage(), ex);

            this.getMensajes().setInformacionAdicional(ex.getCodigo());
            return retorno;
        }
        catch (Exception ex){
            getLogger().error(
                "[validaClaveInternet] [" + this.rutCliente + "] [BCI_FINEX][Exception] Error " + ex.getMessage(),
                ex);
            ex = ControlErrorMsg.modificaCodigoError(ex, canalCliente.getCanalID());
            return this.controlLoginException(ex);
        }
    }

    /**
     * M�todo que valida el despacho de Login, luego de haber ingresado credenciales, esto segun la cantidad de
     * empresas dependiendo de la marca pyme o empresas.
     * <p>
     * Registro de versiones:
     * <ul>
     * <LI>1.0 09/04/2012, Gonzalo Cofre Guzman. (SEnTRA) - Versi�n inicial.
     * <LI>1.1 18/10/2012 H�ctor Hern�ndez Orrego (Sentra): Se corrige llamada a antiguo m�todo obtenerEmpresasPyme
     * en EJB, por cambio de firma en �ste, el nuevo nombre del m�todo es obtenerConveniosEmpresas. Se modifica
     * visibilidad de m�todo de public a private, para acotar su seguimiento.</li>
     * <LI>1.2 04/02/2013, Gonzalo Cofre Guzman. (SEnTRA) - Se agrega un parametro al m�todo y se ordena c�digo.
     * <LI>1.3 27/12/2013, Rodolfo Kafack Ghinelli. (SEnTRA) - Oliver Hidalgo Leal (BCI) - Se modifica el
     * condicional cuando tiene un solo convenio.
     * <li>1.4 02/07/2015, Rodrigo Troncoso Balboa (ImageMaker SA) - Ivan Carvajal (BCI) - Se agrega llamado a 
     *         journalizarSeleccionConvenio cuando tiene un solo convenio.</li>
     * </ul>
     * <p>
     * 
     * @param listaConvenios a utilizar.
     * @return vista a la cual redirecciona.
     * @throws Exception exception
     * @since 1.5
     */
    private String despachoLogin(ListaConvenios[] listaConvenios) throws Exception {
        if (getLogger().isEnabledFor(Level.DEBUG)) {
            getLogger().debug("[despachoLogin] entrando a despacho Login");
        }
        SessionBCI sessionBCI = ConectorStruts.getSessionBCI();
        String userIdrut = rutCliente;
        String pin = clave;
        String userIdAux = String.valueOf(dvCliente);
        long userRUT = (new Long(userIdrut)).longValue();
        if (getLogger().isEnabledFor(Level.DEBUG)) {
            getLogger().debug("[despachoLogin] listaConvenios = " + listaConvenios);
        }
        CanalTO canalCliente = null;
        try {
            canalCliente = (CanalTO) portal.getCanalPersonalizado();
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error(
                    "[despachoLogin] rut[" + rutCliente + "] Canal invalido, al instanciar Canal(" + canal + "): "
                        + e.getMessage());
            }
            throw new wcorp.util.GeneralException("ESPECIAL", "Canal inv�lido");
        }
        if (listaConvenios == null || listaConvenios.length == 0) {
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("validaClaveInternet:" + "No posee Convenios asociados --> Banco en L�nea");
            }
            String indCambClave = String.valueOf(sessionBCI.indCambioClave);
            return this.despachoLoginEmpresas(canal, pin, userIdrut, userIdAux, indCambClave);
        }
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[despachoLogin] listaConvenios.length [" + listaConvenios.length + "]");
        }
        if (listaConvenios.length == 1) {

            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[despachoLogin] Selecciona convenio ");
            }

            String retorno = seleccionConvenvenioEmpresa(listaConvenios);

            try {
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[validaClaveInternet] this.getRutEmpresa()[" + rutCliente + "]");
                    getLogger().debug("[validaClaveInternet] getDigitoVerificador()[" + dvCliente + "]");
                    getLogger().debug("[validaClaveInternet] userIdrut[" + userIdrut + "]");
                    getLogger().debug("[validaClaveInternet] userIdAux[" + userIdAux + "]");
                    getLogger().debug(
                        "[validaClaveInternet] canalCliente.getNombre()[" + canalCliente.getNombre() + "]");
                    getLogger().debug(
                        "[validaClaveInternet] ConectorStruts.getRemoteAddr()[" + ConectorStruts.getRemoteAddr()
                            + "]");
                }
                journalizaLogin(Long.parseLong(rutCliente), String.valueOf(dvCliente), Long.parseLong(userIdrut),
                        userIdAux, SUB_COD_OK, EVE_LOGIN, canalCliente.getNombre(), ConectorStruts.getRemoteAddr());

                journalizaSeleccionConvenio(Long.parseLong(rutCliente), String.valueOf(dvCliente), Long.parseLong(userIdrut),
                        userIdAux, SUB_COD_OK, EVE_LOGINCOV, canalCliente.getNombre(), ConectorStruts.getRemoteAddr());
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[validaClaveInternet][" + rutCliente + "] Journalizacion OK");
                }
            }
            catch (Exception e) {
                if (getLogger().isEnabledFor(Level.WARN)) {
                    getLogger().warn("[" + rutCliente + "] No Journalizo [" + e + "]");
                }
            }

            return retorno;

        }
        else {
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[despachoLogin] Posee Mas de un convenio");
            }
            usuarioModelMB.setListaConvenios(listaConvenios);
            usuarioModelMB.setNombres(listaConvenios[0].getNombre().trim());
            usuarioModelMB.setApPaterno(listaConvenios[0].getApellidoPaterno().trim());
            usuarioModelMB.setApMaterno(listaConvenios[0].getApellidoMaterno().trim());
            usuarioModelMB.setRut(Long.valueOf(rutCliente));
            usuarioModelMB.setDigitoVerif(dvCliente);

            clienteMB.setRut(Long.valueOf(rutCliente));
            clienteMB.setDigitoVerif(dvCliente);

            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug("[validaClaveInternet] Sesion activada");
            }
            this.getEnrolamiento().comprobacionInicialEnrolamiento();

            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug(
                    "[despachoLogin] this.getEnrolamiento().getEnrola().getEstadoEnrolamiento() :: "
                        + this.getEnrolamiento().getEnrola().getEstadoEnrolamiento());
                getLogger().debug(
                    "[despachoLogin] this.getEnrolamiento().getEnrola().getFechaEnrolamiento() :: "
                        + this.getEnrolamiento().getEnrola().getFechaEnrolamiento());
            }
            try {
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[validaClaveInternet] this.getRutEmpresa()[" + rutCliente + "]");
                    getLogger().debug("[validaClaveInternet] getDigitoVerificador()[" + dvCliente + "]");
                    getLogger().debug("[validaClaveInternet] userIdrut[" + userIdrut + "]");
                    getLogger().debug("[validaClaveInternet] userIdAux[" + userIdAux + "]");
                    getLogger().debug(
                        "[validaClaveInternet] canalCliente.getNombre()[" + canalCliente.getNombre() + "]");
                    getLogger().debug(
                        "[validaClaveInternet] ConectorStruts.getRemoteAddr()[" + ConectorStruts.getRemoteAddr()
                            + "]");
                }
                journalizaLogin(Long.parseLong(rutCliente), String.valueOf(dvCliente), Long.parseLong(userIdrut),
                    userIdAux, SUB_COD_OK, EVE_LOGIN, canalCliente.getNombre(), ConectorStruts.getRemoteAddr());
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[validaClaveInternet][" + rutCliente + "] Journalizacion OK");
                }

            }
            catch (Exception e) {
                if (getLogger().isEnabledFor(Level.WARN)) {
                    getLogger().warn("[" + rutCliente + "] No Journalizo [" + e + "]");
                }
            }
            if (this.getEnrolamiento().getEnrola().getEstadoEnrolamiento() == 1
                && this.getEnrolamiento().getEnrola().getFechaEnrolamiento() == null) {
                if (getLogger().isEnabledFor(Level.DEBUG)) {
                    getLogger().debug(
                        "[despachoLogin] Requiere Enrolamiento :: "
                            + this.getEnrolamiento().getEnrola().getEstadoEnrolamiento());
                }
                return vistaEnrolamiento;
            }

            return seleccionConvenio();
        }
    }

    /**
     * M�todo que obtiene los perfiles de un usuario.
     * <p>
     * Registro de versiones:
     * <ul>
     * <LI>1.0 14/02/2012, Sergio Cuevas Diaz. (SEnTRA) - Versi�n inicial
     * <LI>1.1 04/02/2013, Gonzalo Cofre Guzman. (SEnTRA) - Se elimina c�digo redundante.
     * </ul>
     * <p>
     * 
     * @since 1.0
     * @param rutUsuario atributo que representa el rut del usuario.
     * @param identificadorConvenio atributo que representa el id del convenio.
     * @param rutEmpresaCliente atributo que representa el rut de la empresa.
     * @throws Exception en caso de error.
     */
    private void perfilUsuario(String rutUsuario, String identificadorConvenio, String rutEmpresaCliente)
        throws Exception {
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[PerfilUsuario] entrando a PerfilUsuario");
        }
        HttpSession ses = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        EnhancedServiceLocator locatorSeguridad = EnhancedServiceLocator.getInstance();
        BCIExpressHome bciExpressHome = (BCIExpressHome) locatorSeguridad.getHome(
            "wcorp.serv.bciexpress.BCIExpress", BCIExpressHome.class);
        bciexpress = bciExpressHome.create();
        BELPerfilUsu tipoConv = bciexpress.consultaBELPerfil(rutUsuario, identificadorConvenio, rutEmpresaCliente)
            .getBELPerfilUsu()[0];
        getLogger().debug("[PerfilUsuario] 6");
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[PerfilUsuario] Tipo Usuario : " + tipoConv.getTipoUsuario());
            getLogger().debug("[PerfilUsuario] Perfil Usuario : " + tipoConv.getPerfil());
        }
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[PerfilUsuario] despues del if");
        }
        ses.setAttribute("perfil", tipoConv.getPerfil());
        ses.setAttribute("tipoUsuario", tipoConv.getTipoUsuario());
        usuarioModelMB.setPerfil(tipoConv.getPerfil());
        usuarioModelMB.setTipoUsuario(tipoConv.getTipoUsuario());
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[PerfilUsuario] fin del metodo");
        }
    }

    /**
     * <p>
     * M�todo redirecciona a servlet de Login Empresas. Setea parametros en request que son verificados y
     * rescatados en servlet de Login. Se utiliza para aquellos clientes qeu no tienen empresas pyme pero utilizan
     * el login de pyme
     * </p>
     * Registro de versiones:
     * <ul>
     * <LI>1.0 22/03/2012, H�ctor Hern�ndez Orrego. (SEnTRA) -Versi�n inicial
     * <LI>1.1 29/06/2012, H�ctor Hern�ndez Orrego. (SEnTRA) -Se a�ade attribute para identificar redireccion a
     * servlet de Login.
     * <li>1.2 12/08/2014 Rodolfo Mora (IMAGEMAKER)- se valida canal Movil .
     * <li>1.3 26/02/2015 Eric Mart�nez F. (TINet): Se modifica validaci�n de canal M�vil.
     * </ul>
     * 
     * @param canalUsu canal por donde ingreso el usuario.
     * @param claveUsu clave del usuario, necesaria para loguearse en servlet de empresas.
     * @param userIdrut rut del usuario.
     * @param userIdAux DV del usuario.
     * @param indCambiaClave parametro indica si el cliente debe cambiar su clave.
     * @return Solo retorna string para despachar.
     * @since 1.1
     */
    private String despachoLoginEmpresas(String canalUsu, String claveUsu, String userIdrut, String userIdAux,
        String indCambiaClave) {
        try {
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[despachoLoginEmpresas]::inicio");
                getLogger().debug("[despachoLoginEmpresas]::canal [" + canal + "]");
                getLogger().debug("[despachoLoginEmpresas]::userIdrut [" + userIdrut + "]");
                getLogger().debug("[despachoLoginEmpresas]::userIdAux [" + userIdAux + "]");
                getLogger().debug("[despachoLoginEmpresas]::indCambiaClave [" + indCambiaClave + "]");
            }          
          
            if(sesion.getCanalTO().getDispositivo().equals("browsermovil")){
                return SIN_CONVENIO;
            }
            FacesContext ctx = FacesContext.getCurrentInstance();
            ExternalContext ectx = ctx.getExternalContext();
            HttpServletRequest request = (HttpServletRequest) ectx.getRequest();
            HttpServletResponse response = (HttpServletResponse) ectx.getResponse();
            RequestDispatcher dispatcher = request.getRequestDispatcher("/seguridadwls/Login");
            request.setAttribute("redirectDesdeOtroLogin", true);
            request.setAttribute("canal", canal);
            request.setAttribute("clave", claveUsu);
            request.setAttribute("rut", userIdrut);
            request.setAttribute("dig", userIdAux);
            request.setAttribute("cambiaclave", indCambiaClave);
            dispatcher.forward(request, response);
            ctx.responseComplete();
            return "";
        }
        catch (Exception ex) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[despachoLoginEmpresas] error", ex);
            }
        }
        this.getMensajes().setMensajeError(TablaValores.getValor("errores.codigos", "6001", "Desc"));
        return this.getMensajes().obtenerPaginaError();
    }

    /**
     * M�todo retorna String con rut completo del usuario.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 05/02/2013, Gonzalo Cofre Guzman. (SEnTRA) - Versi�n Inicial</li>
     * </ul>
     * </p>
     * 
     * @return String con rut.
     * @since 1.0
     */
    protected String obtenerRutUsuario() {
        return rutCliente;
    }

    /**
     * M�todo valida si cliente debe validar segunda clave y define el despacho si es necesario. M�todo se define
     * como abstracto dado que logica requiere acceso a componentes WebBCI* y desde esta clase de infraestructura
     * esto no se puede realizar.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 05/02/2013, Gonzalo Cofre Guzman. (SEnTRA) - Versi�n Inicial</li>
     * </ul>
     * </p>
     * 
     * @param userIdrut rut usuario
     * @param userIdAux dv usuario
     * @param canalCliente canal de ingreso del usuario
     * @return boolean true, si usuario debe validar segunda clave.
     * @throws SeguridadException excepcion en caso de error de seguridad.
     * @throws Exception en caso de error.
     * @since 1.0
     */
    protected boolean despachaValidacionSegundaClave(String userIdrut, String userIdAux, CanalTO canalCliente)
        throws SeguridadException, Exception {
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[despachaValidacionSegundaClave] Inicio");
        }
        try {
            SegundaClave segundaClave = new SegundaClave();
            Hashtable metodosAutenticacion = segundaClave.asignaMetodosAutenticacion(Long.parseLong(userIdrut),
                userIdAux.charAt(0), canalCliente.getCanalID());

            if (getLogger().isDebugEnabled()) {
                getLogger().debug(
                    "[despachaValidacionSegundaClave]" + "metodosAutenticacion" + metodosAutenticacion);
            }
            ConectorStruts.getSessionBCI().setMetodosAutenticacion(metodosAutenticacion);
            String canalValidaTokenLogin = TablaValores.getValor("Multipass.parametros", "validaTokenLogin",
                canalCliente.getCanalID());
            boolean debeValidarToken = (canalValidaTokenLogin != null && canalValidaTokenLogin.equals("true"));
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[despachaValidacionSegundaClave]validaTok[" + debeValidarToken + "]");
            }
            if (debeValidarToken) {

                boolean respuestaDebeValidarToken = segundaClave.debeValidarToken(rutCliente,
                    canalCliente.getCanalID(), metodosAutenticacion);
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("respuestaDebeValidarToken :: " + respuestaDebeValidarToken);
                    getLogger().debug(
                        "permitirAutentificacion" + ConectorStruts.getAttribBCI("permitirAutentificacion"));
                }
                if (respuestaDebeValidarToken && (ConectorStruts.getAttribBCI("permitirAutentificacion") != null)) {
                    if (getLogger().isDebugEnabled()) {
                        getLogger().debug("[validaClaveInternet]Debe validar Token");
                    }
                    this.setDespacho(autenticaSegundaClave);
                    return true;
                }
            }
            else {
                if (metodosAutenticacion.get(ID_PRODTOKEN) != null) {
                    if (getLogger().isDebugEnabled()) {
                        getLogger().debug("despachaValidacionSegundaClave:: Cambiando Estado Token");
                    }
                    metodosAutenticacion.remove(ID_PRODTOKEN);
                    metodosAutenticacion.put(ID_PRODTOKEN, new Boolean(true));
                    ConectorStruts.getSessionBCI().setMetodosAutenticacion(metodosAutenticacion);
                }

            }
            return false;
        }
        catch (SeguridadException ex) {
            throw ex;
        }
        catch (Exception e) {
            throw e;
        }
    }

    /**
     * M�todo verifica si se debe cambiar la clave con la que ingreso el usuario al sitio. Evalua rut y clave
     * entregados por parametros y verifica si la clave cumple con las condiciones o reglas para ser valida en BCI
     * Empresas.
     * <P>
     * Registro de versiones:
     * <UL>
     * <LI>1.0 06-02-2013 H�ctor Hern�ndez Orrego. (SEnTRA): versi�n inicial.</LI>
     * </UL>
     * <P>
     * 
     * @param rut rut.
     * @param claveIngresada clave ingresada.
     * @return boolean false Si clave paso revisi�n y no debe cambiarse; true si clave no paso alguna revision y
     *         usuario debe cambiarla.
     * @since 1.1
     */
    private boolean debeCambiarClave(long rut, String claveIngresada) {

        final int num2 = 2;
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[debeCambiarClave] Inicio");
        }
        String[] abecedario = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P",
            "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        String[] numeros = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        if (claveIngresada.length() < LARGO_MIN_CLAVE) {
            if (getLogger().isDebugEnabled()) {
                getLogger().debug(
                    "[debeCambiarClave]Largo de clave bajo el minimo permitido de" + LARGO_MIN_CLAVE
                        + " caracteres");
            }
            return true;
        }
        if (claveIngresada.equals(String.valueOf(rut))) {
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[debeCambiarClave]clave debe ser distinta de rut");
            }
            return true;
        }
        if (StringUtil.cuentaOcurrencias(claveIngresada, String.valueOf(rut)) > 0) {
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[debeCambiarClave]clave no puede ser una parte del rut");
            }
            return true;
        }
        if (StringUtil.cuentaOcurrencias(" ", claveIngresada) > 0) {
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[debeCambiarClave]clave no puede tener espacios en blanco");
            }
            return true;
        }
        boolean poseeCaracteresNoAlfanumericos = false;
        for (int i = 0; i < claveIngresada.length(); i++) {
            String caracterClave = String.valueOf(claveIngresada.charAt(i)).toUpperCase();
            if (!StringUtil.estaContenidoEn(caracterClave, abecedario)
                && !StringUtil.estaContenidoEn(caracterClave, numeros)) {
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[debeCambiarClave] caracter No alfanumerico[" + caracterClave + "]");
                }
                poseeCaracteresNoAlfanumericos = true;
                break;
            }
        }
        if (poseeCaracteresNoAlfanumericos) {
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[debeCambiarClave]Clave solo puede tener caracteres" + " alfanumericos");
            }
            return true;
        }
        int cantLetras = 0;
        for (int i = 0; i < claveIngresada.length(); i++) {
            String caracter = String.valueOf(claveIngresada.charAt(i));
            if (StringUtil.estaContenidoEn(caracter.toUpperCase(), abecedario))
                cantLetras = cantLetras + 1;
        }
        if (cantLetras < MIN_LETRAS_CLAVE) {
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[debeCambiarClave] Clave debe tener minimo " + MIN_LETRAS_CLAVE + " letras");
            }
            return true;
        }
        for (int i = 0; i < claveIngresada.length() - num2; i++) {
            char prim = claveIngresada.charAt(i);
            char segu = claveIngresada.charAt(i + 1);
            char terc = claveIngresada.charAt(i + num2);
            if (prim == segu && segu == terc) {
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[debeCambiarClave] clave tiene caracteres repetidos");
                }
                return true;
            }
        }
        int valDif = 0;
        int cantCaracConsecutivos = 0;
        int cantNumConsecutivos = 0;
        for (int i = 0; i < claveIngresada.length() - num2; i++) {
            char valPri = claveIngresada.charAt(i);
            char valSeg = claveIngresada.charAt(i + 1);
            char valTer = claveIngresada.charAt(i + num2);

            if ((StringUtil.estaContenidoEn(String.valueOf(valPri).toUpperCase(), abecedario))
                && (StringUtil.estaContenidoEn(String.valueOf(valSeg).toUpperCase(), abecedario))) {
                valDif = valPri - valSeg;
                if (valDif == 1 || valDif == -1)
                    cantCaracConsecutivos++;

                valDif = valSeg - valTer;
                if (valDif == 1 || valDif == -1)
                    cantCaracConsecutivos++;
            }
            if (cantCaracConsecutivos >= num2) {
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[debeCambiarClave] clave no puede tener " + "letras consecutivas");
                }
                return true;
            }
            if (NumerosUtil.esNumero(String.valueOf(valPri)) && NumerosUtil.esNumero(String.valueOf(valSeg))) {
                valDif = valPri - valSeg;
                if (valDif == 1 || valDif == -1)
                    cantNumConsecutivos++;

                valDif = valSeg - valTer;
                if (valDif == 1 || valDif == -1)
                    cantNumConsecutivos++;
            }
            if (cantNumConsecutivos >= num2) {
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[debeCambiarClave] clave no puede tener " + "numeros consecutivas");
                }
                return true;
            }
            cantCaracConsecutivos = 0;
            cantNumConsecutivos = 0;
        }
        return false;
    }

    /**
     * M�todo que realiza la validacion del multipass, luego de haberlo ingresado.
     * <p>
     * Registro de versiones:
     * <ul>
     * <LI>1.0 10/04/2012, Gonzalo Cofre Guzman. (SEnTRA) - Versi�n inicial
     * </ul>
     * <p>
     * 
     * @since 1.5
     * @return despacho luego de autenticar multipass.
     */

    public String validaToken() {

        try {
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[validaToken]::autenticacion SegundaClave:: inicio");
            }
            SegundaClave segundaClave = new SegundaClave();
            segundaClave.autenticaSegundaClave(claveToken, sesion.getCanalId(), clienteMB.getRut(),
                clienteMB.getDigitoVerif());
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[validaToken]::autenticacion SegundaClave:: OK");
            }

            Hashtable metodosAutenticacion = ConectorStruts.getSessionBCI().getMetodosAutenticacion();
            if (metodosAutenticacion.get(ID_PRODTOKEN) != null) {
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("SegundaClaveMB:: Cambiando Estado Token");
                }
                metodosAutenticacion.remove(ID_PRODTOKEN);
                metodosAutenticacion.put(ID_PRODTOKEN, new Boolean(true));
            }
            ConectorStruts.getSessionBCI().setMetodosAutenticacion(metodosAutenticacion);

            return despachoLogin(usuarioModelMB.getListaConvenios());
        }
        catch (wcorp.serv.seguridad.SeguridadException ex) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[validaToken verifica token] " + "SeguridadException :" + ex);
            }
            ex = (SeguridadException) ControlErrorMsg.modificaCodigoError((Exception) ex, sesion.getCanalId());
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("ex.getCodigo() : " + ex.getCodigo());
            }

            this.getMensajes().setMensajeError(ex.getSimpleMessage());
            return this.getMensajes().obtenerPaginaError();
        }
        catch (wcorp.util.GeneralException ex) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error(
                    "[validaToken verifica token] GeneralException :" + ex + " "
                        + ErroresUtil.extraeStackTrace(ex));
            }
            ex = (GeneralException) ControlErrorMsg.modificaCodigoError((Exception) ex, sesion.getCanalId());
            this.getMensajes().setMensajeError(ex.getMessage());
            return this.getMensajes().obtenerPaginaError();
        }
        catch (Exception ex) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[LoginEmpresasControllerMB verifica token] Sesion HTTP Expirada");
                getLogger().error("[LoginEmpresasControllerMB verifica token] Exception :" + ex);
            }
            ex = ControlErrorMsg.modificaCodigoError(ex, sesion.getCanalId());
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[validaToken verifica token] Sesion Expirada");
            }
            this.getMensajes().setMensajeError(ex.getMessage());
            return this.getMensajes().obtenerPaginaError();
        }
    }

    /**
     * Permite setear el Mensaje de Error.
     * 
     * @param mensajeError para setear Mensaje de Error
     */
    protected void setMensajeError(String mensajeError) {

    }

    /**
     * Obtiene el despacho de la seleccionConvenio.
     * 
     * @return vistaSeleccionConvenio
     */
    public String seleccionConvenio() {
        return "/cl/bci/aplicaciones/seguridad/autenticacion/vista/vistaSelectorConvenio.jsf";
    }

    /**
     * <p>
     * Metodo para dar formato con separador de miles a un rut.
     * </p>
     * Registro de versiones:
     * <ul>
     * <LI>1.0 18/10/2012, H�ctor Hern�ndez Orrego. (SEnTRA) -Versi�n inicial
     * </ul>
     * 
     * @param numero numero a formatear.
     * @return String con el formato de separador de miles.
     * @since 1.9
     */
    public String separadorDeMiles(long numero) {
        try {
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setGroupingSeparator('.');
            DecimalFormat formatter = new DecimalFormat("###,###", symbols);
            return formatter.format(numero);
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[separadorDeMiles] : Numero a formatear = " + numero);
                getLogger().error(
                    "[separadorDeMiles]: Problemas al realizar la separacion de miles["
                        + ErroresUtil.extraeStackTrace(e) + "]");
            }
        }
        return String.valueOf(numero);
    }

    /**
     * <p>
     * Metodo para seleccionar convenio.
     * </p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 27/12/2013, Rodolfo Kafack Ghinelli. (SEnTRA) -Versi�n inicial</li>
     * </ul>
     * 
     * @param listaConvenios lista de convenios.
     * @return String con la url a despachar.
     * @throws Exception excepcion en caso de error.
     * @since 1.0
     */
    public String seleccionConvenvenioEmpresa(ListaConvenios[] listaConvenios) throws Exception {

        SessionBCI sessionBCI = ConectorStruts.getSessionBCI();
        String userIdrut = rutCliente;
        String pin = clave;
        String userIdAux = String.valueOf(dvCliente);
        long userRUT = (new Long(userIdrut)).longValue();

        CanalTO canalCliente = null;
        try {
            canalCliente = (CanalTO) portal.getCanalPersonalizado();
        }
        catch (Exception e) {
            if (getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error(
                    "[despachoLogin] rut[" + rutCliente + "] Canal invalido, al instanciar Canal(" + canal + "): "
                        + e.getMessage());
            }
            throw new wcorp.util.GeneralException("ESPECIAL", "Canal inv�lido");
        }

        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[despachoLogin] entro al if porque posee SOLO UN convenio");
            getLogger().debug("[despachoLogin] canal[" + canal + "]");
            getLogger().debug("[despachoLogin] RutEmpresa[" + listaConvenios[0].getRutEmpresa() + "]");
            getLogger().debug("[despachoLogin] Digito[" + listaConvenios[0].getDigitoVerificador() + "]");
            getLogger().debug("[despachoLogin] userIdRut[" + userIdrut + "]");
            getLogger().debug("[despachoLogin] userIdAux[" + userIdAux + "]");
            getLogger().debug("[despachoLogin] convenio[" + listaConvenios[0].getNumConvenio() + "]");
            getLogger().debug("[despachoLogin] RazonSocial[" + listaConvenios[0].getNombreRazonSocial() + "]");
            getLogger().debug("[despachoLogin] Nombre[" + listaConvenios[0].getNombre() + "]");
            getLogger().debug("[despachoLogin] ApePaterno[" + listaConvenios[0].getApellidoPaterno() + "]");
            getLogger().debug("[despachoLogin] ApeMaterno[" + listaConvenios[0].getApellidoMaterno() + "]");
            getLogger().debug("[despachoLogin] MailUsuario[" + listaConvenios[0].getMailUsuario() + "]");
            getLogger().debug("[despachoLogin] TelefonoUsuario[" + listaConvenios[0].getTelefonoUsuario() + "]");
            getLogger().debug(
                "[despachoLogin] IndicadorEmpresa (0=empresa; 1=webPyme; 2=Empresa Movil)["
                    + listaConvenios[0].getIndicadorEmpresa() + "]");
        }
        String nombreOperador = listaConvenios[0].getNombre().trim() + " "
            + listaConvenios[0].getApellidoPaterno().trim() + " " + listaConvenios[0].getApellidoMaterno().trim();
        usuarioModelMB.setListaConvenios(listaConvenios);
        usuarioModelMB.setRut(Long.valueOf(rutCliente));
        usuarioModelMB.setDigitoVerif(dvCliente);
        usuarioModelMB.setNombres(listaConvenios[0].getNombre().trim());
        usuarioModelMB.setApPaterno(listaConvenios[0].getApellidoPaterno().trim());
        usuarioModelMB.setApMaterno(listaConvenios[0].getApellidoMaterno().trim());
        usuarioModelMB.setTipoUsuario(listaConvenios[0].getTipoUsuario());

        clienteMB.setRut(listaConvenios[0].getRutEmpresa());
        clienteMB.setDigitoVerif(listaConvenios[0].getDigitoVerificador());
        clienteMB.setRazonSocial(listaConvenios[0].getNombreRazonSocial());
        clienteMB.setNumeroConvenio(listaConvenios[0].getNumConvenio());

        String telefonoUsu = listaConvenios[0].getTelefonoUsuario();
        usuarioModelMB.setTelefono(telefonoUsu.trim().equals("") ? "0" : telefonoUsu.trim());
        usuarioModelMB.setMail(listaConvenios[0].getMailUsuario().trim());

        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[despachoLogin]nombreOperador[" + nombreOperador + "]");
        }
        String clavePaso = (String) ConectorStruts.getAttribBCI("claveWLS");
        Hashtable metodosAutenticacion = ConectorStruts.getSessionBCI().getMetodosAutenticacion();
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[despachoLogin] MetodosAutenticacion1: " + sessionBCI.getMetodosAutenticacion());
        }
        String rutEmp = String.valueOf(listaConvenios[0].getRutEmpresa());
        sessionBCI = new SessionBCI(canal, StringUtil.rellenaConCeros(rutEmp, LARGO_RUT),
            String.valueOf(listaConvenios[0].getDigitoVerificador()), StringUtil.rellenaConCeros(userIdrut,
                LARGO_RUT), userIdAux, listaConvenios[0].getNumConvenio().trim(),
            listaConvenios[0].getNombreRazonSocial(), nombreOperador);
        sessionBCI.setMetodosAutenticacion(metodosAutenticacion);
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[despachoLogin] MetodosAutenticacion2: " + sessionBCI.getMetodosAutenticacion());
        }
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[despachoLogin] se crea la sesion definitiva");
        }
        perfilUsuario(String.valueOf(userRUT), String.valueOf(listaConvenios[0].getNumConvenio()),
            String.valueOf(listaConvenios[0].getRutEmpresa()));
        sessionBCI.setPerfil((String) ConectorStruts.getAttribHttp("perfil"));
        sessionBCI.setTipoUsuario((String) ConectorStruts.getAttribHttp("tipoUsuario"));
        sessionBCI.tipoUser = 'T';
        sessionBCI.setIP(ConectorStruts.getRemoteAddr());
        ConectorStruts.setAttribHttp("sessionBci", sessionBCI);
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[validaClaveInternet]SessionBCI en HTTP");
        }
        ConectorStruts.setAttribBCI("parametros", "usunumrut=" + (userIdrut.length() >= LARGO_RUT ? "" : "0")
            + userIdrut + "&usuvrtrut=" + userIdAux);
        ConectorStruts.setAttribBCI("nomEmp", listaConvenios[0].getNombreRazonSocial());
        ConectorStruts.setAttribBCI("listaConvenios", listaConvenios);

        ConectorStruts.setAttribBCI("claveWLS", clavePaso);
        if (userIdrut.compareTo("") == 0 | pin.compareTo("") == 0) {
            throw new wcorp.util.GeneralException("PARAM", "Valores Insuficientes");
        }
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("Redireccion a sitio Pyme con unico convenio.");
        }

        this.getEnrolamiento().comprobacionInicialEnrolamiento();

        if (getLogger().isEnabledFor(Level.DEBUG)) {
            getLogger().debug(
                "[despachoLogin] this.getEnrolamiento().getEnrola().getEstadoEnrolamiento() :: "
                    + this.getEnrolamiento().getEnrola().getEstadoEnrolamiento());
            getLogger().debug(
                "[despachoLogin] this.getEnrolamiento().getEnrola().getFechaEnrolamiento() :: "
                    + this.getEnrolamiento().getEnrola().getFechaEnrolamiento());
        }

        if (this.getEnrolamiento().getEnrola().getEstadoEnrolamiento() == 1
            && this.getEnrolamiento().getEnrola().getFechaEnrolamiento() == null) {
            if (getLogger().isEnabledFor(Level.DEBUG)) {
                getLogger().debug(
                    "[despachoLogin] Requiere Enrolamiento :: "
                        + this.getEnrolamiento().getEnrola().getEstadoEnrolamiento());
            }
            return vistaEnrolamiento;
        }

        return seleccionConvenio();

    }

    /**
     * M�todo retorna url de error propia para BciNova.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 03-08-2012 H�ctor Hern�ndez Orrego (Sentra): Versi�n Inicial</li>
     * </ul>
     * </p>
     * 
     * @return String con la Url de p�gina de error.
     * @since 1.0
     */
    protected String obtenerPaginaError() {
        return null;
    }

    /**
     * Filtra los convenios para la version movil, los convenios a filtrar: dejar fuera los convenios pyme y
     * convenios donde el usuario es supervisor.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0, 25/11/2013 Rodolfo Kafack Ghinelli. (SEnTRA) : Versi�n inicial.</li>
     * <li>1.1, 26/11/2013 Oliver Hidalgo Leal. (BCI) : Agrega arraylist para filtar convenio.</li>
     * </ul>
     * <p>
     * 
     * @param listaConvenios ingresa la lista completa de arreglos.
     * @since 1.0
     * @return filtroConvenios retorna los convenios filtrados
     **/
    public ListaConvenios[] filtraListaConveniosEmpresa(ListaConvenios[] listaConvenios) {
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[filtraListaConvenios] : INICIO : ");
        }

        ArrayList<ListaConvenios> filtroConvenios = null;
        ListaConvenios[] retornoConveniosFiltrados = null;

        if (listaConvenios != null && listaConvenios.length > 0) {

            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[filtraListaConvenios] : Tiene Convenios: ");
            }

            filtroConvenios = new ArrayList<ListaConvenios>();

            for (int i = 0; i < listaConvenios.length; i++) {

                if (getLogger().isDebugEnabled()) {
                    getLogger().debug(
                        "[filtraListaConvenios] : Tipo Usuario: "
                            + listaConvenios[i].getTipoUsuario().equals("SUP"));
                    getLogger().debug(
                        "[filtraListaConvenios] : Indicador Empresas: " + listaConvenios[i].getIndicadorEmpresa());
                }

                if (!listaConvenios[i].getTipoUsuario().equals("SUP")
                    && listaConvenios[i].getIndicadorEmpresa() == 0) {
                    filtroConvenios.add(listaConvenios[i]);
                }
            }
            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[filtraListaConvenios] : salio del ciclo : ");
            }

            if ((filtroConvenios != null) && (filtroConvenios.size() > 0)) {
                if (getLogger().isDebugEnabled()) {
                    getLogger().debug("[filtraListaConvenios] : filtroConvenios.size: " + filtroConvenios.size());
                }
                retornoConveniosFiltrados = new ListaConvenios[filtroConvenios.size()];
                for (int i = 0; i < filtroConvenios.size(); i++) {
                    retornoConveniosFiltrados[i] = filtroConvenios.get(i);
                }
            }

        }
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("[filtraListaConvenios] : seteo todo ok... : ");
            getLogger().debug("[filtraListaConvenios] : FIN : ");
            getLogger().debug("[filtraListaConvenios] : Seteo los convenios en el model ");
        }

        usuarioModelMB.setListaConvenios(retornoConveniosFiltrados);

        return retornoConveniosFiltrados;
    }

    public String getClaveToken() {
        return claveToken;
    }

    public void setClaveToken(String claveToken) {
        this.claveToken = claveToken;
    }

    public ServiciosAutenticacion getServiciosAutenticacion() {
        return serviciosAutenticacion;
    }

    public void setServiciosAutenticacion(ServiciosAutenticacion serviciosAutenticacion) {
        this.serviciosAutenticacion = serviciosAutenticacion;
    }

    public String getAutenticaSegundaClave() {
        return autenticaSegundaClave;
    }

    public void setAutenticaSegundaClave(String autenticaSegundaClave) {
        this.autenticaSegundaClave = autenticaSegundaClave;
    }

    public BCIExpress getBciexpress() {
        return bciexpress;
    }

    public void setBciexpress(BCIExpress bciexpress) {
        this.bciexpress = bciexpress;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public String getRutCliente() {
        return rutCliente;
    }

    public void setRutCliente(String rutCliente) {
        this.rutCliente = rutCliente;
    }

    public char getDvCliente() {
        return dvCliente;
    }

    public void setDvCliente(char dvCliente) {
        this.dvCliente = dvCliente;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getServicioInicial() {
        return servicioInicial;
    }

    public void setServicioInicial(String servicioInicial) {
        this.servicioInicial = servicioInicial;
    }

    public String getDespacho() {
        return despacho;
    }

    public void setDespacho(String despacho) {
        this.despacho = despacho;
    }

    public String getRutEmpresa() {
        return rutEmpresa;
    }

    public void setRutEmpresa(String rutEmpresa) {
        this.rutEmpresa = rutEmpresa;
    }

    public char getDigitoVerificador() {
        return digitoVerificador;
    }

    public void setDigitoVerificador(char digitoVerificador) {
        this.digitoVerificador = digitoVerificador;
    }

    public String getCambiaClave() {
        return cambiaClave;
    }

    public void setCambiaClave(String cambiaClave) {
        this.cambiaClave = cambiaClave;
    }

    public String getVistaEnrolamiento() {
        return vistaEnrolamiento;
    }

    public void setVistaEnrolamiento(String vistaEnrolamiento) {
        this.vistaEnrolamiento = vistaEnrolamiento;
    }

    public PortalMB getPortal() {
        return portal;
    }

    public void setPortal(PortalMB portal) {
        this.portal = portal;
    }

    public SegundaClaveMB getSegundaClaveMB() {
        return segundaClaveMB;
    }

    public void setSegundaClaveMB(SegundaClaveMB segundaClaveMB) {
        this.segundaClaveMB = segundaClaveMB;
    }

    public SesionMB getSesion() {
        return sesion;
    }

    public void setSesion(SesionMB sesion) {
        this.sesion = sesion;
    }

    public ClienteMB getClienteMB() {
        return clienteMB;
    }

    public void setClienteMB(ClienteMB clienteMB) {
        this.clienteMB = clienteMB;
    }

    public UsuarioModelMB getUsuarioModelMB() {
        return usuarioModelMB;
    }

    public void setUsuarioModelMB(UsuarioModelMB usuarioModelMB) {
        this.usuarioModelMB = usuarioModelMB;
    }

    public MensajesErrorUtilityMB getMensajes() {
        return mensajes;
    }

    public void setMensajes(MensajesErrorUtilityMB mensajes) {
        this.mensajes = mensajes;
    }

    public EnrolamientoClaveUtilityMB getEnrolamiento() {
        return enrolamiento;
    }

    public void setEnrolamiento(EnrolamientoClaveUtilityMB enrolamiento) {
        this.enrolamiento = enrolamiento;
    }

    public boolean isCanalMovil() {
        return canalMovil;
    }

    public void setCanalMovil(boolean canalMovil) {
        this.canalMovil = canalMovil;
    }

    /**
     * M�todo que obtiene el log.
     * 
     * @return log.
     */
    public Logger getLogger() {
        if (logger == null) {
            logger = Logger.getLogger(this.getClass());
        }
        return logger;
    }
    

    /**
     * M�todo que realiza la journalizaci�n de selecci�n convenio.
     * <p>
     * Registro de versiones:<ul>
     * <li>1.0 02/07/2015 Rodrigo Troncoso Balboa (ImageMaker SA) - Ivan Carvajal (BCI): Versi�n Inicial </li>
     * </ul>
     * </p>
     * @param rut           rut del cliente 
     * @param dv            dv del cliente 
     * @param rutUsuario    rut del usuario u operador
     * @param dvUsuario     DV del usuario u operador
     * @param subCodNeg     SubCodigo de negocio
     * @param codigo        codigo de exception
     * @param nombreCanal   nombre canal 
     * @param idMedio       ip del host conectandose
     */
    protected void journalizaSeleccionConvenio(long rut, String dv, long rutUsuario, String dvUsuario, 
            String subCodNeg, String codigo, String nombreCanal, String idMedio){
        if(logger.isEnabledFor(Level.INFO)){
            logger.info("[journalizaSeleccionConvenio] [" + rut + "] Inicio journalizacion SubCodNeg=<" 
                + subCodNeg + ">, codigo=<" + codigo + ">, dvCliente=<" 
                + dvCliente + ">, nombreCanal=<" + nombreCanal + ">, " + "idMedio=<" + idMedio + ">");
        }
        
        try{
            HashMap datos = new HashMap();

            datos.put("codEventoNegocio", EVE_LOGINCOV);
            datos.put("subCodEventoNegocio", subCodNeg);
            datos.put("idProducto", ID_PRODINTERNET);
            

            datos.put("glosaLog", "Seleccion de convenio:"+String.valueOf(rutUsuario));
          
            Journalist.getInstance().publicar(datos);
            
            if(logger.isEnabledFor(Level.DEBUG)){
                logger.debug("[journalizacion][" + rutCliente + "] Journalizacion OK");
            }
        }
        catch(Exception e){
            if(logger.isEnabledFor(Level.ERROR)){
                logger.error("[journalizaSeleccionConvenio] [" + rutCliente + "] [Exception] No Journalizo, mensaje=<" 
                        + e.getMessage() + "> ",e);
            }
        }
    }

}