package wcorp.util.com;

import java.util.*;
import javax.naming.*;
import wcorp.util.*;
// Especifica Servidor de Naming para Servicios
public class JNDIConfig {

    // Obtiene el Contexto inicial a partir de una URL
    static public InitialContext getInitialContext(String url) throws NamingException {
        String FACTORY_CLASS = "weblogic.jndi.WLInitialContextFactory";

	    String urlsp = (String)((Hashtable) TablaValores.getTabla("JNDIConfig.parametros").get(url.toUpperCase())).get("PARAM");

        if (urlsp == null)
            throw new NamingException();

        //String urlsp = url != null ? (url.equalsIgnoreCase("WTC_CONTEXT") ? TablaValores.getValor("JNDIConfig.parametros", "wtc_context", "param") : url) : url;

        String URL_OF_NAMING_SERVICE = urlsp;
        String SECURITY_USER = null;
        String SECURITY_PASS = null;

        Properties p = new Properties();
        p.put(Context.INITIAL_CONTEXT_FACTORY, FACTORY_CLASS);
        p.put(Context.PROVIDER_URL, URL_OF_NAMING_SERVICE);

        if (SECURITY_USER != null) {
            p.put(Context.SECURITY_PRINCIPAL, SECURITY_USER);
            p.put(Context.SECURITY_CREDENTIALS, SECURITY_PASS);
        }

        return new InitialContext(p);
    }

    static public InitialContext getInitialContext() throws NamingException {
        //String datoscluster = "t3://" + TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");
//        return getInitialContext("cluster");
//        Modificación para crear instancia de ejbs en integración, sin modificar la key cluster del JNDIConfig.parametros (continua siendo local)
//        Se debe agregar "empresasInte;param=t3://161.131.141.253:9101,161.131.141.246:9101;" en JNDIConfig.parametros
        return getInitialContext(TablaValores.getValor("JNDIConfig.parametros", "ambienteEJB", "contexto"));
    }

    // Obtiene el Contexto inicial a partir de una URL,usuario y pwd
    static public InitialContext getInitialContext(String url,String userParam,String pwdParam) throws NamingException {
        String FACTORY_CLASS = "weblogic.jndi.WLInitialContextFactory";

        String urlsp = (String)((Hashtable) TablaValores.getTabla("JNDIConfig.parametros").get(url.toUpperCase())).get("PARAM");

        if (urlsp == null)
            throw new NamingException();

        //String urlsp = url != null ? (url.equalsIgnoreCase("WTC_CONTEXT") ? TablaValores.getValor("JNDIConfig.parametros", "wtc_context", "param") : url) : url;

        String URL_OF_NAMING_SERVICE = urlsp;
        String SECURITY_USER = userParam;
        String SECURITY_PASS = pwdParam;

        Properties p = new Properties();
        p.put(Context.INITIAL_CONTEXT_FACTORY, FACTORY_CLASS);
        p.put(Context.PROVIDER_URL, URL_OF_NAMING_SERVICE);

        if (SECURITY_USER != null) {
            p.put(Context.SECURITY_PRINCIPAL, SECURITY_USER);
            p.put(Context.SECURITY_CREDENTIALS, SECURITY_PASS);
        }

        return new InitialContext(p);
    }
}