package wcorp.model.seguridad;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.CreateException;
import javax.ejb.Handle;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.bprocess.seguridad.token.TokenDelegate;
import wcorp.infraestructura.seguridad.autenticacion.helpers.SoftTokenHelper;
import wcorp.infraestructura.seguridad.autenticacion.to.SoftTokenTO;
import wcorp.model.actores.Cliente;
import wcorp.model.actores.Colaborador;
import wcorp.serv.autenticacion.ServiciosAutenticacion;
import wcorp.serv.autenticacion.ServiciosAutenticacionHome;
import wcorp.serv.clientes.ClientesException;
import wcorp.serv.clientes.Operacion;
import wcorp.serv.clientes.ServiciosCliente;
import wcorp.serv.clientes.ServiciosClienteHome;
import wcorp.serv.cuentas.Cta;
import wcorp.serv.misc.ServiciosMiscelaneos;
import wcorp.serv.misc.ServiciosMiscelaneosHome;
import wcorp.serv.seguridad.EstadoPin;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.serv.seguridad.ServiciosSeguridad;
import wcorp.serv.seguridad.ServiciosSeguridadHome;
import wcorp.serv.seguridad.ValidaPinCorp;
import wcorp.util.ErroresUtil;
import wcorp.util.GeneralException;
import wcorp.util.LogFile;
import wcorp.util.NumberWraper;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.com.JNDIConfig;
import wcorp.util.com.TransaccionException;
import wcorp.util.com.TuxedoException;
import wcorp.util.tcreditoonline.TCreditoOnlineSessionCache;

import cl.bci.middleware.infraestructura.seguridad.to.TipoBancaEnum;
import cl.bci.middleware.infraestructura.seguridad.to.UsuarioTO;

/**
 * <b>MODELA LA SESI�N BCI.</b>
 * <p>
 *
 * Registro de versiones:<ul>
 *
 * <li>1.0 (??/??/????, autor desconocido): Versi�n Inicial
 *
 * <li>1.1 (07/04/2004, Hugo Villavicencio (Apolo)): Sobrecarga m�todo cambioClave
 *          para poder retornar por un nuevo par�metro por referencia cuales de los
 *          m�todos internos fueron ejecutados satisfactoriamente.
 *
 * <li>1.2 (14/03/2005, Mart�n Maturana): Se remueve atributo dispatcherPassCode en
 *          m�todo PassCodePaso2 para efectos de redireccionar correctamente una
 *          vez realizado un cambio de clave al estar realizando una transferencia
 *          de fondos por concepto de expiraci�n de clave passcode. Se crea nuevo
 *          m�todo autenticaClaveExp para permitir la autenticaci�n de un cliente
 *          con clave passcode cuando �ste la tiene expirada.
 *
 * <li>1.3  (12/11/2004, Victor Urra (Novared)): Proyecto Mejoras Seguridad, se modifico m�todo <b>asignaMetodosAutenticacion</b>
 *                                               para saber cuando un token esta bloquedo, esto es utilizado con el fin de no
 *                                               permitir transferencia si un token esta en estado bloqueado y a su vez permitir
 *                                               ingresar al Banco Enlinea.
 * <li>1.2 (2005-11-11, Aurora Leyton Benitez), SEnTRA. Se modifica:
 *          Se lee atributo de session si es clinocnv para autentificar la clave con canal 110
 *          a clientes sin convenio
 * <li>2.0 (19/12/2005, Alfredo Reyes.): Se modifica la clase para que pueda
 * recibir y entregar el id se la session con los  metodo setIdSession() y getIdSession().
 *
 * <li>2.1 (08/03/2006, Aurora Leyton Benitez(SEnTRA)): se agrego l�gica para permitir el cambio de clave a clientes empresa
 * con convenio en express y Epyme.
 * <li>2.2 (20/06/2006, Paola Parra (SEnTRA)): Se agrega log en m�todo autenticaToken, autentica y asignaMetodosAutenticacion.
 * <li>2.3 (16/06/2006, Paola Parra (SEnTRA)): Se agrega control de excepciones a m�todo autenticaToken
 * <li>2.4 (28/06/2006, Claudio P�rez Bravo(SEnTRA)): En el m�todo cambioClave(String pin, String newPin, String canal)
 * se agrego l�gica para controlar las validaciones y excepciones que se producen en claves internet expiradas y
 * asi proveer la posibilidad de actualizar la fecha de expiraci�n en el cambio de la clave, estas acciones se
 * realizan solamente cuando la conexi�n no es la primera que realiza el cliente.
 * <li>2.5 (28/08/2006, Luis Cruz (Imagemaker IT)): se agrega reemplazo de tdc desde nexus.
 * <li>2.6 (09/01/2007, Rodrigo Devia): Se agrega setTipoUsuario, setPerfil, getTipoUsuario, getPerfil para ser utilizados en el ingreso de los
 *                      clientes corporate al sitio de empresas.
 * <li>2.7 (29/09/2006, Victor Urra (Novared)): Se incorporo l�gica en el m�todo <b>asignaMetodosAutenticacion</b> para la obtenci�n
 *                                              de un valor que identifica si un cliente debe realizar la autentificaci�n de multipass,
 *                                              este dato es obtenido de sybase SOLO cuando un cliente tiene su multipass en estado "ACTIVO".
 * <li>2.8 (01/02/2007, Pedro Carmona (SEnTRA)): Se agregan l�neas de log's para detectar problemas de solicitudes de cambio
 *                      Clave Dos y Autenticaci�n de Multipass. Los m�todos que intervenidos son: autenticaClaveExp(), cambioClave().
 * <li>2.9 (27/05/2008, Emilio Fuentealba Silva (SEnTRA)): Se modifica m�todo asignaMetodosAutenticacion en la cual se condiciona la preferencia de solicitud de multipass al momento
 *                                              de realiazar el login. Para ello se ha agregado una entrada en la tabla Seguridad.parametros donde se activa o desactiva la opci�n
 *                                              que verifica si se considera la preferencia del cliente la cual se va a buscar a la opc01 en TANDEM.
 * <li>3.0 (13/04/2009, Jaime Pezoa N��ez (BCI)): Actualmente los usuarios Empresas con convenio Banca Electr�nica no pueden
 *          cambiar su clave por el sitio Cash Express, porque se compara la clave que ingresaron con la clave del Supervisor
 *          del Rut de la Empresa.
 *          Se modifican los m�todos autenticaClaveExp y cambioClave, para enviar el Rut y d�gito verificador del Usuario
 *          conectado, en vez del Rut de la Empresa, cuando el canal (PinID) 130.
 *
 *          NOTA:
 *          Seg�n el seguimiento que se hizo al Login, ServicioSeguridadBean y SessionBCI, estamos hablando del PinID, no del
 *          canal, aunque aparece con ese nombre en el SessionBCI. Los canales Empresas son:
 *          - El canal 130 corresponde a MOPA (BCIExpress - http://www.bciexpress.bci.cl)
 *          - El canal 131 corresponde a MOPA (Webbel - http://webbel.bci.cl)
 *          - El Canal 230 corresponde a WLS   (Cash Express - http://www.bci.cl/empresas y http://www.bci.cl/empresarios)
 *
 *          Los canales 130 y 131 son MOPA y no se gatillan hacia WLS, por lo que quedan exclu�dos.
 *          Para el canal 230, tenemos 2 casos:
 *          - Clientes sin Convenio: Tiene  Clave Internet,   su canalID es 230 y el PinID es 110. No est�n afectados al cambio.
 *          - Clientes con Convenio: Tienen Clave BCIExpress, su canalID es 230 y el PinID es 130. No est�n afectados al cambio.
 *
 * <li>3.0a(13/04/2009, Jaime Pezoa N��ez (BCI)): - Por petici�n de Miguel Farah, se modifica la variable enum por enumera en los m�tos getObjetos y cargaProductos, ya que desde Java 5
 *          pasa a ser una palabra reservada.
 * <LI>3.1 08/04/2010 Magdalena D�az (ImageMaker IT): Se capturan las excepciones TuxedoException y RemoteException en el m�todo {@link #autenticaToken(String)}, porque actualmente
 *         cuando se genera un TuxedoException o RemoteException en la autenticaci�n del Token se contin�a con el flujo y para casos como TTFF se permite la ejecuci�n.
 * <LI>3.2 28/04/2010 Meng-Hsiung Chao (ImageMaker IT)): Se agrega excepcion para autenticar softtoken RSA en el metodo {@link #autenticaToken(String)}
 * <LI>3.3 29/06/2010 Meng-Hsiung Chao (ImageMaker IT): Se agregan nuevas excepciones para el manejo de softtoken RSA y pin de validaci�n en el metodo {@link #autenticaToken(String)}
 * <li>3.0 13/10/2010 Pablo Gavil�n Lara(Imagemaker IT): se agrega un if en autenticaToken para validar el tipo de dispositivo desde
 * donde se obtiene el passcode. Se remueven imports en desuso y se especifican las importaciones.
 * <li>3.4 03/05/2011 Pablo Gavil�n Lara(Imagemaker IT): Se modifica la llamada a {@link ServiciosSeguridad#autenticar(SoftTokenTO)} por una llamada
 * a {@link SoftTokenHelper#autenticar(SoftTokenTO)}</li>
 * <li>3.5 15/09/2011, Gustavo L�pez S. (Orand): Se agrega el parametro canal al m�todo {@link #asignaMetodosAutenticacion()}
 *         para identificar desde donde se esta realizando la consulta.
 * <li>3.6 24/05/2012, H�ctor Hern�ndez Orrego. (SEnTRA) - Se modifican {@link #autenticaToken(String)} y
 * {@link #asignaMetodosAutenticacion()}, para utilizar el rut de usuario y no de la empresa (cliente) en 
 * las funcionalidades de los m�todos.</li>       
 * <li>3.7 (23/04/2012, Jaime Pezoa N��ez (BCI): Se modifican los m�todos autenticaClaveExp y cambioClave para incorporar al sitio Pyme.</li>  
 * <li>3.6 (23/04/2012, Jaime Pezoa N��ez (BCI): Se modifican los m�todos autenticaClaveExp y cambioClave para incorporar al sitio Pyme.
 * <li>3.8 07/03/2013, Carlos Cerda I. (Kubos): Se cambia clase de logueo por Log4j en los siguientes m�todos:
 * <ul>
 *  <li>{@link #setCliente(Cliente)}
 *  <li>{@link #autentica(String)}
 *  <li>{@link #autentica(String, String)}
 *  <li>{@link #autenticaClaveExp(String, String)}
 *  <li>{@link #autenticaToken(String)}
 *  <li>{@link #asignaMetodosAutenticacion()}
 *  <li>{@link #cambioClave(String, String, NumberWraper)}
 *  <li>{@link #cambioClave(String, String, String)}
 * </ul>.
 * Se cambia forma de invocar a {@link #cargaProductos(boolean)} desde los m�todos {@link #setCliente(Cliente)} y 
 * {@link #autoriza()}, a este �ltimo se le cambia el tipo de public a private.  
 * <li>3.9 02/07/2013 Carlos Cerda I. (Kubos): Se agrega l�gica de consulta de los estados del
 * Multipass hacia Tandem. Esto debido a los constantes problemas presentados en el login por la alta concurrencia
 * a la m�quina principal del Authentication Manager. Se intervienen 
 *     los m�todos:<ul>
 *     <li>{@link SessionBCI#asignaMetodosAutenticacion()}
 *     <li>{@link SessionBCI#esCanalBackOffice(String, String[])}
 *     </ul>
 * </li>
 * <li>4.0 12/05/2014 Francisco Mu�oz Arellano (BCI): Se comenta c�digo contenido en m�todo {@link SessionBCI#journalizar()}
 * para evitar conexiones al servidor ya que el archivo de log  no existe, pero se mantiene su firma para evitar impactar los
 * servicios de los diferentes canales que lo invoca. </li>
 * <li>4.1 20/04/2015 Rodrigo Troncoso Balboa (ImageMaker IT): Se cambia la validacion checkPinCorporativo
 * del ejb ServiciosSeguridad al ejb ServiciosAutenticacion para no validar logica de Clave RedBanc.
 * Metodos modificados: {@link autentica(String)}, {@link autentica(String, String)}, {@link autenticaClaveExp(String, String)}.</li>
 * <li>4.2 05/07/2013, Rodolfo Kafack Ghinelli (SEnTRA): Se agrega m�todo de autenticaci�n
 *           SafeSigner. 
 *
 * </ul>
 * <P>
 *
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * <P>
 */

// Objeto de Negocio SuperClase para Sesiones de Empleados, Clientes, Sistemas. etc.
public class SessionBCI implements Serializable, Sesion {

    /**
     * N�mero de versi�n utilizado durante la serializaci�n.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Log de la clase.
     */
    private static Logger logger = Logger.getLogger(SessionBCI.class);

    /**
     * Par�metros del Authentication Manager.
     */
    private static final String TABLA_AUTHENTICATION = "authentication_manager.parametros";

    public static final int EXEC_AUTENTICA= 1;
    public static final int EXEC_AUTENTICAUYP= 2;
    public static final int EXEC_AUTORIZA_UYP= 4;
    public static final int EXEC_CAMBIOCLAVE= 8;
    public static final int EXEC_CAMBIAPINCORPORATIVO= 16;
    public static final int EXEC_AUTORIZAUYP= 32;

    private Cliente cliente;
    private Colaborador colab;
    private AutenticaUyP auyp;
    private String ip; // Direccion IP de Origen
    private final int PINID_PINCORP_TBANC    = 100;
    private final int PINID_PINCORP_BCI      = 110;
    private final int PINID_INTRA            = 120;
    //private final int PINID_CALLCENTER_PAGOS = 160;
    private final int PINID_CALLCENTER = 160;
    private final int PINID_MICROMATICOS     = 200;

    // Datos Generales
    public String cic; // cic del cliente
    public char tipoUser; // Titular y/o Adicional
    public boolean primeraVez; // Indicador de Primera Vez
    public boolean restriccion; // Indica si tiene o no restricciones
    public boolean indCambioClave; // Indicar de Cambio obligatorio de Clave

    public boolean indAccesoPermitido;
    public boolean indCambioExitoso;
    public boolean indClaveDuplicada;
    public boolean indClaveExpirada;
    public boolean indClaveRevocada;

    //(24/06/2005, Mario Alcaraz (ECC)): EJBObjects referenciados por medio de Handles para serializar.
    private Handle servSegHandle; // Servicio de Seguridad
    /** Manejador de Servicio de Autenticacion */
    private Handle servAuthHandle;
    private Handle servMiscHandle; // Servicio de Miscelaneos

    public Canal canal; // Indica el Origen donde fue activada la Sesion
    protected List servicios = new LinkedList(); // Matriz de Servicios Activados
    protected Date fechaCreacion = new Date(); // Momento de Creaci�n
    protected Date ultimaFechaAcceso; // Momento de ultimo Uso
    protected boolean activa = false; // Indicador de Actividad de la Sesion
    protected Hashtable atributos = new Hashtable(); // Tabla de Atributos Adicionales
    protected Hashtable metodosAutenticacion = new Hashtable(); //Lista de m�todos de autenticaci�n

    private String rutusuario = null;
    private String dvusuario = null;
    private String rutempresa = null;
    private String dvempresa = null;
    private String idconvenio = null;
    private String nomEmpresa = null;
    private String nomOperador = null;
    //Variable que contiene el Id de la session
    private String idSession = null;


    // se agrega para identificar el tipoUsuario y perfil en el ingreso de corporate en el sitio
    // de empresas en el men� de tarjeta de cr�dito.
    private String tipoUsuario = null;
    private String perfil = null;


    //Para el proyecto de cambio de clave con validacion de pan
    private boolean indCtaCorrentista; // Indica si es cuenta correntista
    /**
     * Atributo para verificar si el cliente tiene SafeSigner.
     */
    private ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO = null;
    

    // Ajusta Momento de Uso
    protected void setFechaUso() {
        ultimaFechaAcceso = new Date();
    }

    // Set del canal
    protected void setCanal(String ch) {
        Canal c = new Canal(ch);
        this.canal = c;
    }

    /**
     * Establece los datos del cliente en la sesi�n eliminado los anteriores
     * Registro de versiones:<ul>
     * <li>1.0  (??/??/????, Desconocido): versi�n inicial.
     * <li>1.1  (30/08/2006, Luis Cruz (ImageMaker IT)): versi�n inicial.
     * <li>1.2  08/03/2013, Carlos Cerda I. (Kubos): Se cambia clase de logueo por Log4j y se llama al m�todo 
     * {@link #cargaProductos(boolean)} con true.
     * </ul>
     * @since 1.1
     * @param cli Cliente a establcer
     * @throws GeneralException
     * @throws RemoteException
     */
    public void setCliente(Cliente cli) throws GeneralException, RemoteException{

        logger.debug("entr� a setCliente");

        if(getAttrib("CCT")!=null) removeAttrib("CCT");
        if(getAttrib("CPR")!=null) removeAttrib("CPR");
        if(getAttrib("CCT_CCC")!=null) removeAttrib("CCT_CCC");
        if(getAttrib("TDC")!=null) removeAttrib("TDC");
        if(getAttrib("TDCNX")!=null) removeAttrib("TDCNX");
        if(getAttrib("CTANX")!=null) removeAttrib("CTANX");
        if(getAttrib("LDE")!=null) removeAttrib("LDE");

        if (cli==null) return;

        logger.debug("setCliente el cliente es=" + cli.getRut());
        this.cliente = cli;
        /**
         * Se incorporan productos del cliente a la sesion del portal
         */

        servicios = new LinkedList();

        this.cargaProductos(true);
        String file = canal.getPathTablas() + canal.getDispositivo() + ".servicios.parametros";
        servicios = ((ServiciosSeguridad)servSegHandle.getEJBObject()).autoriza((SessionBCI)this, file);
    }

    // Get del canal
    public Canal getCanal() {
        return this.canal;
    }

    // Valida si la sesion es V�lida o no
    public void checkAlive() throws wcorp.serv.seguridad.SeguridadException {
        if (!activa) throw new wcorp.serv.seguridad.SeguridadException("NOSESSION");
        expiracion(); // Unico Metodo est�ndar de Validacion
        if (!activa) throw new wcorp.serv.seguridad.SeguridadException("NOSESSION");
        setFechaUso(); // Ajusta Uso
    }

    // Momento de Inicio
    public Date getInicio() {
        return fechaCreacion;
    }

    // Momento de Inicio
    public Date getUltimoUso() {
        return ultimaFechaAcceso;
    }

    // Estado de Actividad, sin Validaci�n
    public boolean getEstado() {
        return activa;
    }

    // OPERACIONES SOBRE LOS ATRIBUTOS

    // Setea un Atributo
    public void setAttrib(String nombre, Object attrib) {
        atributos.put(nombre, attrib);
    }

    // Get de un Atributo
    public Object getAttrib(String nombre) {
        return atributos.get(nombre);
    }

    // Remove de un Atributo
    public Object removeAttrib(String nombre) {
        return atributos.remove(nombre);
    }

    // Setea un Atributo para una Aplicacion
    public void setAttrib(String app, String nombre, Object attrib) {
        atributos.put("[APP:"+app+"]"+nombre, attrib);
    }

    // Get de un Atributo para una aplicacion
    public Object getAttrib(String app, String nombre) {
        return atributos.get("[APP:"+app+"]"+nombre);
    }

    // Remove de un Atributo para una Aplicacion
    public Object removeAttrib(String app, String nombre) {
        return atributos.remove("[APP:"+app+"]"+nombre);
    }

    // OPERACIONES SOBRE LOS SERVICIOS DISPONIBLES

    // Entrega Matriz de Servicios y Objetos completa
    public List getMatrizServicios() {
        return servicios;
    }

    // Agrega Funcionalidad a la Matriz de Servicios
    public void addServicio(Servicio serv) {
        servicios.add(serv);
    }

    // Entrega Lista de Servicios en forma de 'List'
    public List getListServicios() {
        return servicios;
    }

    // Entrega Lista de Servicios Validos
    public String[] getServicios() {
        if (servicios.size()==0) return null;
        String[] lista = new String[servicios.size()];
        for (int i=0; i<servicios.size(); i++) {
            lista[i] = ((Servicio)servicios.get(i)).getId();
        }
        return lista;
    }

    // Entrega Lista de Objetos validos para un Servicio
    public Hashtable getTypedObjetos(String serv) {
        if (servicios.size()==0) return null;
        Servicio aux;
        for (int i=0; i<servicios.size(); i++) {
            aux = (Servicio)servicios.get(i);
            if (aux.getId().toUpperCase().compareTo(serv.toUpperCase())==0)return aux.getObjetos();
        }
        return null;
    }

    // Entrega el Tipo de Objetos para un Servicio
    public String getTipoObjeto(String serv, String obj) {
        if (servicios.size()==0) return null;
        Servicio aux;
        for (int i=0; i<servicios.size(); i++) {
            aux = (Servicio)servicios.get(i);
            if (aux.getId().toUpperCase().compareTo(serv.toUpperCase())==0) return aux.getTipoObjeto(obj);
        }
        return null;
    }

    /**
     * Entrega Lista de Objetos validos para un Servicio
     * Registro de versiones:<ul>
     * <li>1.0  (�?, Desconocido): versi�n inicial.
     * <li>1.1  (13/04/2009, Jaime Pezoa N��ez (BCI)): Se modifica nombre de variable
     *           enum por enumera.
     * </ul>
     * @since �?
     * @throws GeneralException
     * @throws RemoteException
     */
    public String[] getObjetos(String serv) {
        if (servicios.size()==0) return null;
        Servicio aux;
        for (int i=0; i<servicios.size(); i++) {
            aux = (Servicio)servicios.get(i);
            if (aux.getId().compareTo(serv.toUpperCase())==0) {
                Hashtable ht = aux.getObjetos();
                if (ht==null) return null;
                List result = new LinkedList();
                for (Enumeration enumera = ht.elements(); enumera.hasMoreElements(); ) {
                    String[] objs = (String[])enumera.nextElement();
                    if (objs != null) {
                        for (int j=0; j<objs.length ; j++) result.add(objs[j]);
                    }
                }
                if (result.size()<=0) return null;
                String[] data = new String[result.size()];
                for (int k=0; k<result.size();k++) data[k]=(String)result.get(k);
                return data;
            }
        }
        return null;
    }

    // Entrega Lista de Objetos de un tipo dado, validos para un Servicio
    public String[] getObjetos(String serv, String type) {
        if (servicios.size()==0) return null;
        Servicio aux;
        for (int i=0; i<servicios.size(); i++) {
            aux = (Servicio)servicios.get(i);
            if (aux.getId().toUpperCase().compareTo(serv.toUpperCase())==0) return aux.getObjetos(type);
        }
        return null;
    }

    // Valida si el Servicio y el Objeto son Validos
    public void checkServicioObjeto(String serv, String objeto) throws wcorp.serv.seguridad.SeguridadException {

        if (Integer.parseInt(canal.getPinID()) == PINID_INTRA) return;

        if (servicios.size()==0) throw new wcorp.serv.seguridad.SeguridadException("AUTORIZA");
        Servicio aux;
        for (int i=0; i<servicios.size(); i++) {
            aux = (Servicio)servicios.get(i);

            if (aux.getId().toUpperCase().compareTo(serv.toUpperCase())==0){
                aux.checkObjeto(objeto);
                return;
            }
        }
        throw new wcorp.serv.seguridad.SeguridadException("AUTORIZA");
    }

    // Valida si es un Servicio Valido
    public void checkServicio(String serv) throws wcorp.serv.seguridad.SeguridadException {
        //if (Integer.parseInt(canal.getPinID()) == PINID_INTRA) return;

        if (servicios.size()==0) throw new wcorp.serv.seguridad.SeguridadException("AUTORIZA");
        for (int i=0; i<servicios.size(); i++) {

            if (((Servicio)servicios.get(i)).getId().toUpperCase().compareTo(serv.toUpperCase())==0){
                return;
            }
        }
        throw new wcorp.serv.seguridad.SeguridadException("AUTORIZA");
    }

    // Inicia la Session
    public void activa() {
        activa=true;
        setFechaUso();
    }

    // Constructor.
    // Fecha : 24/11/2003
    public SessionBCI(String idCanal, String rutEmpresa, String dvEmpresa, String rutUsuario, String dvUsuario, String idConvenio, String nomEmpresa, String nomOperador)
            throws java.rmi.RemoteException, javax.ejb.CreateException, javax.naming.NamingException, Exception {

        this(idCanal, rutEmpresa, dvEmpresa);

        this.rutusuario = rutUsuario;
        this.dvusuario  = dvUsuario;
        this.rutempresa = rutEmpresa;
        this.dvempresa  = dvEmpresa;
        this.idconvenio = idConvenio;
        this.nomEmpresa = nomEmpresa;
        this.nomOperador = nomOperador;

        // AEAD 20031128
        // This is to avoid making maintenance to Servlets and JSPs components
        // made when WLS was going to MOPAIII to get a session.
        // These attributes must not be used in new developments !!!
        setAttrib("convenio", idConvenio);
        setAttrib("rutCliente", rutEmpresa);
        setAttrib("digVerCliente", dvEmpresa);
        setAttrib("digVerCliente", dvEmpresa);
        setAttrib("nombreCliente", nomEmpresa);
        setAttrib("rutOperadorCliente", rutUsuario);
        setAttrib("digVerOperadorCliente", dvUsuario);
        setAttrib("nombreOperadorCliente", nomOperador);
        setAttrib("apellidoPaternoOperadorCliente", "");
        setAttrib("apellidoMaternoOperadorCliente", "");

        InitialContext                 ic = JNDIConfig.getInitialContext();
        ServiciosClienteHome clientesHome = (ServiciosClienteHome) ic.lookup("wcorp.serv.clientes.ServiciosCliente");
        ServiciosCliente     clientesBean = (ServiciosCliente) clientesHome.create();

        Operacion[] ope = clientesBean.consultaOperaciones(new Long(rutEmpresa.trim()).longValue(), dvEmpresa.charAt(0), 'T');
        setAttrib("empCtas", ope);
    }

    /**
     * Constructor. Setea Valores Bases.
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0  (??/??/????, Desconocido): version inicial.
     * <li>1.1  20/04/2015 Rodrigo Troncoso Balboa (ImageMaker IT): Se agrega javadoc.
     * Se agrega instancia de handle de EJB ServiciosAutenticacion.
     * Corrige checkstyle: Se elimina wcorp.utilStrUtl</li>
     * </ul>
     * @param idCanal : id de canal.
     * @param userId : id usuario.
     * @param userIdAux : id usuario aux.
     * @throws RemoteException excepcion remota
     * @throws CreateException excepcion al crear ejb.
     * @throws NamingException excepcion al llamar a ejb
     * @throws Exception excepciones.
     * @since ??
     */
    public SessionBCI(String idCanal, String userId, String userIdAux)
            throws java.rmi.RemoteException, javax.ejb.CreateException, javax.naming.NamingException, Exception {

        setCanal(idCanal);


        switch(Integer.parseInt(canal.getPinID())){
        case PINID_MICROMATICOS:
            setIP(userId);

            break;

        case PINID_INTRA:
            this.colab= new Colaborador(userId.toUpperCase());

            break;
            //case PINID_CALLCENTER_PAGOS:
        case PINID_CALLCENTER:
            String msg = userId;
            String[] msgs = StringUtil.divide(msg, ';');
            for(int i=0; i < msgs.length; i++){
                if (msgs[i].toUpperCase().startsWith("RUT=")){
                    int idx = msgs[i].indexOf('=');
                    userId = msgs[i].substring(idx + 1, msgs[i].length() - 1);
                    userIdAux = msgs[i].substring(msgs[i].length() - 1);


                }
                if (msgs[i].toUpperCase().startsWith("TIPOUSU=")){
                    int idx = msgs[i].indexOf('=');
                    this.tipoUser = msgs[i].substring(idx + 1).charAt(0);

                }
            }
        default:
            this.cliente = new Cliente(Long.parseLong(userId), userIdAux.charAt(0));
            try{
                this.cliente.setDatosBasicos();
            }
            catch(TuxedoException e){ 
                throw new GeneralException("BADMSG",e.getMessage());
            }
            catch(ClientesException e){ 
                throw e;
            }
            catch(GeneralException e){ 
                throw e;
            }
            catch(Exception e){ 
                throw e;
            }
        }

        // Instancia EJBs de Servicios de Seguridad
        InitialContext ic = JNDIConfig.getInitialContext();
        ServiciosSeguridadHome homeSeg = (ServiciosSeguridadHome)ic.lookup("wcorp.serv.seguridad.ServiciosSeguridad");
        servSegHandle = ((ServiciosSeguridad)homeSeg.create()).getHandle();

        ServiciosMiscelaneosHome homeMisc = (ServiciosMiscelaneosHome)ic.lookup("wcorp.serv.misc.ServiciosMiscelaneos");
        servMiscHandle = ((ServiciosMiscelaneos)homeMisc.create()).getHandle();
        
        ServiciosAutenticacionHome authHome = (ServiciosAutenticacionHome)ic.lookup("wcorp.serv.autenticacion.ServiciosAutenticacion");
        servAuthHandle = ((ServiciosAutenticacion)authHome.create()).getHandle();
    }

    // Entrega el Cliente
    public Cliente getCliente() {
        return cliente;
    }

    public Colaborador getColaborador() {
        return colab;
    }

    // Entrega Rut Empresa
    // Fecha : 24/11/2003
    public String getRutEmpresa() {
        return rutempresa;
    }

    // Entrega dv Empresa
    // Fecha : 24/11/2003
    public String getDvEmpresa() {
        return dvempresa;
    }

    // Entrega Rut Usuario
    // Fecha : 24/11/2003
    public String getRutUsuario() {
        return rutusuario;
    }

    // Entrega dv Usuario
    // Fecha : 24/11/2003
    public String getDvUsuario() {
        return dvusuario;
    }

    // Entrega idConvenio
    // Fecha : 24/11/2003
    public String getIdConvenio() {
        return idconvenio;
    }

    // Entrega nomEmpresa
    // Fecha : 24/11/2003
    public String getNomEmpresa() {
        if (nomEmpresa == null) {
            return "";
        }
        return nomEmpresa;
    }

    // Entrega nomOperador
    // Fecha : 24/11/2003
    public String getNomOperador() {
        if (nomOperador == null) {
            return getCliente().getFullName();
        }
        return nomOperador;
    }


    // Setea la IP
    public void setIP(String ip) {
        this.ip=ip;
    }

    public String getIP() {
        return this.ip;
    }


    /**
     * Autentifica la Sesion.
     *
     * Registro de versiones:<ul>
     *
     * <li>1.0  (??/??/????, Desconocido): version inicial
     * <li>1.1 (20/06/2006, Paola Parra (SEnTRA)): Se agrega log para cuando se trata de autentificar token
     * <li>1.2  08/03/2013, Carlos Cerda I. (Kubos): Se cambia clase de logueo por Log4j.
     * <li>1.3  20/04/2015 Rodrigo Troncoso Balboa (ImageMaker IT): Se cambia la validacion checkPinCorporativo
     * del ejb ServiciosSeguridad al ejb ServiciosAutenticacion para no validar logica de Clave RedBanc.</li>
     * </ul>
     * <p>
     * @param pin : clave token
     * @throws TuxedoException excepcion de tuxedo
     * @throws SeguridadException excepcion de seguridad
     * @throws GeneralException excepcion general
     * @throws RemoteException excepcion remota
     * @throws Exception excepcion
     * @since 1.0
     */
    public void autentica(String pin) throws TuxedoException, SeguridadException, GeneralException, RemoteException, Exception {

        switch(Integer.parseInt(canal.getPinID())){
        case PINID_INTRA:
            try{
                AutenticaUyP auyp = new AutenticaUyP(colab);
                auyp.autentica(pin);
                this.indAccesoPermitido = auyp.indAccesoPermitido;
                this.indCambioExitoso = auyp.indCambioExitoso;
                this.indClaveDuplicada = auyp.indClaveDuplicada;
                this.indClaveExpirada = auyp.indClaveExpirada;
                this.indClaveRevocada = auyp.indClaveRevocada;
                this.indCambioClave = auyp.indCambioClave;

                this.tipoUser = 'E';
            }
            catch(TransaccionException e){
                throw e;
            }
            catch(GeneralException e){
                throw e;
            }
            break;
            //case PINID_CALLCENTER_PAGOS:
        case PINID_CALLCENTER:
            this.indCambioClave = false;
            this.cic = "";
            this.primeraVez = false;
            this.restriccion = false;
            break;
        default:
            logger.debug("[auntentica default] rut["+getCliente().getRut()+"] canal.getCanalID() ["
                    + canal.getCanalID() + "]");

            ValidaPinCorp vp = null;
            if (getRutUsuario()== null)          //para clientes empresa con convenio la primera vez es nula esta variable
            {
                logger.debug("[auntentica default] rut["+getCliente().getRut()+"] canal igual 230");
                logger.debug("[auntentica default] rut["+getCliente().getRut()+"] Rut usuario  : [" 
                        + getRutUsuario() + "]");
                logger.debug("[auntentica default] rut["+getCliente().getRut()+"] getRut()     : [" 
                        + getCliente().getRut() + "]");
                vp = ((ServiciosAutenticacion)servAuthHandle.getEJBObject()).checkPinCorporativo(canal.getPinID(), getCliente().getRut(), getCliente().getDigito(), pin); // Llama Servicio

            } 
            else {
                logger.debug("[auntentica default] rut["+getCliente().getRut()+"] va a checkPinCorporativo ");
                vp = ((ServiciosAutenticacion)servAuthHandle.getEJBObject()).checkPinCorporativo(canal.getPinID(), Long.parseLong(getRutUsuario()), getDvUsuario().charAt(0), pin); // Llama Servicio
                logger.debug("[auntentica default] rut["+getCliente().getRut()+"] regreso de checkPinCorporativo");

            }
            this.indCambioClave = vp.cambioClave;
            this.cic = vp.identInternaBCI;
            this.primeraVez = vp.primeraConexion;
            this.restriccion = vp.restriccionClave;
            this.tipoUser = vp.tipoUsuario;
            //Agregado para el proyecto de cambio de clave con validacion de pan
            this.setIndCtaCorrentista(vp.getEsCuentaCorrentista());
            cliente.setDatosBasicos();
            logger.debug("[auntentica default] rut["+getCliente().getRut()+"] sale de auntentica default "
                    +this.indCambioClave);
            break;
        }
    }

    /**
     * M�todo que autentifica la Sesion.
     *
     * Registro de versiones:<ul>
     * <li>1.0 (??/??/????, Desconocido): version inicial
     * <li>1.1 (20/06/2006, Paola Parra (SEnTRA)): Se agrega log para cuando se trata de autentificar token
     * <li>1.2  08/03/2013, Carlos Cerda I. (Kubos): Se cambia clase de logueo por Log4j.
     * <li>1.3  20/04/2015 Rodrigo Troncoso Balboa (ImageMaker IT): Se cambia la validacion checkPinCorporativo
     * del ejb ServiciosSeguridad al ejb ServiciosAutenticacion para no validar logica de Clave RedBanc.</li>
     * 
     * </ul>
     *
     * @param pin : clave token
     * @param canal : canal por el cual entr� el cliente
     * @throws TuxedoException excepcion de tuxedo
     * @throws SeguridadException excepcion de seguridad
     * @throws GeneralException excepcion general
     * @throws RemoteException excepcion remota
     * @throws Exception excepcion
     * 
     * @since 2.2
     */

    public void autentica(String pin, String canal) throws TuxedoException, SeguridadException, 
            GeneralException, RemoteException, Exception {

        //Si el canal es de Token, autentica a trav�s de este medio
        if (canal.equalsIgnoreCase("token")) {
            logger.debug("[autentica] rut["+getCliente().getRut()+"] va a autenticaToken");
            autenticaToken(pin);
            logger.debug("[autentica] rut["+getCliente().getRut()+"] volvio de autenticaToken");
            return;
        }

        ValidaPinCorp vp = null;



        logger.debug("[autentica] rut["+getCliente().getRut()+"] autentica2 - canal                  : [" 
                + canal + "]");
        logger.debug("[autentica] rut["+getCliente().getRut()+"] autentica2 - getCanal().getCanalID(): [" 
                + getCanal().getCanalID() + "]");
        logger.debug("[autentica] rut["+getCliente().getRut()+"] autentica2 - getRutUsuario()        : [" 
                + getRutUsuario() + "]");

        if ((getRutUsuario() != null) && ( "230".equals(getCanal().getCanalID()) || "230".equals(canal))) {
            logger.debug("[autentica] rut["+getCliente().getRut()+"] canal [" + canal + "]");
            vp = ((ServiciosAutenticacion)servAuthHandle.getEJBObject()).checkPinCorporativo(canal, Long.parseLong(getRutUsuario()), getDvUsuario().charAt(0), pin); // Llama Servicio
        }
        else {
            logger.debug("[autentica] rut["+getCliente().getRut()+"] canal [" + canal + "]");
            vp = ((ServiciosAutenticacion)servAuthHandle.getEJBObject()).checkPinCorporativo(canal, getCliente().getRut(), getCliente().getDigito(), pin); // Llama Servicio
        }
        this.indCambioClave = vp.cambioClave;
        this.cic = vp.identInternaBCI;
        this.primeraVez = vp.primeraConexion;
        this.restriccion = vp.restriccionClave;
        this.tipoUser = vp.tipoUsuario;
        //Agregado para el proyecto de cambio de clave con validacion de pan
        this.setIndCtaCorrentista(vp.getEsCuentaCorrentista());
        cliente.setDatosBasicos();
        logger.debug("[autentica] rut["+getCliente().getRut()+"] fin de auntentica");
    }

    /**
     * Autentifica la sesi�n del usuario cuando este tiene su clave expirada.
     * <p>
     * Nota: Es igual que el m�todo autentica(String pin, String canal) con la
     *       �nica diferencia que no retorna excepci�n cuando la clave est�
     *       expirada. Este m�todo se cre� s�lo para la clave dos (canal 120)
     *       aunque en el futuro puede ser usada por otros canales.
     * <p>
     * Registro de versiones:<ul>
     *
     * <li>1.0 (14/03/2004, Mart�n Maturana): Versi�n Inicial
     *
     * <li>1.1 (02/02/2007, Pedro Carmona (SEntra): Se agrega unas l�neas de log para detectar un error se est� produciendo.
     * <li>1.2 (13/04/2009, Jaime Pezoa N��ez (BCI): Si el canal (PinID) consultado es 130, se debe llamar al m�todo
     *                      checkPinCorporativo con el Rut y d�gito verificador del Usuario conectado, y no con el Rut de
     *                      la Empresa.
     * <li>1.3 (23/04/2012, Jaime Pezoa N��ez (BCI): Se agrega el canal 132 (Pyme) para que tenga la misma l�gica que empresas.
     * <li>1.4  08/03/2013, Carlos Cerda I. (Kubos): Se cambia clase de logueo por Log4j.
     * <li>1.5  20/04/2015 Rodrigo Troncoso Balboa (ImageMaker IT): Se cambia la validacion checkPinCorporativo
     * del ejb ServiciosSeguridad al ejb ServiciosAutenticacion para no validar logica de Clave RedBanc.</li>
     *
     * </ul>
     * <p>
     * @param pin Corresponde a la clave del cliente que se quiere autenticar.
     * @param canal Canal de la clave del cliente a autenticar.
     * @throws wcorp.util.com.TuxedoException
     * @throws wcorp.serv.seguridad.SeguridadException
     * @throws wcorp.util.GeneralException
     * @throws java.rmi.RemoteException
     * @throws java.lang.Exception
     * @since 1.2
     */
    public void autenticaClaveExp(String pin, String canal) throws TuxedoException, 
            SeguridadException, GeneralException, RemoteException, Exception {

        ValidaPinCorp vp = null;

        try {
            logger.debug("[autenticaClaveExp default] rut [" + getCliente().getRut() + "] canal [" 
                    + canal + "] antes chequeo.");

            if((this.canal.getCanalID().equalsIgnoreCase("230") || this.canal.getCanalID().equalsIgnoreCase("132")) && canal.equals("130") && !this.indCambioClave && !this.primeraVez && getRutUsuario()!= null){
                vp = ((ServiciosAutenticacion)servAuthHandle.getEJBObject()).checkPinCorporativo(canal, Long.valueOf(getRutUsuario()).longValue(), getDvUsuario().charAt(0), pin); // Llama Servicio
                logger.debug("canal 132 y 230");
            }
            else{
                vp = ((ServiciosAutenticacion)servAuthHandle.getEJBObject()).checkPinCorporativo(canal, getCliente().getRut(), getCliente().getDigito(), pin); // Llama Servicio
            }
            logger.debug("[autenticaClaveExp default] rut " + getCliente().getRut() + "terminado.");
            this.indCambioClave = vp.cambioClave;
            this.cic = vp.identInternaBCI;
            this.primeraVez = vp.primeraConexion;
            this.restriccion = vp.restriccionClave;
            this.tipoUser = vp.tipoUsuario;
            //Agregado para el proyecto de cambio de clave con validacion de pan
            this.setIndCtaCorrentista(vp.getEsCuentaCorrentista());
            cliente.setDatosBasicos();

        }
        catch (SeguridadException se){
            logger.debug("[" + getCliente().getRut() + "]" + " Cliente canal " + canal + " con error [" 
                    + se.getCodigo() + "]");
            if(se.getCodigo().equals("0433")){
                logger.debug("[" + getCliente().getRut() + "]" + " Cliente canal " 
                        + canal + " con clave expirada");
            }
            else{
                logger.debug("[autenticaClaveExp] rut " + getCliente().getRut() + "] retorno por otra excepci�n.");
                throw se;
            }
        }
        logger.debug("[autenticaClaveExp] rut " + getCliente().getRut() + "]" + " retorno ok");
    }

    /**
     *  M�todo que Autentica a trav�s de Token
     *
     * Registro de versiones:<ul>
     * <li>1.0 (??/??/????, Desconocido): version inicial
     * <li>1.1 (20/06/2006, Paola Parra (SEnTRA)): Se agrega log para cuando se trata de autentificar token
     * <li>1.2 (27/06/2006, Paola Parra (SEnTRA)): Se agrega manejo de excepciones al momento de autenticar token
     * <LI>1.3 08/04/2010 Magdalena D�az (ImageMaker IT): Se capturan y manejan las excepciones para TuxedoException y RemoteException.
     * <LI>1.4 28/04/2010 Meng-Hsiung Chao (ImageMaker IT)): Se agrega excepcion para autenticas softtoken RSA
     * <LI>1.5 29/06/2010 Meng-Hsiung Chao (ImageMaker IT)): Se manejan excepciones para compatibilidad softtoken RSA e integraci�n de tabla de par�metros Softtoken.parametros
     * <li>1.3 13,10,2010 Pablo Gavil�n Lara(Imagemaker IT): Se agrega un if donde se valida el tipo de dispositivo
     * <li>1.6 03/05/2011 Pablo Gavil�n Lara(Imagemaker IT): Se modifica la llamada a {@link ServiciosSeguridad#autenticar(SoftTokenTO)} por una llamada
     * a {@link SoftTokenHelper#autenticar(SoftTokenTO)}.</li>
     * <li>1.7 24/05/2012, H�ctor Hern�ndez Orrego. (SEnTRA) - Se incorpora validaci�n de canal 132, para 
     * autenticar token con el rut del usuario.</li>
     * <li>1.8  08/03/2013, Carlos Cerda I. (Kubos): Se cambia clase de logueo por Log4j.
     * </ul>
     *
     * @param passcode : clave token
     * @throws TuxedoException
     * @throws SeguridadException
     * @throws GeneralException
     * @throws RemoteException
     * @throws Exception
     * @since 2.2
     */

    private void autenticaToken( String passcode )
            throws TuxedoException, SeguridadException, GeneralException, RemoteException, Exception {

        long rut;
        char digito;
        if (getCanal().getCanalID().equals("132")){
            rut=Long.parseLong(getRutUsuario());
            digito=getDvUsuario().charAt(0);
        }else{
            rut=getCliente().getRut();
            digito=getCliente().getDigito();
        }

        try {

            String pin = TablaValores.getValor("Seguridad.parametros", "pinToken", getCanal().getCanalID());
            int largoSoftToken = -1;
            try
            {
                largoSoftToken = Integer.parseInt(TablaValores.getValor("Softtoken.parametros","largoToken",getCanal().getCanalID()));
            }   catch (Exception e)
            {
                largoSoftToken = -1;
                logger.debug("[autenticaToken] Error al abrir Softtoken.parametros (largoToken, Canal:"
                        +getCanal().getCanalID()+")");
            }

            logger.debug("[autenticaToken] rut["+rut+"] getCanal().getCanalID(): [" 
                    + getCanal().getCanalID() + "]");

            pin = (pin == null)?"":pin;

            if (passcode.length() == largoSoftToken)
            {
                logger.debug("[autenticaToken] Softtoken RSA rut["+rut+"] va a autenticaToken ["
                        +(digito + passcode)+"]");
                SoftTokenTO to = new SoftTokenTO();
                to.setRut(rut);
                to.setDvCliente(digito);
                to.setTokenCode1(passcode);

                try {
                    String cuenta = "";
                    if (getCliente() != null && getCliente().getCuentas() != null && getCliente().getCuentas().cuentas != null && getCliente().getCuentas().cuentas.length > 0) {
                        for (int i = 0; i < getCliente().getCuentas().cuentas.length && cuenta.equals(""); i++) {
                            Cta cta = getCliente().getCuentas().cuentas[i];
                            if (cta.getCodEstado() != null && cta.getCodEstado().equals("VIG")) {
                                cuenta = cta.getCuenta();
                            }
                        }
                    }
                    if (!cuenta.equals("")) {
                        to.setNumCtaCte(cuenta);    
                    }
                } catch (Exception e) {

                }
                SoftTokenHelper helper = new SoftTokenHelper();
                if (!helper.autenticar(to)) {
                    throw new wcorp.util.com.TuxedoException("TK10");
                }
            }
            else
            {
                logger.debug("[autenticaToken] rut["+rut+"] va a autenticaToken ["+(digito + pin + passcode)+"]");
                ((ServiciosSeguridad)servSegHandle.getEJBObject()).autenticaToken(rut, digito, pin + passcode);    
            }

            logger.debug("[autenticaToken] rut["+rut+"] volvio de autenticaToken");
            return;
        } catch (TuxedoException e) {
            logger.debug("[autenticaToken] rut["+rut+"] TuxedoException");
            ErroresUtil.extraeStackTrace(e);
            throw new wcorp.serv.seguridad.SeguridadException("TK10");
        } catch (SeguridadException e) {
            logger.debug("[autenticaToken] rut["+rut+"] SeguridadException");
            ErroresUtil.extraeStackTrace(e);
            throw new wcorp.serv.seguridad.SeguridadException(e.getCodigo());
        } catch (RemoteException e) {
            logger.debug("[autenticaToken] rut["+rut+"] RemoteException");
            ErroresUtil.extraeStackTrace(e);
            throw new wcorp.serv.seguridad.SeguridadException("TK10");
        }
    }

    /**
     * Asigna a la variable de sesi�n los m�todos de autenticaci�n disponibles
     * para el cliente y el estado en que est� cada uno:
     * '0': No autenticado en el login con el m�todo
     * '1': Autneticado en el login con el m�todo
     * Ejemplo:<br>
     * <p> ("TOK", '1'), significa que el cliente est� autenticado con Token
     * correctamente
     * <p>
     * Registro de versiones:<ul>
     *
     * <li>1.0  (??/??/????, Desconocido): version inicial
     * <li>1.1  (12/11/2004, Victor Urra (Novared)): Se agrego <p> ("TOKBLO", '1'), significa que el cliente tiene el token
     *                                               bloqueado con lo cual permite el ingreso al banco enlinea pero no deja
     *                                               efectuar transferencias.
     * <li>1.2 (20/06/2006, Paola Parra (SEnTRA)): Se agrega log para cuando se trata de autentificar token
     * <li>1.2 (29/09/2006, Victor Urra (Novared)): Se incorporo l�gica para la obtenci�n de un valor que identifica si un cliente
     *                                              debe realizar la autentificaci�n de multipass, este dato es obtenido de sybase SOLO cuando un
     *                                              cliente tiene su multipass en estado "ACTIVO", en <b>if (estadoToken.equalsIgnoreCase("TOKENST05"))</b>.
     *
     * <li>1.3 15/09/2011, Gustavo L�pez S. (Orand): Se agrega el parametro canal al m�todo para identificar desde donde se esta realizando la consulta.
     * <li>1.4 24/05/2012, H�ctor Hern�ndez Orrego. (SEnTRA) - Se incorpora validaci�n de canal 132, para 
     * para utilizar el rut del usuario en la funcionalidad del m�todo.</li>
     * <li>1.5  08/03/2013, Carlos Cerda I. (Kubos): Se cambia clase de logueo por Log4j.
     * <li>1.6 02/07/2013 Carlos Cerda I.(Kubos): Se consulta por el estado del dispositivo en Tandem, siempre y
     *   cuando este activa esta l�gica.   
     * <li>1.7 05/07/2013, Rodolfo Kafack Ghinelli. (SEnTRA): Se agrega metodos de autenticacion SafeSigner en
	 *  metodosAutenticacion.put(String, true).
     * <p>
     * @since 1.0
     */
    public void asignaMetodosAutenticacion()
            throws GeneralException, Exception, SeguridadException {
        String estadoToken = null;
        long rut;
        char digito;

        if (getCanal().getCanalID().equals("132")){
            rut=Long.parseLong(getRutUsuario());
            digito=getDvUsuario().charAt(0);
        }else{
            rut=getCliente().getRut();
            digito=getCliente().getDigito();
        }

        logger.debug("[asignaMetodosAutenticacion] rut["+rut+"] Inicia 'asignaMetodosAutenticacion()'");
		try {
			if (logger.isEnabledFor(Level.DEBUG)) {
				logger.debug("[asignaMetodosAutenticacion] getParametrosEstrategiaSegundaClaveTO()"
						+ getParametrosEstrategiaSegundaClaveTO());
			}
			if(logger.isEnabledFor(Level.DEBUG)){
				logger.debug("[asignaMetodosAutenticacion]invocando a ControllerBCI.obtenerEstadoSegundaClave");
			}
			boolean poseeSafeSigner = ControllerBCI
					.obtenerEstadoSegundaClave(getParametrosEstrategiaSegundaClaveTO());

			if (logger.isEnabledFor(Level.DEBUG)) {
				logger.debug("[asignaMetodosAutenticacion] Se agrega parametro para SafeSigner");
				logger.debug("[asignaMetodosAutenticacion] PoseeSafeSigner ["
						+ poseeSafeSigner + "]");
			}
			if (poseeSafeSigner) {
				if (logger.isEnabledFor(Level.DEBUG)) {
					logger.debug("[asignaMetodosAutenticacion]agrega parametro a metodosAutenticacion");
				}
				metodosAutenticacion.put("MPM", new Boolean(false));
			}
		}catch (Exception e) {
			if(logger.isEnabledFor(Level.DEBUG)){
				logger.debug("[asignaMetodosAutenticacion]No se pudo determinar si posee SafeSigner");
				logger.debug("[asignaMetodosAutenticacion]Desde ControllerBCI..");
			}
		}
        //Asigna por defecto m�todo de autenticaci�n "Clave Internet"
        logger.debug("[asignaMetodosAutenticacion] rut["
                +rut+"] Asigna 'Clave Internet' como medio de autenticaci�n");
        metodosAutenticacion.put("CLI", new Boolean(true));

        //Verifica por Token
        logger.debug("[asignaMetodosAutenticacion] rut["+rut+"] verificar� Token");
        try{
            boolean consultaEstadoTandem = new Boolean(TablaValores.getValor(
                    TABLA_AUTHENTICATION, "TANDEM", "FLAG_LOGIN")).booleanValue();

            logger.debug("[asignaMetodosAutenticacion] rut["+rut+"] consultara a tandem: " + consultaEstadoTandem);

            boolean esCanalBackOffice = esCanalBackOffice( getCanal().getCanalID());
            logger.debug("[asignaMetodosAutenticacion] rut["+rut+"] esCanalBackOffice: " + esCanalBackOffice);

            if( consultaEstadoTandem && !esCanalBackOffice ) {
                estadoToken = ((ServiciosSeguridad)servSegHandle.getEJBObject()).determinaEstadoToken( 
                        new UsuarioTO(rut,digito, TipoBancaEnum.BCI_PERSONA_TBANC_NOVA ) );
            }
            else {
                estadoToken = ((ServiciosSeguridad)servSegHandle.getEJBObject()).determinaEstadoToken(rut, digito);
            }
        }catch(Exception se){
            logger.debug("[asignaMetodosAutenticacion] rut["+rut+"] Exception12 : " + se.toString());
            throw se;
        }

        logger.debug("[asignaMetodosAutenticacion] rut["+rut+"] verificar� valores del estado del Token");
        logger.debug("[asignaMetodosAutenticacion] rut["+rut+"] estadoToken["+estadoToken+"]");

        if (estadoToken.equalsIgnoreCase("TOKENST01") ||
                estadoToken.equalsIgnoreCase("TOKENST02") ||
                estadoToken.equalsIgnoreCase("TOKENST03") ) {
            //No tiene token o no es necesario solicitarlo
        } else if (estadoToken.equalsIgnoreCase("TOKENST05")) {
            //se pide token
            metodosAutenticacion.put("TOK", new Boolean(false));

            logger.debug("Cliente con multipass, procedo a verificar si tiene multipass opcional");
            try{
                String canal=this.getCanal().getCanalID();
                logger.debug("[asignaMetodosAutenticacion] rut["+rut+"] canal["+canal+"]");
                String preferenciaTabla=TablaValores.getValor("Seguridad.parametros",canal+"-MultipassOpcional","consultaPreferenciaCliente")!=null?TablaValores.getValor("Seguridad.parametros",canal+"-MultipassOpcional","consultaPreferenciaCliente"):"";
                logger.debug("[asignaMetodosAutenticacion] rut["+rut+"] preferenciaTabla["+preferenciaTabla+"]");
                boolean consultaPreferenciaCliente=preferenciaTabla.equalsIgnoreCase("si");
                if(consultaPreferenciaCliente){
                    TokenDelegate delegate = new TokenDelegate();
                    boolean respuesta = delegate.consultaOpcionalidadToken(rut,String.valueOf(digito), canal);
                    logger.debug("[asignaMetodosAutenticacion] rut["+rut+"] respuesta consultaOpcionalidadToken("
                            +respuesta+")");
                    if(!respuesta)
                        setAttrib("permitirAutentificacion",""+respuesta);
                }else{
                    logger.debug("[asignaMetodosAutenticacion] rut["+rut+"] se solicita token al cliente");
                    setAttrib("permitirAutentificacion",""+false);
                }

            }catch(SeguridadException Se){
                logger.debug(ErroresUtil.extraeStackTrace(Se));
            }


        } else if (estadoToken.equalsIgnoreCase("TOKENST06") ||
                estadoToken.equalsIgnoreCase("TOKENST07")) {
            //Token bloqueado

            //Identifica que el token esta Bloqueado, esto permite no realizar transferencia peri si ingresar al banco
            //Enlinea.
            metodosAutenticacion.put("TOKBLO", new Boolean(false));

        } else  if (estadoToken.equalsIgnoreCase("TOKENST04") ){
            metodosAutenticacion.put("ACTTOK", new Boolean(false));
        }



        //Verifica por Passcode
        logger.debug("[asignaMetodosAutenticacion] rut["+rut+"] verificar� si posee Passcode");
        if (!getCanal().getCanalID().equals("150")) {
            try {
                EstadoPin respuestaEstado = ((ServiciosSeguridad)servSegHandle.getEJBObject()).estadoPin("01",
                        getCliente().getRut(),
                        getCliente().getDigito(),
                        "12");

                if (respuestaEstado != null){
                    logger.debug("[asignaMetodosAutenticacion] rut["+rut+"] Tiene Passcode en estado [" 
                            + respuestaEstado.estado + "]");
                    metodosAutenticacion.put("PAS", new Boolean(false));
                } else {
                    logger.debug("[asignaMetodosAutenticacion] rut["+rut+"] No tiene Passcode");
                }

            } catch (GeneralException ge) {
                if (ge.getCodigo().equals("0438")) {
                    logger.debug("[asignaMetodosAutenticacion] rut["+rut+"] No tiene Passcode - Exc. 0438");
                } else {
                    throw new GeneralException("ESPECIAL", ge.getMessage());
                }
            } catch (Exception e) {
                e.printStackTrace();
                // No pudo encontrar el bean o bien hubo error al localizarlo
                // en el arbol JNDI de weblogic.
                throw new GeneralException("ESPECIAL", e.getMessage());
            }
        }

        return;
            }

    /**
     * M�todo utilizado para saber si un canal en BackOffice.
     * 
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 24/06/2013 Carlos Cerda I. (Kubos): versi�n inicial.
     * </ul>
     * <p>
     * @param aCanalID canal.
     * @return boolean con la respuesta.
     * @since 1.0
     */
    private boolean esCanalBackOffice( String aCanalID) {
        String[] canales = StringUtil.divide(
                TablaValores.getValor(TABLA_AUTHENTICATION, "CANALES", "CANALES_BACK_OFFICE"), ',');
        return StringUtil.estaContenidoEn(aCanalID, canales, true);
    }

    /**
     * Retorna los m�todosde autenticaci�n disponibles
     * @return
     */
    public Hashtable getMetodosAutenticacion() {
        return this.metodosAutenticacion;
    }

    /**
     * Retorna los m�todosde autenticaci�n disponibles
     * @return
     */
    public void setMetodosAutenticacion(Hashtable seguridad) {
        this.metodosAutenticacion = seguridad;
    }

    public boolean cambiaEstadoMetodoAutenticacion(String key, boolean value) {

        if (this.metodosAutenticacion.get(key) == null) {
            return false;
        }
        this.metodosAutenticacion.remove(key);
        this.metodosAutenticacion.put(key, new Boolean(value));
        return true;
    }

    /**
     * Carga los productos del cliente
     * Registro de versiones:<ul>
     * <li>1.0  (�?, Desconocido): versi�n inicial.
     * <li>2.0  (21/06/2006, Luis Cruz (ImageMaker IT)): versi�n inicial.
     * <li>2.1  (13/04/2009, Jaime Pezoa N��ez (BCI)): Se modifica nombre de variable
     *           enum por enumera.
     * </ul>
     * 
     * @param agregaTarjeta boolean si debe agregar las tarjetas de nexus.
     * @throws GeneralException excepci�n lanzada por el m�todo.
     * @throws RemoteException excepci�n lanzada por el m�todo.
     * @since 1.0
     */
    private void cargaProductos(boolean agregaTarjeta) throws GeneralException, RemoteException {
        Operacion[] listaOp = null;

        listaOp = cliente.getOperaciones(this.tipoUser, null, null);

        if(agregaTarjeta){
            TCreditoOnlineSessionCache cache = new TCreditoOnlineSessionCache();
            listaOp = cache.reemplazaTDC(this, listaOp);
        } 


        Hashtable aux = new Hashtable();
        if (listaOp!=null) { // Tiene Operaciones
            // Crea tabla con lista de Operaciones por tipo
            int totOp = listaOp.length;
            for (int i=0; i<totOp;i++) {
                String cod = listaOp[i].tipoOperacion;
                Operacion[] ops = (Operacion[])aux.get(cod);
                if (ops==null) { // es primer caso
                    Operacion[] newOps = new Operacion[1];
                    newOps[0] = listaOp[i];
                    aux.put(cod, newOps);
                } else { // ya existian
                    int pre = ops.length;
                    Operacion[] newOps = new Operacion[pre+1];
                    for (int j=0; j<pre; j++) newOps[j]=ops[j];
                    newOps[pre]=listaOp[i];
                    aux.put(cod, newOps);
                }
            }

            // Incorpora Objetos como parte de la sesion
            for (Enumeration enumera = aux.keys(); enumera.hasMoreElements(); ) {
                String cod = (String)enumera.nextElement();
                Operacion[] objetos = (Operacion[])aux.get(cod);
                setAttrib(cod,objetos);
            }
        }

    }

    /**
     * M�todo que autoriza la Sesi�n y para determinados canales carga los productos del cliente.
     * <p>
     * 
     * Registro de versiones:<UL>
     *
     * <li>1.0 ??/??/????, ??? ??? ??? (???): Versi�n Inicial.
     * <li>1.0 07/03/2013, Carlos Cerda I. (Kubos): Se reemplaza switch por secuencia if-else y se agrega caso 
     * cuando el canal pertenezca a un listado invocar� al m�todo {@link #cargaProductos(boolean)}} con false.
     *
     * </UL>
     * <p>
     * 
     * @exception GeneralException excepci�n lanzada por el m�todo.
     * @exception NamingException excepci�n lanzada por el m�todo.
     * @exception RemoteException excepci�n lanzada por el m�todo.
     * @exception CreateException excepci�n lanzada por el m�todo.
     * @exception Exception excepci�n lanzada por el m�todo.
     * @since ?
     */
    public void autoriza() throws GeneralException, NamingException, RemoteException, CreateException, Exception {

        if(canal.getPinID().equals(String.valueOf(PINID_INTRA))){
            auyp = new AutenticaUyP(colab);
            auyp.autoriza();
        }
        else if(TablaValores.getValor("tcreditoonline.parametros", 
                "tarjetasOnlineEnLogin", "canales").indexOf(canal.getPinID()) > -1){
            logger.debug("[autoriza] No se cargaran tarjetas de credito desde nexus");
            cargaProductos(false); 
        }
        else{
            logger.debug("[autoriza] Se cargaran tarjetas de credito desde nexus");
            cargaProductos(true);
        }

        String file = canal.getPathTablas() + canal.getDispositivo() + ".servicios.parametros";
        servicios = ((ServiciosSeguridad)servSegHandle.getEJBObject()).autoriza((SessionBCI)this, file);
        
        //borrar
        String[] tmpServ =  getServicios();
        // fin borrar
    }


    /**
     * Cambia la clave del usuario y retorna en un par�metro por referencia
     * cuales de los m�todos internos fueron ejecutados.
     * <p>
     * Nota: Este m�todo es una sobrecarga del m�todo cambioClave(String pin, String newPin)
     *       puesto que se le agreg� un nuevo par�metro llamado metodosEjecutados.
     *       El m�todo llama a varios otros m�todos que pueden arrojar excepciones, pero
     *       cambioClave no retorna informaci�n sobre los m�todos ejecutados satisfactoriamente.
     *       El par�metro metodosEjecutados permite a cambioClave entregar una lista con los
     *       m�todos llamados que no arrojaron excepciones.
     *
     * <p>
     * Registro de versiones:<ul>
     *
     * <li>1.0 (07/04/2004, Hugo Villavicencio (Apolo)): Versi�n Inicial
     *
     * </ul>
     *  <ul>
     * <li>2.0, 2005-11-11, Aurora Leyton Benitez, SEnTRA. Se modifica:<ul>
     *                <li>Se lee atributo de session si es clinocnv para autentificar la clave con canal 110
     * <li>2.1  08/03/2013, Carlos Cerda I. (Kubos): Se cambia clase de logueo por Log4j.
     * </ul>
     * </ul>
     * <p>
     * @param pin Corresponde a la clave antigua del cliente que la quiere cambiar.
     * @param newPin Corresponde a la nueva clave a almacenar.
     * @param metodosEjecutados Corresponde a una clase que almacena cuales de los
     *        m�todos internos de este m�todo fueron ejecutados. Los valores posibles son:
     *        EXEC_AUTENTICA
     *        EXEC_AUTENTICAUYP
     *        EXEC_AUTORIZA_UYP
     *        EXEC_CAMBIOCLAVE
     *        EXEC_CAMBIAPINCORPORATIVO
     *        EXEC_AUTORIZAUYP
     * @exception wcorp.serv.seguridad.SeguridadException
     * @exception wcorp.util.GeneralException
     * @exception java.rmi.RemoteException
     * @exception java.lang.Exception
     * @since 1.1
     */
    public void cambioClave(String pin, String newPin,
            NumberWraper metodosEjecutados) throws SeguridadException,
            GeneralException, RemoteException, Exception {

        int iAux= 0;
        if (metodosEjecutados != null) {
            iAux= metodosEjecutados.intValue();
        }
        logger.debug("cambioClave. iAux : "+iAux);
        String bcolinea= (String) this.getAttrib("clinocnv");
        logger.debug("antes bcolinea"+ " " + bcolinea);
        if ( bcolinea!=null && bcolinea.equals("1")) {
            logger.debug("clientes banco en l�nea");
            autentica(pin,"110"); // Autentifica la Sesion
        } else   autentica(pin);
        //autentica(pin);
        iAux|= EXEC_AUTENTICA;
        if (metodosEjecutados != null) {
            metodosEjecutados.set(iAux);
        }

        switch (Integer.parseInt(canal.getPinID())) {
        case PINID_INTRA:
            auyp= new AutenticaUyP(colab);
            iAux|= EXEC_AUTENTICAUYP;
            if (metodosEjecutados != null) {
                metodosEjecutados.set(iAux);
            }

            auyp.cambioClave(pin.toUpperCase(), newPin.toUpperCase());
            iAux|= EXEC_CAMBIOCLAVE;
            if (metodosEjecutados != null) {
                metodosEjecutados.set(iAux);
            }

            this.indAccesoPermitido= auyp.indAccesoPermitido;
            this.indCambioExitoso= auyp.indCambioExitoso;
            this.indClaveDuplicada= auyp.indClaveDuplicada;
            this.indClaveExpirada= auyp.indClaveExpirada;
            this.indClaveRevocada= auyp.indClaveRevocada;
            this.indCambioClave= auyp.indCambioClave;
            auyp.autoriza();
            iAux|= EXEC_AUTORIZAUYP;
            if (metodosEjecutados != null) {
                metodosEjecutados.set(iAux);
            }
            break;
        default:
            // Termino modificacion Carcos
            //modificaci�n aurora
            logger.debug("antes bcolinea"+ " " + bcolinea);
            if ( bcolinea!=null && bcolinea.equals("1")) {
                logger.debug("clientes banco en l�nea");
                ((ServiciosSeguridad)servSegHandle.getEJBObject()).cambiaPinCorporativo("110", getCliente().getRut(), getCliente().getDigito(), pin, newPin);
            } else  if (canal.getPinID().equals("130") && getRutUsuario() != null){
                logger.debug("Cambiando Clave a Rut Usuario : " + getRutUsuario());
                ((ServiciosSeguridad)servSegHandle.getEJBObject()).cambiaPinCorporativo(canal.getPinID(), Long.parseLong(getRutUsuario()), getDvUsuario().charAt(0), pin, newPin);
            } else {
                logger.debug("clientes normales");
                ((ServiciosSeguridad)servSegHandle.getEJBObject()).cambiaPinCorporativo(canal.getPinID(), getCliente().getRut(), getCliente().getDigito(), pin, newPin);
            }
            iAux|= EXEC_CAMBIAPINCORPORATIVO;
            if (metodosEjecutados != null) {
                metodosEjecutados.set(iAux);
            }
        }

    }

    /**
     * Cambia la clave del usuario.
     * <p>
     * Registro de versiones:<ul>
     *
     * <li>1.0 (??/??/????, Desconocido): Versi�n Inicial
     *
     * <li>1.1 (07/04/2004, Hugo Villavicencio (Apolo)): La l�gica de este m�todo
     *          se puso en otro m�todo cambioClave sobrecargado con un nuevo
     *          par�metro. Por lo tanto este m�todo lo �nico que hace ahora es
     *          llamar al nuevo m�todo sobrecargado con el nuevo par�metro.
     * </ul>
     * <p>
     * @param pin Corresponde a la clave antigua del cliente que la quiere cambiar.
     * @param newPin Corresponde a la nueva clave a almacenar.
     * @exception wcorp.serv.seguridad.SeguridadException
     * @exception wcorp.util.GeneralException
     * @exception java.rmi.RemoteException
     * @exception java.lang.Exception
     * @since 1.0
     */
    public void cambioClave(String pin, String newPin)
            throws SeguridadException, GeneralException, RemoteException, Exception {

        cambioClave(pin, newPin, (NumberWraper) null);
    }


    /**
     * Cambia la clave del usuario dado un canal.
     * <p>
     * Registro de versiones:<ul>
     *
     * <li>1.0 (??/??/????, Desconocido): Versi�n Inicial
     *
     * <li>1.1 (14/03/2005, Mart�n Maturana): Se llama al nuevo m�todo
     *          autenticaClaveExp cuando el canal es 120 (clave passcode).
     *
     * <li>1.2 (28/06/2006, Claudio P�rez Bravo (SEnTRA)): se agrego l�gica para controlar las validaciones y excepciones
     * que se producen en claves internet expiradas y asi proveer la posibilidad de actualizar la fecha de expiraci�n
     * en el cambio de la clave, estas acciones se realizan solamente cuando la conexi�n no es la primera que realiza el cliente.
     *
     * <li>1.3 (02/02/2007, Pedro Carmona (SEnTRA)): Se agregan unas l�neas de log's para poder detectar un problema de autenticaci�n.
     *
     * <li>1.4 (13/04/2009, Jaime Pezoa N��ez (BCI): Si el canal (PinID) consultado es 130, se debe llamar al m�todo cambiaPinCorporativo
     *                      con el Rut y d�gito verificador del Usuario conectado, y no con el Rut de la Empresa.
     *                      
     * <li>1.5 (23/04/2012, Jaime Pezoa N��ez (BCI): Se agrega el canal 132 (Pyme) para que tenga la misma l�gica que empresas.
     * </ul>
     * <p>
     * @param pin Corresponde a la clave antigua del cliente que la quiere cambiar.
     * @param newPin Corresponde a la nueva clave a almacenar.
     * @param canal Corresponde al canal que pertenece el cliente.
     * @exception wcorp.serv.seguridad.SeguridadException
     * @exception wcorp.util.GeneralException
     * @exception java.rmi.RemoteException
     * @exception java.lang.Exception
     * @since 1.0
     */
    public void cambioClave(String pin, String newPin, String canal)
            throws SeguridadException, GeneralException, RemoteException, Exception {

        String verificaFecha = TablaValores.getValor("Seguridad.parametros", "verificaFecha"+canal, "valor") != null ?
                TablaValores.getValor("Seguridad.parametros", "verificaFecha"+canal, "valor") : "false";
                logger.debug("[cambioClave]:: Omite fecha de expiracion para canal ?:"+verificaFecha );
                boolean verifica =  new Boolean(verificaFecha).booleanValue();

                if(canal.equals("120") || verifica) {
                    logger.debug("[" + getCliente().getRut() + "]" + " Entra a autenticaClaveExp con canal " 
                            + canal);
                    autenticaClaveExp(pin, canal);
                    logger.debug("[cambioClave]:: rut " 
                            + getCliente().getRut() + " Retorno de autenticaClaveExp()");
                } else {
                    logger.debug("[cambioClave]:: rut " + getCliente().getRut() + " Entra a autentica() con canal"
                            + canal);
                    autentica(pin, canal);
                    logger.debug("[cambioClave]:: rut " 
                            + getCliente().getRut() + " Retorno de autenticaClaveExp()");
                }

                logger.debug("[" + getCliente().getRut() + "]" + " Entra a cambiaPinCorporativo con canal " 
                        + canal);

                if ((this.canal.getCanalID().equalsIgnoreCase("230") || this.canal.getCanalID().equalsIgnoreCase("132")) && canal.equals("130") && !this.indCambioClave && !this.primeraVez && getRutUsuario()!= null){
                    logger.debug("canal 132 y 230");
                    ((ServiciosSeguridad)servSegHandle.getEJBObject()).cambiaPinCorporativo(canal, Long.valueOf(getRutUsuario()).longValue(), getDvUsuario().charAt(0), pin, newPin); // Llama Servicio
                }else{
                    ((ServiciosSeguridad)servSegHandle.getEJBObject()).cambiaPinCorporativo(canal, getCliente().getRut(), getCliente().getDigito(), pin, newPin); // Llama Servicio
                }
                logger.debug("[cambioClave default]:: rut "+ getCliente().getRut() + "]" 
                        + " Retorno de cambiaPinCorporativo con canal");
    }

    // Entrega la Identidad del Usuario
    public String getFullIdentity() {
        return cliente.getFullName();
    }

    /**
     * Registra Actividad en el Log mediante LogFile.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 ??/??/????, Desconocido: Versi�n Inicial.
     *
     * <li>1.1 12/05/2014 Francisco Mu�oz Arellano (BCI): Se comenta c�digo contenido en m�todo
     * para evitar conexiones al servidor ya que el archivo de log  no existe, pero se mantiene
     * su firma para evitar impactar los servicios de los diferentes canales que lo invocan.
     * </li>
     *
     * </ul>
     * <p>
     * @param act Registro de actividad en el log.
     * @since 4.0
     */
    public void journalizar(String act) {
    	/*
        String fileName = new SimpleDateFormat("ddMMyyyy").format(new Date());
        fileName = canal.getPathLog() + canal.getDispositivo() + "/" + "Journal." + fileName;
        LogFile lf = new LogFile(this, fileName);
        String msg;
        if (cliente != null)
            msg = "[" + "SessionBCI"+ "] [" + ip + "] [" + act + "] [" + cliente.getFullRut() + "] [" + cliente.getFullName() + "]";
        else if (colab != null)
            msg = "[" + "SessionBCI"+ "] [" + ip + "] [" + act + "] [" + colab.getUsuario() + "]";
        else
            msg = "[" + "SessionBCI"+ "] [" + ip + "] [" + act + "] [" + "ERROR: SIN DEFINICION DE ACTORES!!!";
        lf.writeToLog(this, msg);
    	*/
    }

    // Registra Actividad en el Log de
    public void logging(String act) {

    }

    public void cerrar() {
        activa = false;
    }

    protected void expiracion() {
        boolean st = false;
        // Caso normal
        if (canal.getTimeOutUso() > 0) { // TimeOut de Uso
            Calendar ultUsoPlusTimeOut = Calendar.getInstance();
            ultUsoPlusTimeOut.setTime(ultimaFechaAcceso);
            ultUsoPlusTimeOut.add(Calendar.MINUTE, canal.getTimeOutUso());

            Calendar ahora = Calendar.getInstance();
            ahora.setTime(new Date());

            if (ahora.after(ultUsoPlusTimeOut)) {
                activa = false;
            }
        }
        if (canal.getTimeOutTotal() > 0) { // TimeOut de Sesion Total
            Calendar inicioPlusTimeOut = Calendar.getInstance();
            inicioPlusTimeOut.setTime(fechaCreacion);
            inicioPlusTimeOut.add(Calendar.MINUTE, canal.getTimeOutTotal());

            Calendar ahora = Calendar.getInstance();
            ahora.setTime(new Date());

            if (ahora.after(inicioPlusTimeOut)) {
                activa = false;
            }
        }
    }

    public boolean getUyPAccess(String sistema, String puerta){
        return auyp.getUyPAccess(sistema, puerta);
    }

    // Retorna un Folio Global de operacion en el Middleware
    public long getFolio(Object clase, String aplic,  String operacion, String detalles) {
        try {
            return ((ServiciosMiscelaneos)servMiscHandle.getEJBObject()).getNumeroFolio(clase.getClass().getName(), aplic, operacion, "["+cliente.getFullRut()+"] "+this.getFullIdentity(), detalles);
        } catch (Exception ex) {
            return -1;
        }
    }

    /**
     * @param estado Los estados son:
     *  00 : Reseteo Clave
     *  01 : Bloqueo clave
     *  02 : Clave Restringida
     *  03 : Elimina Clave
     * @param motivo Motivo del cambio de estado:
     * 04
     */
    public void cambioEstadoClave(String estado, String motivo) throws SeguridadException, GeneralException, RemoteException, Exception {

        String grupoCanal = canal.getGrupoCanal();
        ((ServiciosSeguridad)servSegHandle.getEJBObject()).cambiaEstadoPin(grupoCanal, cliente.getRut(), cliente.getDigito(), null,
                estado, motivo, " ", " ");

    }



    /**
     * Permite accesar a la lista de puertas permitidas para un usuario
     *
     * @return Clase Hashtable, que almecena el sistema y la puerta en cuesti�n en cada registro
     * @since 13/05/2002
     * @author Claudia Arcos
     * @version 1.0
     */

    public Hashtable getPuertas() throws GeneralException{
        return auyp.getListaPuertas();
    }

    /**
     * getIndCtaCorrentista()
     * @return retorna si es cuenta correntista o no
     * @since 30/07/2004
     */
    public boolean getIndCtaCorrentista() {
        return indCtaCorrentista;
    }

    /**
     * setIndCtaCorrentista()
     * @param b identifica si el cliente es cuenta Correntista
     * @since 30/07/2004
     */
    public void setIndCtaCorrentista(boolean b) {
        this.indCtaCorrentista = b;
    }

    /**
     * setIdSession()
     * @param b identifica si el id se la session del cliente
     * @since 13/12/2005
     */
    public String getIdSession(){
        return idSession;
    }

    /**
     * setIdSession()
     * @param b retorna el id se la session del cliente
     * @since 13/12/2005
     */
    public void setIdSession(String idSession){
        this.idSession = idSession;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setCic(String cic) {
        this.cic = cic;
    }

    public String getCic() {
        return cic;
    }

	public ParametrosEstrategiaSegundaClaveTO getParametrosEstrategiaSegundaClaveTO() {
		return parametrosEstrategiaSegundaClaveTO;
	}
	
	public void setParametrosEstrategiaSegundaClaveTO(
			ParametrosEstrategiaSegundaClaveTO parametrosEstrategiaSegundaClaveTO) {
		this.parametrosEstrategiaSegundaClaveTO = parametrosEstrategiaSegundaClaveTO;
	}


}