package wcorp.model.actores;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.Vector;

import javax.ejb.CreateException;
import javax.ejb.Handle;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.model.actores.cliente.ClientePersona;
import wcorp.model.actores.cliente.Direccion;
import wcorp.model.actores.cliente.DireccionEmail;
import wcorp.serv.acciones.AccionesException;
import wcorp.serv.acciones.ServiciosAcciones;
import wcorp.serv.acciones.ServiciosAccionesHome;
import wcorp.serv.clientes.Campanas;
import wcorp.serv.clientes.ClientesException;
import wcorp.serv.clientes.DatosBasicosCliente;
import wcorp.serv.clientes.InfoGlosaProductos;
import wcorp.serv.clientes.InfoRelevante;
import wcorp.serv.clientes.Operacion;
import wcorp.serv.clientes.Periodo;
import wcorp.serv.clientes.ProcesoNegocio;
import wcorp.serv.clientes.Rentabilidad;
import wcorp.serv.clientes.RentabilidadProd;
import wcorp.serv.clientes.RetornoTipCli;
import wcorp.serv.clientes.Riesgo;
import wcorp.serv.clientes.Riesgopar;
import wcorp.serv.clientes.ServiciosCliente;
import wcorp.serv.clientes.ServiciosClienteHome;
import wcorp.serv.clientes.vo.PerfilTbanc;
import wcorp.serv.cuentas.CtasAbonoTercAutorizadas;
import wcorp.serv.cuentas.CuentasException;
import wcorp.serv.cuentas.ListaCuentas;
import wcorp.serv.cuentas.ServiciosCuentas;
import wcorp.serv.cuentas.ServiciosCuentasHome;
import wcorp.serv.cuentas.TitularMandato;
import wcorp.serv.cuentasvinculadas.ServiciosCuentasVinculadas;
import wcorp.serv.cuentasvinculadas.ServiciosCuentasVinculadasHome;
import wcorp.serv.misc.InformacionReemplazo;
import wcorp.serv.misc.MiscelaneosException;
import wcorp.serv.misc.ServiciosMiscelaneos;
import wcorp.serv.misc.ServiciosMiscelaneosHome;
import wcorp.util.GeneralException;
import wcorp.util.StrUtl;
import wcorp.util.com.JNDIConfig;
import wcorp.util.com.TuxedoException;

/**
 * Objeto de Negocio. Representa a un Cliente
 *
 * Registro de versiones:<ul>
 *
 * <li>1.0 (xx/xx/xxxx,                     (xxxxx)): versi�n inicial
 * <li>1.1 (27/01/2004, Hugo Villavicencio (APOLO)): Modificaci�n para incluir nuevos campos trx B0241
 * <li>1.2 (27/01/2004, Esteban Landaeta D�az (SEnTRA)): Modificaci�n para incluir nuevos campos ocupados en 
 *          EscritorioBean
 * <li>1.3 (30/05/2005, Esteban Landaeta D�az (SEnTRA)): Se agrega nuevo atributo para el manejo del perfil TBanc.
 * <li>1.4 (12/12/2007, Daniel Zuniga Figueroa (TINet)): Se agrega m�todo getDeudaBci para consulta de Deuda Total
 * BCI del cliente.</li>
 * <li>1.5 (21/06/2011, Jorge G�mez O. (SEnTRA)): Se modifica el constructor, dejando de instanciar en este m�todo 
 *         los EJBs ServiciosCuentas, 
 *         ServiciosAcciones, ServiciosCuentasVinculadas y ServiciosMiscelaneos.
 *         Se crean los m�todos getInstanceServCtas(), getInstanceServAcc(), getInstanceServCueVin() 
 *         y getInstanceServMisc() para instanciar los EJBs ServiciosCuentas, ServiciosAcciones, 
 *         ServiciosCuentasVinculadas y ServiciosMiscelaneos respectivamente.
 *         Se modifican los m�todos getCuentas(), getDatosAcciones(), getCuentasCtes(), getCuentasPrimas(), 
 *         getOperacionesCompuestas(char, String, String), getOperaciones(char),
 *         getOperaciones(char, String, String), getCtasCargoAutorizMandato(), getCtasAbonoAutoriz(String), 
 *         getDatosBasicosBCI(), getMandatoCliente() y getEjecutivoReemplazante(String),
 *         agregando la invocaci�n al m�todo de instanciaci�n del EJB que corresponda.
 * <li>1.6 (06/03/2012, Eduardo Villagr�n Morales (Imagemaker)): Se agregan m�todos get y set para los atributos
 * 		   nombres, apellidoPaterno y apellidoMaterno.</li>
 * 
 * <li>1.7 20/07/2012 Miguel Quiroz H. (TINet): Se crea el metodo {@link #getDatosBasicosCliente()}. 
 * Es normalizada la javadoc de la clase para que cumpla con las normas del Banco.</li>
 * <li>1.8 (14/08/2012, Juan Jose Buendia (BCI)): Se modifica el metodo getFullName, para identificar si se debe 
 *         desplegar el nombre de 
 *         cliente en formato persona o empresa.</li>
 * <li>1.8 22/08/2012 Miguel Quiroz H. (TINet): Se crea el metodo {@link #consultaDatosGeneralesDeCliente()}.
 *          Es normalizada la javadoc de la clase para que cumpla con las normas del Banco.</li>
 * <li>1.9 07/01/2013 Juan Jose Buendia(BCI): Se valida en cada llamada antes de crear un nuevo objeto Cliente
 * </ul>
 *
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * <p>
 */
public class Cliente implements Serializable {

    /** 
    * N�mero de versi�n utilizado durante la serializaci�n. 
    */
   private static final long serialVersionUID = 4L;
   private static Logger logger = (Logger)Logger.getLogger(Cliente.class);
   
	private long rut;
	private char digitoVerif;
	public String nombres;
	public String apellidoPaterno;
	public String apellidoMaterno;
	public String direccion;
	public String fono;
	public String email;
	public String tipoUsuario;
	public String tipoAtencion;
    public String oficina;
    public String codEjecutivo;
    public RetornoTipCli tipCli;

    public String tipoBanca;
    public double rentaLiquida;
    public String segmento;
    public java.util.Date periodoSegmento;
    public String indConvenio;
    public String codSocioEconomico;
    public String actividad;
    private PerfilTbanc perfilTbanc;

	private wcorp.serv.acciones.DatosCliente datosBasicosAcciones=null;

    private Handle /*ServiciosCuentas*/ servCtasHandle; // Servicios de Cuentas
    private Handle /*ServiciosCliente*/ servCliHandle; // Servicios de Cuentas
    private Handle /*ServiciosAcciones*/ servAccHandle; // Servicios de acciones
    private Handle /*ServiciosCuentasVinculadas*/ servCueVinHandle; //Servicios Cuentas Vinculadas
    private Handle /*ServiciosMiscelaneos*/ servMiscHandle; // Servicios Miscelaneos
    /* Variable para los datos basicos del cliente*/
    private DatosBasicosCliente datosBasicosCliente;
    
    /**
     * Constructor de la clase.
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *   <li>1.0 ??/??/????, ????. (????): Versi�n inicial.</li> 
     *   <li>1.1 21/06/2011, Jorge G�mez O. (SEnTRA): Se dejan de instanciar los EJBs ServiciosCuentas, 
     *       ServiciosAcciones, ServiciosCuentasVinculadas y ServiciosMiscelaneos.</li>
     *  </ul>
     * </p>
     * @param  rut long que contiene el RUT del cliente
     * @param  digito char que contiene el d�gito verificador del cliente
     * @throws NamingException
     * @throws RemoteException  
     * @throws CreateException
     * @throws GeneralException  
     * @since 1.0
     */
    public Cliente(long rut,char dig) throws NamingException, RemoteException, CreateException, GeneralException {
    	this.rut=rut;
    	this.digitoVerif=String.valueOf(dig).toUpperCase().charAt(0);
    	if (servCliHandle==null){
    		InitialContext ic = JNDIConfig.getInitialContext();
    		ServiciosClienteHome homeCli= (ServiciosClienteHome)ic.lookup("wcorp.serv.clientes.ServiciosCliente");
    		servCliHandle = ((ServiciosCliente)homeCli.create()).getHandle();
    	}
    }

    /**
     * M�todo que instancia el EJB ServiciosCuentas.
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *   <li>1.0 21/06/2011, Jorge G�mez O. (SEnTRA): Versi�n inicial.</li>
     *  </ul>
     * </p>
     * @return instancia del EJB
     * @since 1.6
     */
    private Handle getInstanceServCtas() {
    	try{
    		if (servCtasHandle==null){
    			InitialContext ic = JNDIConfig.getInitialContext();
    			ServiciosCuentasHome homeCtas =(ServiciosCuentasHome)ic.lookup("wcorp.serv.cuentas.ServiciosCuentas");
    			servCtasHandle = ((ServiciosCuentas)homeCtas.create()).getHandle();
    		}
    	} catch (NamingException e) {
    		if(logger.isEnabledFor(Level.ERROR)){ logger.error("[getInstanceServCtas]::NamingException["+e.toString()+"]"); }
    	} catch (RemoteException e) {
    		if(logger.isEnabledFor(Level.ERROR)){ logger.error("[getInstanceServCtas]::RemoteException["+e.toString()+"]"); }
    	} catch (CreateException e) {
    		if(logger.isEnabledFor(Level.ERROR)){ logger.error("[getInstanceServCtas]::CreateException["+e.toString()+"]"); }
    	}
    	return servCtasHandle;
    }

    /**
     * M�todo que instancia el EJB ServiciosAcciones.
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *   <li>1.0 21/06/2011, Jorge G�mez O. (SEnTRA): Versi�n inicial.</li>
     *  </ul>
     * </p>
     * @return instancia del EJB
     * @since 1.6
     */
    private Handle getInstanceServAcc() {
    	try{
    		if (servAccHandle==null){
    			InitialContext ic = JNDIConfig.getInitialContext();
    			ServiciosAccionesHome homeAcc= (ServiciosAccionesHome)ic.lookup("wcorp.serv.acciones.ServiciosAcciones");
    			servAccHandle = ((ServiciosAcciones)homeAcc.create()).getHandle();
    		}
    	} catch (NamingException e) {
    		if(logger.isEnabledFor(Level.ERROR)){ logger.error("[getInstanceServAcc]::NamingException["+e.toString()+"]"); }
    	} catch (RemoteException e) {
    		if(logger.isEnabledFor(Level.ERROR)){ logger.error("[getInstanceServAcc]::RemoteException["+e.toString()+"]"); }
    	} catch (CreateException e) {
    		if(logger.isEnabledFor(Level.ERROR)){ logger.error("[getInstanceServAcc]::CreateException["+e.toString()+"]"); }
    	}
    	return servAccHandle;
    }
    
    /**
     * M�todo que instancia el EJB ServiciosCuentasVinculadas.
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *   <li>1.0 21/06/2011, Jorge G�mez O. (SEnTRA): Versi�n inicial.</li>
     *  </ul>
     * </p>
     * @return instancia del EJB
     * @since 1.6
     */    
    private Handle getInstanceServCueVin() {
    	try{
    		if (servCueVinHandle==null){
    			InitialContext ic = JNDIConfig.getInitialContext();
    			ServiciosCuentasVinculadasHome homeCvin= (ServiciosCuentasVinculadasHome)ic.lookup("wcorp.serv.cuentasvinculadas.ServiciosCuentasVinculadas");
    			servCueVinHandle = ((ServiciosCuentasVinculadas)homeCvin.create()).getHandle();
    		}
    	} catch (NamingException e) {
    		if(logger.isEnabledFor(Level.ERROR)){ logger.error("[getInstanceServCueVin]::NamingException["+e.toString()+"]"); }
    	} catch (RemoteException e) {
    		if(logger.isEnabledFor(Level.ERROR)){ logger.error("[getInstanceServCueVin]::RemoteException["+e.toString()+"]"); }
    	} catch (CreateException e) {
    		if(logger.isEnabledFor(Level.ERROR)){ logger.error("[getInstanceServCueVin]::CreateException["+e.toString()+"]"); }
    	}
    	return servCueVinHandle;
    }

    /**
     * M�todo que instancia el EJB ServiciosMiscelaneos.
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *   <li>1.0 21/06/2011, Jorge G�mez O. (SEnTRA): Versi�n inicial.</li>
     *  </ul>
     * </p>
     * @return instancia del EJB
     * @since 1.6
     */ 
    private Handle getInstanceServMisc() {
    	try{
    		if (servMiscHandle==null){
    			InitialContext ic = JNDIConfig.getInitialContext();
    			ServiciosMiscelaneosHome homeMisc= (ServiciosMiscelaneosHome)ic.lookup("wcorp.serv.misc.ServiciosMiscelaneos");
    			servMiscHandle = ((ServiciosMiscelaneos)homeMisc.create()).getHandle();
    		}
    	} catch (NamingException e) {
    		if(logger.isEnabledFor(Level.ERROR)){ logger.error("[getInstanceServMisc]::NamingException["+e.toString()+"]"); }
    	} catch (RemoteException e) {
    		if(logger.isEnabledFor(Level.ERROR)){ logger.error("[getInstanceServMisc]::RemoteException["+e.toString()+"]"); }
    	} catch (CreateException e) {
    		if(logger.isEnabledFor(Level.ERROR)){ logger.error("[getInstanceServMisc]::CreateException["+e.toString()+"]"); }
    	}
    	return servMiscHandle;
    }
    
    // Metodos Get de Atributos
    public String getFullName() {
       if(tipoUsuario != null && tipoUsuario.trim().equals("E")){
    		return apellidoPaterno + apellidoMaterno + nombres;
    	}
         else{
    		return nombres + " " + apellidoPaterno + " " + apellidoMaterno; 
    	     }
	}
    /**
     * M�todo que retorna los nombres del cliente
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *      <li>1.0 06/03/2012, Eduardo Villagr�n Morales (Imagemaker)): versi�n inicial</li>
     *  </ul>
     * </p>
     * @return nombres del cliente
     * @since 1.6
     */
    public String getNombres() {
		return nombres;
	}

    /**
	 * M�todo que asigna los nombres del cliente
	 * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *      <li>1.0 06/03/2012, Eduardo Villagr�n Morales (Imagemaker)): versi�n inicial</li>
     *  </ul>
     * </p>
	 * @param nombres Los nombres a asignar
	 * @since 1.6
	 */
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	/**
     * M�todo que retorna apellido paterno del cliente
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *      <li>1.0 06/03/2012, Eduardo Villagr�n Morales (Imagemaker)): versi�n inicial</li>
     *  </ul>
     * </p>
     * @return apellido paterno del cliente
     * @since 1.6
     */
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	/**
	 * M�todo que asigna apellido paterno del cliente
	 * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *      <li>1.0 06/03/2012, Eduardo Villagr�n Morales (Imagemaker)): versi�n inicial</li>
     *  </ul>
     * </p>
	 * @param apellidoPaterno El apellido paterno a asignar
	 * @since 1.6
	 */
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	/**
     * M�todo que retorna apellido materno del cliente
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *      <li>1.0 06/03/2012, Eduardo Villagr�n Morales (Imagemaker)): versi�n inicial</li>
     *  </ul>
     * </p>
     * @return apellido materno del cliente
     * @since 1.6
     */
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	/**
	 * M�todo que asigna apellido materno del cliente
	 * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *      <li>1.0 06/03/2012, Eduardo Villagr�n Morales (Imagemaker)): versi�n inicial</li>
     *  </ul>
     * </p>
	 * @param apellidoMaterno El apellido materno a asignar
	 * @since 1.6
	 */
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

    public String getFullRut() {
	    return StrUtl.formatMonto(rut) + "-" + digitoVerif;
	}

    public long getRut() {
	    return rut;
	}

    public char getDigito() {
	    return digitoVerif;
	}

    public String getDireccion() {
	    return direccion;
	}

    public String getFono() {
	    return fono;
	}

    public String getEmail() {
	    return email;
	}

    /**
     * M�todo que obtiene el listado de cuentas.
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *   <li>1.0 ??/??/????, ????. (????): Versi�n inicial.</li>
     *   <li>1.1 21/06/2011, Jorge G�mez O. (SEnTRA): Se agrega invocaci�n al m�todo getInstanceServCtas.</li>
     *  </ul>
     * </p>
     * @return listado de cuentas
     * @throws TuxedoException
     * @throws CuentasException
     * @throws GeneralException
     * @throws RemoteException
     * @since 1.0
     */ 
    public ListaCuentas getCuentas() throws TuxedoException, CuentasException, GeneralException, RemoteException {
    	servCtasHandle = getInstanceServCtas();
    	return ((ServiciosCuentas) servCtasHandle.getEJBObject()).cuentasPorRut(rut, digitoVerif);
	}

    /**
     * M�todo que obtiene los datos b�sicos en los sistemas de Acciones
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *   <li>1.0 ??/??/????, ????. (????): Versi�n inicial.</li>
     *   <li>1.1 21/06/2011, Jorge G�mez O. (SEnTRA): Se agrega invocaci�n al m�todo getInstanceServAcc.</li>
     *  </ul>
     * </p>
     * @return datos b�sicos de Acciones
     * @throws TuxedoException
     * @throws AccionesException
     * @throws GeneralException
     * @throws RemoteException
     * @since 1.0
     */
    public wcorp.serv.acciones.DatosCliente getDatosAcciones()
            throws TuxedoException, AccionesException, GeneralException, RemoteException {
    	servAccHandle = getInstanceServAcc();
        if (datosBasicosAcciones==null) {
	       datosBasicosAcciones = ((ServiciosAcciones)servAccHandle.getEJBObject()).accOrdBasCliNet(rut);
	    }
	    return datosBasicosAcciones;
	}

    /**
     * M�todo que obtiene los datos de cuentas corrientes de un cliente
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *   <li>1.0 ??/??/????, ????. (????): Versi�n inicial.</li>
     *   <li>1.1 21/06/2011, Jorge G�mez O. (SEnTRA): Se agrega invocaci�n al m�todo getInstanceServCtas.</li>
     *  </ul>
     * </p>
     * @return listado de cuentas corrientes
     * @throws TuxedoException
     * @throws CuentasException
     * @throws GeneralException
     * @throws RemoteException
     * @since 1.0
     */
    public ListaCuentas getCuentasCtes() throws TuxedoException, CuentasException, GeneralException, RemoteException {
    	servCtasHandle = getInstanceServCtas();
    	return ((ServiciosCuentas) servCtasHandle.getEJBObject()).cuentasCorrientesPorRut(rut, digitoVerif);
	}

    /**
     * M�todo que obtiene los datos de cuentas primas de un cliente
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *   <li>1.0 ??/??/????, ????. (????): Versi�n inicial.</li>
     *   <li>1.1 21/06/2011, Jorge G�mez O. (SEnTRA): Se agrega invocaci�n al m�todo getInstanceServCtas.</li>
     *  </ul>
     * </p>
     * @return listado de cuentas primas
     * @throws TuxedoException
     * @throws CuentasException
     * @throws GeneralException
     * @throws RemoteException
     * @since 1.0
     */
    public ListaCuentas getCuentasPrimas()
            throws TuxedoException, CuentasException, GeneralException, RemoteException {
    	servCtasHandle = getInstanceServCtas();
    	return ((ServiciosCuentas) servCtasHandle.getEJBObject()).cuentasPrimasPorRut(rut, digitoVerif);
	}

   /**
     * Setea Informaci�n B�sica del Cliente
     * 
     * Registro de versiones:<ul>
     *
     * <li>1.0 (xx/xx/xxxx,                       (xxxxx)): versi�n inicial          
     * <li>1.1 (30/05/2005, Esteban Landaeta D�az (SEnTRA)): Se agrega asignaci�n a nuevo atributo perfilTbanc cuando el cliente es Tbanc.
     * <li>1.2 07/01/2013 Juan Jose Buendia(BCI): Se Se utiliza variable goblal para la obtencion y seteo de informacion de cliente.
     * </ul>
     *
     */
    public void setDatosBasicos() throws TuxedoException, ClientesException, GeneralException, RemoteException {

        if (this.apellidoPaterno==null) {
            // Se hace la primera vez, no m�s
        	
        	datosBasicosCliente = ((ServiciosCliente) servCliHandle.getEJBObject()).datosBasicos(rut, digitoVerif);
        	
            this.nombres=datosBasicosCliente.nombre.trim();
            this.apellidoPaterno=datosBasicosCliente.apellidoPaterno.trim();
            this.apellidoMaterno=datosBasicosCliente.apellidoMaterno.trim();
            this.fono=datosBasicosCliente.fono;
            this.email=datosBasicosCliente.email;
            this.direccion=datosBasicosCliente.domicilio;
            this.tipoUsuario = datosBasicosCliente.tipoUsuario;
            this.tipoBanca= datosBasicosCliente.tipoBanca;
            this.rentaLiquida= datosBasicosCliente.rentaLiquida;
            this.segmento= datosBasicosCliente.segmento;
            this.periodoSegmento= datosBasicosCliente.periodoSegmento;
            this.indConvenio= datosBasicosCliente.indConvenio;
            this.codSocioEconomico= datosBasicosCliente.codSocioEconomico;
            this.actividad= datosBasicosCliente.actividad;
            this.oficina = datosBasicosCliente.oficina;
            //this.tipoAtencion= ((ServiciosCliente) servCliHandle.getEJBObject()).getTipoAtencion(this.tipoBanca);
            this.codEjecutivo= datosBasicosCliente.codEjecutivo;
            if ("247".equals(this.oficina.trim())){
                try{
                    this.perfilTbanc = ((ServiciosCliente) servCliHandle.getEJBObject()).getPerfilTbanc(rut);
                }catch(Exception ex){
                    this.perfilTbanc = null;
                }
            }
        }
	}

    /**
     * M�todo que obtiene las operaciones compuestas de un cliente
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *   <li>1.0 ??/??/????, ????. (????): Versi�n inicial.</li>
     *   <li>1.1 21/06/2011, Jorge G�mez O. (SEnTRA): Se agrega invocaci�n al m�todo getInstanceServCueVin.</li>
     *  </ul>
     * </p>
     * @param  tipoUser tipo de usuario
     * @param  tiposOperaciones tipos de operaciones
     * @param  estado estado de las operaciones
     * @return listado de operaciones compuestas
     * @throws TuxedoException
     * @throws ClientesException
     * @throws GeneralException
     * @throws RemoteException
     * @since 1.0
     */
    public Operacion[] getOperacionesCompuestas(char tipoUser, String tiposOperaciones, String estado)
        throws TuxedoException, ClientesException, GeneralException, RemoteException
    {
      servCueVinHandle =getInstanceServCueVin();
      String objs[] = StrUtl.getLista(tiposOperaciones, "&");
      java.util.ArrayList lista = new java.util.ArrayList();
      for (int i = 0; i < objs.length; i++) {
        lista.add(componeOperaciones(((ServiciosCuentasVinculadas) servCueVinHandle.getEJBObject()).obtieneVinculadasComoOperaciones(String.valueOf(rut)), ((ServiciosCliente)servCliHandle.getEJBObject()).consultaOperaciones(rut, digitoVerif, tipoUser, null, "VIG")));
      }

        java.util.ArrayList operaciones = new java.util.ArrayList();
        for (java.util.Iterator it = lista.iterator(); it.hasNext(); ) {
          Operacion op[] = (Operacion[]) it.next();
          for (int j = 0; j < op.length; j++) {
            operaciones.add(op[j]);
          }
        }
      return (Operacion[])operaciones.toArray(new Operacion[0]);
    }

    /**
     * M�todo que obtiene las operaciones del cliente seg�n su rol
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *   <li>1.0 ??/??/????, ????. (????): Versi�n inicial.</li>
     *   <li>1.1 21/06/2011, Jorge G�mez O. (SEnTRA): Se agrega invocaci�n al m�todo getInstanceServCueVin.</li>
     *  </ul>
     * </p>
     * @param  tipoUser tipo de usuario
     * @return listado de operaciones
     * @throws TuxedoException
     * @throws ClientesException
     * @throws GeneralException
     * @throws RemoteException
     * @since 1.0
     */
    public Operacion[] getOperaciones(char tipoUser)
            throws TuxedoException, ClientesException, GeneralException, RemoteException {
    	servCueVinHandle =getInstanceServCueVin();
        return componeOperaciones(
            ((ServiciosCuentasVinculadas) servCueVinHandle.getEJBObject()).obtieneVinculadasComoOperaciones(String.valueOf(rut)),
					  ((ServiciosCliente) servCliHandle.getEJBObject()).consultaOperaciones(rut, digitoVerif, tipoUser));
	}

    /**
     * M�todo que obtiene las operaciones del cliente seg�n su rol, tipo de operaci�n y estado de la operaci�n
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *   <li>1.0 ??/??/????, ????. (????): Versi�n inicial.</li>
     *   <li>1.1 21/06/2011, Jorge G�mez O. (SEnTRA): Se agrega invocaci�n al m�todo getInstanceServCueVin.</li>
     *  </ul>
     * </p>
     * @param  tipoUser tipo de usuario
     * @param  tipoOp tipo de operaci�n
     * @param  estadoOp estado de la operaci�n
     * @return listado de operaciones
     * @throws TuxedoException
     * @throws ClientesException
     * @throws GeneralException
     * @throws RemoteException
     * @since 1.0
     */
    public Operacion[] getOperaciones(char tipoUser, String tipoOp, String estadoOp)
            throws TuxedoException, ClientesException, GeneralException, RemoteException {
    	servCueVinHandle =getInstanceServCueVin();
        return componeOperaciones(
            ((ServiciosCuentasVinculadas) servCueVinHandle.getEJBObject()).obtieneVinculadasComoOperaciones(String.valueOf(rut)),
					  ((ServiciosCliente) servCliHandle.getEJBObject()).consultaOperaciones(rut, digitoVerif, tipoUser, tipoOp, estadoOp));
	}

	    /**
	     * M�todo especialmente constru�do para Cuentas Vinculadas.
	     * Une dos listas de operaciones.
	     * @param nuevas
	     * @param data
	     * @return
	     */
    private Operacion[] componeOperaciones(Operacion nuevas[], Operacion data[]) {
	  int normales = (data!=null)?data.length:0;
	  int adicionales = (nuevas!=null)?nuevas.length:0;

	  
	  if(normales + adicionales <= 0)
	    return null;

	  Operacion newOps[] = new Operacion[normales + adicionales];
	  for(int k = 0; k < normales; k++)
	    newOps[k] = data[k];

	 
	  if(adicionales <= 0)
	    return newOps;

	    
        if (nuevas != null) {
	       
            for (int j= 0; j < nuevas.length; j++) {
		 
		newOps[normales] = nuevas[j].copiar();
		newOps[normales].tipoOperacion = nuevas[j].tipoOperacion;
		normales++;
	      }
	}

	  return newOps;
	}

	//M�todos de Vista Genral Cliente (Bci en Acci�n)

    //Obtiene informacion personal
    public RetornoTipCli getDatosBasicos()
        throws TuxedoException, ClientesException, GeneralException, RemoteException {
        return ((ServiciosCliente) servCliHandle.getEJBObject()).consultaAntecedentes(rut,digitoVerif);
    }

    //Obtiene Rentabilidad y productos
    public Rentabilidad getRentabilidad(long rut,char dv)
        throws TuxedoException, ClientesException, GeneralException, RemoteException {
        return ((ServiciosCliente) servCliHandle.getEJBObject()).consultaRentabilidad(rut,dv);
    }

    //Obtiene Procesos de negocio del cliente
    public ProcesoNegocio[] getProcesos(String CliCic)
        throws TuxedoException, ClientesException, GeneralException, RemoteException {
        return ((ServiciosCliente) servCliHandle.getEJBObject()).consultaProcesos(CliCic);
    }

    //Obtiene participaci�n en campaas
    public Campanas getCampanas(String CliCic)
        throws TuxedoException, ClientesException, GeneralException, RemoteException {
        return ((ServiciosCliente) servCliHandle.getEJBObject()).consultaCampanas(CliCic);
    }

    //Obtiene factores de riesgo

    public Riesgopar[] getRiesgo(String CliCic)
        throws TuxedoException, ClientesException, GeneralException, RemoteException {
        return ((ServiciosCliente) servCliHandle.getEJBObject()).consultaRiesgo(CliCic);
    }

    //Obtiene valores de riesgo
    public Riesgo getValoresRiesgo(String CliCic)
        throws TuxedoException, ClientesException, GeneralException, RemoteException {
        return ((ServiciosCliente) servCliHandle.getEJBObject()).consultaValoresRiesgo(CliCic);
    }

    /**
     * M�todo que obtiene las cuentas autorizadas para cargo del cliente
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *   <li>1.0 ??/??/????, ????. (????): Versi�n inicial.</li>
     *   <li>1.1 21/06/2011, Jorge G�mez O. (SEnTRA): Se agrega invocaci�n al m�todo getInstanceServCtas.</li>
     *  </ul>
     * </p>
     * @return listado de cuentas autorizadas para cargo
     * @throws TuxedoException
     * @throws ClientesException
     * @throws GeneralException
     * @throws RemoteException
     * @since 1.0
     */
    public String[] getCtasCargoAutorizMandato()
        throws TuxedoException, ClientesException, GeneralException, RemoteException {
    	servCtasHandle = getInstanceServCtas();
    	return ((ServiciosCuentas) servCtasHandle.getEJBObject()).getCtasCargoAutorizMandato(rut);
    }

    /**
     * M�todo que obtiene las cuentas autorizadas de abono para terceros
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *   <li>1.0 ??/??/????, ????. (????): Versi�n inicial.</li>
     *   <li>1.1 21/06/2011, Jorge G�mez O. (SEnTRA): Se agrega invocaci�n al m�todo getInstanceServCtas.</li>
     *  </ul>
     * </p>
     * @param  cuenta
     * @return listado de cuentas autorizadas de abono para terceros
     * @throws TuxedoException
     * @throws ClientesException
     * @throws GeneralException
     * @throws RemoteException
     * @since 1.0
     */
    public CtasAbonoTercAutorizadas[] getCtasAbonoAutoriz(String cta)
        throws TuxedoException, ClientesException, GeneralException, RemoteException {
    	servCtasHandle = getInstanceServCtas();
    	return ((ServiciosCuentas) servCtasHandle.getEJBObject()).getCtasAbonoAutorizMandato(rut, cta);
    }

    /**
     * M�todo que obtiene los datos b�sicos de un cliente BCI
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *   <li>1.0 ??/??/????, ????. (????): Versi�n inicial.</li>
     *   <li>1.1 21/06/2011, Jorge G�mez O. (SEnTRA): Se agrega invocaci�n al m�todo getInstanceServCtas.</li>
     *   <li>1.2 07/01/2013 Juan Jose Buendia(BCI): Se valida existencia del informacion de cliente antes
     *       Realizar una nueva consulta de informacion
     *  </ul>
     * </p>
     * @return datos b�sicos del cliente
     * @throws TuxedoException
     * @throws ClientesException
     * @throws GeneralException
     * @throws RemoteException
     * @since 1.0
     */
    public DatosBasicosCliente getDatosBasicosBCI()
        throws TuxedoException, ClientesException, GeneralException, RemoteException {
        // Se hace la primera vez, no m�s
    	if(datosBasicosCliente == null){    	
    	   servCtasHandle = getInstanceServCtas();
    	   datosBasicosCliente =  ((ServiciosCliente) servCliHandle.getEJBObject()).datosBasicos(rut, digitoVerif);
    	}    	
    	return datosBasicosCliente;
	}

    /**
* M�todo que consulta los antecedentes generales de un cliente desde la
* plataforma IBM
*
* @return Clase Object
* @since 26/10/2001
* @version 1.0
*/
public RetornoTipCli getDatosBasicosGenerales()
        throws TuxedoException, ClientesException, GeneralException, RemoteException{

		RetornoTipCli cliente=((ServiciosCliente) servCliHandle.getEJBObject()).consultaAntecedentesGenerales(rut,digitoVerif);
        tipoAtencion = ((ServiciosCliente) servCliHandle.getEJBObject()).getTipoAtencion(cliente.getDatosComun().TipoBca);
		
        if(cliente.TipoCliente.equals("P")){
            cliente.DatPersona.rentaFamiliar=cliente.DatPersona.rentaConyuge + cliente.DatPersona.Rentafijamasvariable;
            return cliente;
        } else {
            return cliente;
        }
    }

/**
* M�todo que consulta la Rentabilidad de los productos de un cliente en particular
* BD Bci en Accion
*
* @return Clase Object
* @since 29/10/2001
* @version 1.0
*/

public java.util.Vector getRentabilidadProducto(String cicCliente)
        throws TuxedoException, ClientesException, GeneralException, RemoteException {

        java.util.Vector rentProd = new java.util.Vector();

        rentProd.addElement(((ServiciosCliente) servCliHandle.getEJBObject()).consultaFechaRentabilidad());
        rentProd.addElement(((ServiciosCliente) servCliHandle.getEJBObject()).consultaRentabilidadProductos(cicCliente));
        return rentProd;

    }

    /**
    * M�todo que rescata el valor de Banco Principal de un cliente
    * BD Bci en Accion. Se realiza lo sgte:
    *       para tipoCliente Persona
*		   Si (Deuda BCI/Deuda SBif)>=50%)
    *			   BancoPrincipal=TRUE
    *		   sino
    *			   BancoPrincipal=FALSE
    *
    *       para tipoCliente Empresa
    *           Si (Relacion DeudaBCI y Deuda SBif)>50%)
    *		       Banco Principal=TRUE
    *          sino
    *               BancoPrincipal=FALSE
    *
    * @return boolean
    * @since 19/11/2003
    * @version 1.0
    */
    public boolean getBancoPrincipal(Vector rentabilidadProductos, String tipoCliente) {
        RentabilidadProd[] rtasProd= null;
        String glosa= "";
        long valor= 0;
        long valor_deudaBCI= 0, valor_deudaSuper= 0;
        float valor_divisionDeudas= 0;
        float valor_relacionDeudas= 0;
        String unidad= "";
        String codigo= "", codigo2= ""; //codigo y subcodigo en la tabla
        boolean bancoPrincipal= false;
        
        
        try {
            rtasProd= (RentabilidadProd[])rentabilidadProductos.elementAt(1);
            
            if (rtasProd.length > 0) {
                for (int i= 0; i < rtasProd.length; i++) {
                    codigo= (String)rtasProd[i].codigo;
                    codigo2= (String)rtasProd[i].codigo2;
                    glosa= (String)rtasProd[i].glosa;
                    unidad= rtasProd[i].unidad;
                    valor= rtasProd[i].valorRentabilidad;
                    
                    
                    
                    
                    
                    
                    if (codigo != null)
                        codigo= codigo.trim();
                    if (codigo2 != null)
                        codigo2= codigo2.trim();
                    if ("P".equals(tipoCliente)) //****cliente tipo Persona
                        { //if ("Deuda BCI (Banca Persona)".equalsIgnoreCase(glosa)){
                        if ("DEU".equalsIgnoreCase(codigo) && "BCP".equalsIgnoreCase(codigo2)) {
                            valor_deudaBCI= valor;
                        } else if ("DEU".equalsIgnoreCase(codigo) && "SBP".equalsIgnoreCase(codigo2)) {
                            //if ("Deuda SBIF (Banca Persona)".equalsIgnoreCase(glosa)){
                            valor_deudaSuper= valor;
                        }
                    } else //****cliente tipo Empresa
                        if ("REL".equalsIgnoreCase(codigo) && "SUP".equalsIgnoreCase(codigo2)) {
                            //if ("Relacion Deuda Bci v/s Deuda Sup(Total)".equals(glosa)){
                            valor_relacionDeudas= valor;
                            break;
                        }

                } //end del for

                //***calculando Banco Principal
                if ("P".equals(tipoCliente)) { //****cliente tipo Persona
                    if (valor_deudaSuper != 0)
		         valor_divisionDeudas=(Float.parseFloat(String.valueOf(valor_deudaBCI))/Float.parseFloat(String.valueOf(valor_deudaSuper)));//0..1
                    else if (valor_deudaBCI != 0)
                        bancoPrincipal= true;
			 if (valor_divisionDeudas>=0.5)
                        bancoPrincipal= true;
                } else {
		         if (valor_relacionDeudas>=50)
                        bancoPrincipal= true;
                }

                //***
                

            }
        } catch (Exception e) {
            
            return false;

        }

        return bancoPrincipal;

    }


    /**
    * @Obs:M�todo que consulta los protestos y aclaraciones de un cliente en particular
    * BD Bci en Accion
    *
    * @return Clase java.util.Vector
    * @since 18/11/2003
    * @version 1.0
    */

    public java.util.Vector getProtestosAclaraciones()
        throws TuxedoException, ClientesException, GeneralException, RemoteException {

        java.util.Vector protestosAcla= new java.util.Vector();

        protestosAcla.addElement(((ServiciosCliente) servCliHandle.getEJBObject()).consultaProtestosAclaraciones(getRut(), getDigito()));

        return protestosAcla;
    }

/** ------------------------------------------------------------------------------- /
	/   @Obs     : Solicita todos los saldos promedios de un cliente y los peridos      /
	/               que compreden esos valores.                                         /
    /   @autor   : Jos� Luis FLores L.                                                  /
    /   @since   : 31/10/2001                                                           /
    /   @version : 1.0                                                                  /
    /---------------------------------------------------------------------------------- /
	*/
    public java.util.Vector getSaldosPromedios(String cicCliente)
        throws wcorp.util.com.TuxedoException, wcorp.util.GeneralException, RemoteException {
        java.util.Vector saldosProm = new java.util.Vector();
        java.util.Vector auxSaldosProm = new java.util.Vector();
        java.util.Vector paso = new java.util.Vector();
//        return servCli.consultaSaldosPromedios(cicCliente);
        saldosProm = ((ServiciosCliente) servCliHandle.getEJBObject()).consultaSaldosPromedios(cicCliente);

        InfoRelevante[] info,auxInfo  = null;
        InfoRelevante  aux = null;
        InfoGlosaProductos[] infoGlosa = null;
        Periodo[] periodo = null;

        periodo   = (Periodo[])saldosProm.elementAt(0);
        infoGlosa = (InfoGlosaProductos[])saldosProm.elementAt(1);
        info    = (InfoRelevante[])saldosProm.elementAt(2);

        int total = info.length;
        int i,j,k;
        int existe = 0;
        if (infoGlosa != null){
                for(i=0;i<infoGlosa.length;i++){
                    auxInfo = new InfoRelevante[periodo.length];
                    for(j=0;j<info.length;j++){

                    if ((infoGlosa[i].codigo.equals(info[j].codigo))
                        && (infoGlosa[i].subCodigo.equals(info[j].subCodigo))) {
                        
                            for(k=0;k<periodo.length;k++){
                                
                                if(auxInfo[k]== null) {
                                    
                                    aux = new InfoRelevante();
                                    aux.codigo = "";
                                    aux.subCodigo = "";
                                    aux.codigoPeriodo = 0;
                                    aux.valor = 0;
                                    existe = 1;
                                }
                                if(periodo[k].codigo == info[j].codigoPeriodo){
                                    
                                    aux = new InfoRelevante();
                                    aux.codigo = info[j].codigo;
                                    aux.subCodigo = info[j].subCodigo;
                                    aux.codigoPeriodo = info[j].codigoPeriodo;
                                    aux.valor = info[j].valor;
                                    existe = 1;
                                }
                                if (existe == 1){
                                    auxInfo[k] = new InfoRelevante();
                                    auxInfo[k] = aux;
                                    existe = 0;
                                    aux =null;
                                }
                             }//cierra for k
                          }//cierra if

                     }//cierra  for j
                     paso.addElement(auxInfo);
                 }
            auxSaldosProm.addElement(periodo);
            auxSaldosProm.addElement(infoGlosa);
            auxSaldosProm.addElement(paso);
            saldosProm = null;
        }//cierra if
     return auxSaldosProm;
    }//cierra metodo

		/* *********************** Metodos del Modelo nuevo de cliente **************** */

		//Obtiene informacion de un cliente persona natural
		public ClientePersona getClientePersona()
        throws TuxedoException, ClientesException, GeneralException, RemoteException {
			return ((ServiciosCliente) servCliHandle.getEJBObject()).getClientePersona(rut, digitoVerif);
    }

		//Obtiene informacion basica de un cliente persona natural
		public ClientePersona getInfoBasicaPersona()
        throws TuxedoException, ClientesException, GeneralException, RemoteException {
			return ((ServiciosCliente) servCliHandle.getEJBObject()).getInfoBasicaPersona(rut, digitoVerif);
		}

		public wcorp.model.actores.cliente.Cliente getCliente()
        throws TuxedoException, ClientesException, GeneralException, RemoteException {
			wcorp.model.actores.cliente.Cliente cliente = ((ServiciosCliente) servCliHandle.getEJBObject()).getCliente(rut, digitoVerif);
			tipoAtencion = ((ServiciosCliente) servCliHandle.getEJBObject()).getTipoAtencion(cliente.tipoBanca);

        if (cliente instanceof ClientePersona) {
				//cliente.DatPersona.rentaFamiliar=cliente.DatPersona.rentaConyuge + cliente.DatPersona.Rentafijamasvariable;
				return cliente;
        } else {
				return cliente;
			}
		}

		//retorna la segmentacion del cliente
    public String segmentacionCliente() throws TuxedoException, ClientesException, GeneralException, RemoteException {
		  String segmentoCliente = "";
		  wcorp.model.actores.cliente.Segmentacion segmento = ((ServiciosCliente) servCliHandle.getEJBObject()).getSegmentacion(rut);
		  segmentoCliente = segmento.tipo;
		  return segmentoCliente;
		}

		// Setea Informaci�n B�sica del Cliente
    public void setInfoBasicaPersona() throws TuxedoException, ClientesException, GeneralException, RemoteException {
        if (this.apellidoPaterno == null) {
				// Se hace la primera vez, no m�s
				ClientePersona cliPer = ((ServiciosCliente) servCliHandle.getEJBObject()).getInfoBasicaPersona(rut, digitoVerif);

				this.nombres = cliPer.nombres.trim();
				this.apellidoPaterno = cliPer.apellidoPaterno.trim();
				this.apellidoMaterno = cliPer.apellidoMaterno.trim();
				this.fono = ((Direccion)cliPer.direcciones.get("direcciones")).telefono;
				this.direccion = ((Direccion)cliPer.direcciones.get("direcciones")).glosa;
				this.email = ((DireccionEmail)cliPer.direcciones.get("emails")).email;
				//this.tipoUsuario = db.tipoUsuario;
			}
		}

    /**
     * M�todo que valida el estado del mandato de un cliente
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *   <li>1.0 ??/??/????, ????. (????): Versi�n inicial.</li>
     *   <li>1.1 21/06/2011, Jorge G�mez O. (SEnTRA): Se agrega invocaci�n al m�todo getInstanceServCtas.</li>
     *  </ul>
     * </p>
     * @return verdadero si el estado del mandato es SI
     * @since 1.0
     */
    public boolean getMandatoCliente(){
    	String[] rutTitul = new String[1];
    	rutTitul[0] = new String();
    	rutTitul[0] = String.valueOf(rut) + String.valueOf(digitoVerif);
    	try{
    		servCtasHandle = getInstanceServCtas();
    		TitularMandato[] mandato = ((ServiciosCuentas) servCtasHandle.getEJBObject()).consultaMandatosTitulares(rutTitul);

    		if (mandato != null && mandato[0].getEstado().trim().toUpperCase().equals("SI"))
    			return true;
    		else
    			return false;

    	}catch(Exception e){
    		return false;
    	}
    }

    /**
     * M�todo que obtiene el ejecutivo reemplazante
     * <p>
     *  <b>Registro de versiones: </b>
     *  <ul>
     *   <li>1.0 ??/??/????, ????. (????): Versi�n inicial.</li>
     *   <li>1.1 21/06/2011, Jorge G�mez O. (SEnTRA): Se agrega invocaci�n al m�todo getInstanceServMisc.</li>
     *  </ul>
     * </p>
     * @param  ejecutivo
     * @return datos del ejecutivo reemplazante
     * @throws TuxedoException
     * @throws MiscelaneosException
     * @throws ParseException
     * @throws RemoteException
     * @since 1.0
     */
    public InformacionReemplazo getEjecutivoReemplazante(String ejecutivo)
    throws TuxedoException, MiscelaneosException, ParseException, RemoteException {
    	servMiscHandle = getInstanceServMisc();
    	InformacionReemplazo infRee = ((ServiciosMiscelaneos)servMiscHandle.getEJBObject()).getInformacionReemplazo(ejecutivo);

    	if (infRee.getEjecutivo() != null){
    		Colaboradores col = new Colaboradores();
    		Colaborador colab = col.getColaborador(infRee.getEjecutivo());
    		if (colab != null) {
    			infRee.setNombreEjecutivo(colab.getFullName());
    		}
    	}

    	return infRee;
    }

   /**
     * Obtiene la informaci�n del perfil del cliente Tbanc.
     * 
     * Registro de versiones:<ul>
     * <li>1.0 (30/05/2005, Esteban Landaeta D�az (SEnTRA)): versi�n inicial               
     * </ul>
     *
     * @since 1.3
     */
    public PerfilTbanc obtienePerfilTbanc()
            throws TuxedoException, ClientesException, GeneralException, RemoteException {
        return ((ServiciosCliente) servCliHandle.getEJBObject()).getPerfilTbanc(rut);
    }

   /**
     * Entraga la informaci�n del perfil del cliente Tbanc.
     * 
     * Registro de versiones:<ul>
     * <li>1.0 (30/05/2005, Esteban Landaeta D�az. (SEnTRA)): versi�n inicial               
     * </ul>     
     *
     * @since 1.3
     */
    public PerfilTbanc getPerfilTbanc(){
        return perfilTbanc;
    }

    /**
     * Entrega el valor de Deuda Total BCI de un cliente. Registro de versiones:
     * <ul>
     * <li>1.0 (12/12/2007, Daniel Zuniga Figueroa. (TInet)): versi�n inicial
     * </ul>
     * 
     * @param rut RUT del cliente.
     * @param rutDv Digito Verificador del RUT.
     * @return double, Valor Deuda Total BCI del cliente.
     * @throws TuxedoException
     * @throws GeneralException
     * @throws RemoteException
     * @since 1.4
     */
    public double getDeudaBci(long rut, char rutDv) throws TuxedoException, GeneralException, RemoteException {
    	return ((ServiciosCliente) servCliHandle.getEJBObject()).getDeudaBci(rut, rutDv);
    }
    
    /**
     * Este metodo fue dise�ado para obtener los datos basicos de un cliente y, setear estos datos en el objeto
     * Cliente siempre y cuando sea necesario. <br>
     * NOTA:
     * <ul>
     * <li>Este metodo es un hibrido entre los metodos {@link #setDatosBasicos()} y {@link #getDatosBasicos()}
     * replicando la logica de negocio existente y su manejo de excepciones , por lo cual, la invocacion de este
     * reemplaza la llamada de estos dos metodos mencionados anteriormente.
     * </ul>
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 20/07/2012 Miguel Quiroz H. (TINet): Version inicial.
     * <li>1.1 07/01/2013 Juan Jose Buendia(BCI): Se valida existencia del informacion de cliente antes
     *       Realizar una nueva consulta de informacion
     * </ul>
     * <p>
     * 
     * @return datos basicos asociados a un cliente.
     * @throws TuxedoException en caso de existir un problema al invocar el servicio
     * @throws ClientesException En caso de existir un problema al setear los datos basicos del cliente
     * @throws GeneralException En caso de existir un error General.
     * @throws RemoteException En caso de Existir un error en la invocacion remota
     * @since 1.7
     */
    public DatosBasicosCliente getDatosBasicosCliente() throws TuxedoException, ClientesException,
        GeneralException, RemoteException {
        logger.debug("Ingresando al metodo getDatosBasicosCliente");
        
        if (this.apellidoPaterno == null) {
        logger.debug("Antes de invocar al servicio de datos basicos");
        
        
           datosBasicosCliente = ((ServiciosCliente) servCliHandle.getEJBObject()).datosBasicos(
            rut, digitoVerif);
       
        
        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.debug("resultado obtenido desde el servicio --> " + datosBasicosCliente);
        }
       
            logger.debug("Iniciando seteo de atributos de Cliente");
            this.nombres = datosBasicosCliente.nombre.trim();
            this.apellidoPaterno = datosBasicosCliente.apellidoPaterno.trim();
            this.apellidoMaterno = datosBasicosCliente.apellidoMaterno.trim();
            this.fono = datosBasicosCliente.fono;
            this.email = datosBasicosCliente.email;
            this.direccion = datosBasicosCliente.domicilio;
            this.tipoUsuario = datosBasicosCliente.tipoUsuario;
            this.tipoBanca = datosBasicosCliente.tipoBanca;
            this.rentaLiquida = datosBasicosCliente.rentaLiquida;
            this.segmento = datosBasicosCliente.segmento;
            this.periodoSegmento = datosBasicosCliente.periodoSegmento;
            this.indConvenio = datosBasicosCliente.indConvenio;
            this.codSocioEconomico = datosBasicosCliente.codSocioEconomico;
            this.actividad = datosBasicosCliente.actividad;
            this.oficina = datosBasicosCliente.oficina;
            logger.debug("Antes de invocar al servicio para obtener tipo de atencion");
            this.tipoAtencion = ((ServiciosCliente) servCliHandle.getEJBObject()).getTipoAtencion(this.tipoBanca);
            if (logger.isEnabledFor(Level.DEBUG)) {
                logger.debug("Tipo de Atencion --> " + tipoAtencion);
            }
            this.codEjecutivo = datosBasicosCliente.codEjecutivo;
            if ("247".equals(this.oficina.trim())) {
                logger.debug("Es oficina TBANC");
                try {
                    this.perfilTbanc = ((ServiciosCliente) servCliHandle.getEJBObject()).getPerfilTbanc(rut);
                    if (logger.isEnabledFor(Level.DEBUG)) {
                        logger.debug("Perfil de TBANC --> " + tipoAtencion);
                    }
                }
                catch (Exception ex) {
                    logger.error("Error al obtener el perfil TBANC --> ", ex);
                    this.perfilTbanc = null;
                }
            }
        }
        logger.debug("final ejecucion metodo getDatosBasicosCliente");
        return datosBasicosCliente;
    }
    
    /**
     * Este metodo consulta datos generales asociados a un cliente especifico.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 22/08/2012 Miguel Quiroz H. (TINet): Version inicial.
     * </ul>
     * <p>
     * 
     * @return instancia del objeto RetornoTipCli, que contiene datos generales del Cliente.
     * @throws TuxedoException en caso de existir un problema al invocar el servicio.
     * @throws ClientesException En caso de existir un problema al setear los datos del cliente.
     * @throws RemoteException En caso de Existir un error en la invocacion remota.
     * @since 1.7
     */
    public RetornoTipCli consultaDatosGeneralesDeCliente() throws TuxedoException, ClientesException,
        RemoteException {
        logger.debug("Ingresando al metodo consultaDatosGeneralesDeCliente");
        logger.debug("Antes de Invocar a consultaDatosGeneralesDeCliente del EJB de ServicioCliente");
        return ((ServiciosCliente) servCliHandle.getEJBObject()).consultaDatosGeneralesDeCliente(rut, digitoVerif);
    }

}