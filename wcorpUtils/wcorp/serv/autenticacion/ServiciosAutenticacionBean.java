package wcorp.serv.autenticacion;

import java.rmi.RemoteException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import wcorp.bprocess.dirclientes.DireccionesProcessDelegate;
import wcorp.infraestructura.seguridad.autenticacion.adapter.SecurIdAdapter;
import wcorp.infraestructura.seguridad.autenticacion.to.OpcionalidadSegundaClaveTO;
import wcorp.model.actores.Cliente;
import wcorp.model.actores.DireccionClienteBci;
import wcorp.serv.autenticacion.dao.ServiciosAutenticacionDAO;
import wcorp.serv.seguridad.EstadoPin;
import wcorp.serv.seguridad.EstadoToken;
import wcorp.serv.seguridad.ListaConvenios;
import wcorp.serv.seguridad.SeguridadException;
import wcorp.serv.seguridad.ValidaPinCorp;
import wcorp.util.ErroresUtil;
import wcorp.util.GeneralException;
import wcorp.util.StringUtil;
import wcorp.util.TablaValores;
import wcorp.util.com.JNDIConfig;
import wcorp.util.com.JOLTPoolConnection;
import wcorp.util.com.TuxedoException;

import bea.jolt.pool.ApplicationException;
import bea.jolt.pool.DataSet;
import bea.jolt.pool.Result;
import bea.jolt.pool.servlet.ServletSessionPool;

import cl.bci.mensajeria.sdp.conector.ConectorServicioEnvioSDP;

/**
 * EJB con los m�todos de negocio del servicio de autenticaci�n
 * <p>
 * Registro de versiones:
 * <ul>
 * <li>1.0 16/05/2011 Jorge G�mez O. (SEnTRA), Hector Hernandez O. (SEnTRA): Versi�n inicial.
 * <li>1.1 15/09/2011, Gustavo L�pez S. (Orand): Se agrega el parametro canal al m�todo
 *  {@link #consultaOpcionalidadToken(long,String,String)}
 *         para identificar desde donde se esta realizando la consulta.
 * <LI>1.2 01/03/2012 Sergio Cuevas Diaz (Sentra): Se agrega el m�todo obtenerEmpresasPyme.
 * <li>1.3 01/08/2012 Rodrigo Rodr�guez H.(BCI):Migraci�n ACE SERVER - Authentication Manager. Se intervienen 
 *     los m�todos:<ul><li> {@link ServiciosAutenticacionBean#ejbCreate()}:Instancia adapter AM.
 *     <li>{@link ServiciosAutenticacionBean#obtieneEstadoToken(long, char)}:Entrega el estado del token.
 *     <li>{@link ServiciosAutenticacionBean#autenticaToken(long, char, String)}:Autentifica.
 *     </ul>
 * <li>1.4 06/12/2012 Christopher Finch U. (BCI): Se agrega opcionalidad inversa al m�todo
 *     {@link #consultaOpcionalidadToken(long,String,String)} y constante con la tabla de
 *     par�metros asociada.</li>
 * <li>1.5 19/04/2013 Eduardo Mascayano (TInet): Se env�a un correo HTML de aviso de clave bloqueada al cliente
 * cuando �ste ingresa 3 veces o m�s una clave internet err�nea en el m�todo 
 * {@link #checkPinCorporativo(String, long, char, String, String)}. Se crean los m�todos utilitarios:
 * {@link #obtenerNombreCliente(long, char)} y {@link #obtenerEmailCliente(long, char)}.
 * <li>1.6 02/07/2013 Carlos Cerda I. (Kubos): Se agrega l�gica de consulta de los estados del
 * Multipass hacia Tandem. Esto debido a los constantes problemas presentados en el login por la alta concurrencia
 * a la m�quina principal del Authentication Manager. Se intervienen 
 *     los m�todos:<ul>
 *     <li>{@link ServiciosAutenticacionBean#ejbCreate()}
 *     <li>{@link ServiciosAutenticacionBean#obtieneEstadoToken(long, char)}
 *     <li>{@link ServiciosAutenticacionBean#autenticaToken(long, char, String)}
 *     <li>{@link ServiciosAutenticacionBean#inicializaAdapterAM()}
 * 
 * <li>1.7 04/06/2013, Gonzalo Cofre Guzman (SEnTRA) - Se agrega el m�todo {@link 
 * #obtenerConveniosPorRutEmpresas(long)}.
 * <li>1.8 16/01/2014 Eduardo Villagr�n Morales (Imagemaker): Se agrega control de excepci�n cuando cliente
 *              no posee clave internet en {@link #checkPinCorporativo(String, long, char, String, String)}.
 * <li>1.9 24/04/2014 Math�as Fern�ndez (SEnTRA): Se realiza Optimizaci�n de log.             
 * <li>1.10 02/05/2014 Eduardo Villagr�n Morales (Imagemaker): Se elimina llamada a 'SegAnuBloClvRed' ya que no
 *              se emplea en {@link #checkPinCorporativo(String, long, char, String, String)}.
 * <li>1.11 30/06/2014 Rodrigo Pino G. (SEnTRA): Se agrega m�todo
 * 		{@link #consultaOpcionalidadSegundaClave(OpcionalidadSegundaClaveTO)} </li>
 * <li>1.12 30/06/2013 Yon Sing Sius. (ExperimentoSocial): Se agrega m�todo 
 * 		{@link #ingresoOpcionalidadSegundaClave(OpcionalidadSegundaClaveTO)} </li>
 *</ul>
 *
 * <p>
 * <b>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</b>
 * <p>
 */

public class ServiciosAutenticacionBean implements SessionBean {

    /**
     * Atributo que permite hacer log para trazabilidad del c�digo fuente.
     */
    private transient Logger log = (Logger) Logger.getLogger(ServiciosAutenticacionBean.class);

    /**
     * Atributo que representa sessionContext.
     */
    private SessionContext sessionContext;

    /**
     * Atributo que representa el PIN para el canal WAP de TBANC.
     */
    private JOLTPoolConnection joltPool;

    /**
     * Atributo que obtiene el nombre de la clase.
     */	
    private String ejbName = this.getClass().getName();

    /**
     * Atributo que representa el PIN para el canal TBANC.
     */
    private final String PIN_WEBTBANC = "100";

    /**
     * Atributo que representa el largo m�ximo de clave.
     */	
    private final int LARGO_CLAVE = 8;
    
    /**
     * Atributo que representa la posici�n inicial para obtener el dia.
     */ 
    private final int POSICION_INICIAL_DIA = 0;
    
    /**
     * Atributo que representa la posici�n final para obtener el dia.
     */ 
    private final int POSICION_FINAL_DIA = 4;
    
    /**
     * Atributo que representa la posici�n inicial para obtener el mes.
     */ 
    private final int POSICION_INICIAL_MES = 5;
    
    /**
     * Atributo que representa la posici�n final para obtener el mes.
     */ 
    private final int POSICION_FINAL_MES = 7;
    
    /**
     * Atributo que representa la posici�n inicial para obtener el a�o.
     */ 
    private final int POSICION_INICIAL_ANO = 8;
    
    /**
     * Atributo que representa la posici�n final para obtener el a�o.
     */ 
    private final int POSICION_FINAL_ANO = 10;
    
    /**
     * Atributo que representa el largo m�nimo de clave para cualquier clave distinta a TBANC.
     */ 
    private final int LARGO_MINIMO_CLAVES = 4;
    
    /**
     * Atributo que representa el largo m�nimo de clave para cualquier clave distinta a TBANC.
     */ 
    private final int LARGO_MINIMO_CLAVE = 6;
    
    /**
     * Atributo que representa la cantidad de intentos.
     */ 
    private final int CANTIDAD_INTENTOS = 3;
    
    /**
     * Atributo que representa el PIN para el canal WAP de TBANC.
     */ 
    private final String PIN_WAPTBANC = "111";
    
    /**
     * Nombre de la tabla de parametros de la consulta de opcionalidad multipass para m�viles.
     */
    private final String TABLA_OPCIONALIDAD_MULTIPASS = "canales_opcionalidad_inversa.parametros";

    /**
     * C�digo error clave bloqueada.
     */
    private final String COD_ERR_CLAVE_BLOQUEADA = "0432";
    
    /**
     * Atributo que permite hacer log para trazabilidad del c�digo fuente.
     */
    private static final String TABLA_SEGURIDAD = "Seguridad.parametros";

    /**
     * Identificador del tipo de direcci�n email particular del cliente.
     */
    private static final char TIPO_DIR_EMAIL = '7';

    /**
     * Identificador del tipo de direcci�n email particular.
     */
    private static final int COD_ENVIO_CORREO_OK = 1;
	
    /**
     * Identificador del c�digo cuando no tiene clave internet.
     */
    private static final String CODIGO_SIN_CLAVE_INTERNET = "SINCLAVEINTERNET";
   
    /**
     * M�todo para loggear.
     * @return log .
     */
    public Logger getLogger() {
        if (log == null) {
            log = Logger.getLogger(this.getClass());
        }
        return log;
    }
    /**
     * Adapter de plataforma AM.
     */
    private SecurIdAdapter securIdAdapter;

    public void ejbActivate() throws EJBException, RemoteException {
    }

    public void ejbPassivate() throws EJBException, RemoteException {
    }

    public void ejbRemove() throws EJBException, RemoteException {
    }

    /**
     * M�todo invocado por el contenedor para instanciar el EJB.
     *
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 ??/??/2000 Alejandro Ituarte: versi�n inicial.
     * <li>1.1 01/08/2012 Rodrigo Rodr�guez H.(BCI):Migraci�n ACE SERVER - Authentication Manager. Se instancia
     *     el adaptador para la plataforma AM.
     * <li>1.2 02/07/2013 Carlos Cerda I. (Kubos): Se retira la instancia del adaptador para la plataforma AM.
     * </ul>
     * <p>
     *
     * @throws RemoteException Error remoto.
     * @throws CreateException Error de instanciaci�n.
     * @since 1.0
     */
    public void ejbCreate() throws RemoteException, CreateException {
        try {
            joltPool = new JOLTPoolConnection();
        } 
        catch (javax.naming.NamingException ne) {
            throw new CreateException("No se logr� Conexi�n al Pool Manager de JOLT - " + ne);
        }
    }

    public void setSessionContext(SessionContext context) throws RemoteException {
        sessionContext = context;
    }

    /**
     * M�todo que realiza la validaci�n de PIN Corporativo en TANDEM.
     *
     * <b>Registro de versiones:</b>
     * <ul>
     * 	<li>1.0 16/05/2011 Jorge G�mez O. (SEnTRA): Se copia m�todo desde ServiciosSeguridadBean.
     * </ul>
     *
     * @param canal par�metro que representa el canal
     * @param rut par�metro que representa el rut del cliente
     * @param dig par�metro que representa el digito verificador del cliente
     * @param pin par�metro que representa la clave del cliente
     * @return resultado de la validaci�n
     * @throws wcorp.util.com.TuxedoException 
     * @throws wcorp.serv.seguridad.SeguridadException 
     * @since 1.0
     */
    public ValidaPinCorp checkPinCorporativo(String canal, long rut, char dig, String pin)
    throws wcorp.util.com.TuxedoException, wcorp.serv.seguridad.SeguridadException {
        return checkPinCorporativo(canal, rut, dig, pin, null);
    }

    /**
     * M�todo que realiza la validaci�n de PIN Corporativo en TANDEM.
     *
     * <b>Registro de versiones:</b>
     * <ul>
     * 	<li>1.0 16/05/2011 Jorge G�mez O. (SEnTRA): Se copia m�todo desde ServiciosSeguridadBean.
     * <li>1.1 19/04/2013 Eduardo Mascayano (TInet): Se env�a correo HTML de clave bloqueada al cliente
     * cuando ingresa 3 veces o m�s una clave internet err�nea o cuando el servicio retorna error de
     * calve boqueada.
     * <li>1.2 16/01/2014 Eduardo Villagr�n Morales (Imagemaker): Se agrega control de excepci�n cuando cliente
     *              no posee clave internet.
     * <li>1.9 02/05/2014 Eduardo Villagr�n Morales (Imagemaker): Se elimina llamada a 'SegAnuBloClvRed' ya que no
     *              se emplea.
     * </ul>
     *
     * @param canal par�metro que representa el canal.
     * @param rut par�metro que representa el rut del cliente.
     * @param dig par�metro que representa el digito verificador.
     * @param pin par�metro que representa la clave del cliente.
     * @param tarjeta par�metro que representa la tarjeta del cliente.
     * @return resultado de la validaci�n
     * @throws wcorp.util.com.TuxedoException 
     * @throws wcorp.serv.seguridad.SeguridadException 
     * @since 1.0
     */
    public ValidaPinCorp checkPinCorporativo(String canal, long rut, char dig, String pin, String tarjeta)
    throws wcorp.util.com.TuxedoException, wcorp.serv.seguridad.SeguridadException {
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[checkPinCorporativo][BCI_INI]["+rut+"]Canal ["+canal
             +"]tarjeta[" + tarjeta + "]");
        }
        // Obtenemos una sesi�n en Jolt
        ServletSessionPool sesion = joltPool.getSesion(ejbName);

        // Se llenan los parametros para llamar al servicio
        DataSet parametros = new DataSet();

        parametros.setValue("identCanal", canal);
        if (canal.equals(PIN_WEBTBANC)){

            if (pin.length() < LARGO_MINIMO_CLAVE || pin.length() > LARGO_CLAVE) {
                throw new SeguridadException("USERLOGIN");
            }
        }
        else
            if (pin.length() < LARGO_MINIMO_CLAVES || pin.length() > LARGO_CLAVE) {
                throw new SeguridadException("USERLOGIN");
            }

        parametros.setValue("claveCorporativa", pin);
        parametros.setValue("digitoVerificador", String.valueOf(dig));
        parametros.setValue("rut", String.valueOf(rut));

        if (tarjeta != null) {
            parametros.setValue("numeroTarjeta", tarjeta);
        }

        ValidaPinCorp data = null;
        Result resultado=null;

        try {

            resultado = sesion.call("SegValPinCorp", parametros, null);
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[checkPinCorporativo]["+rut+"]Resultado: ["
                        +resultado+"]");
            }

            // Se llena el objeto con el resultado
            data = new ValidaPinCorp();
            data.tipoUsuario=((String)resultado.getValue("tipoUsuario",0, null)).charAt(0);
            data.identInternaBCI = (String)resultado.getValue("identInternaBCI",0, null);
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[checkPinCorporativo]["+rut+"]tipoUsuario: ["
                        +data.tipoUsuario+"]identInternaBCI: ["+data.identInternaBCI+"]");
            }
            // Valida Correctitud de la autentificaci�n
            if (data.tipoUsuario == 'E') {
                // Hubo error
                if (data.identInternaBCI.trim().compareTo("")==0) {
                    throw new SeguridadException(CODIGO_SIN_CLAVE_INTERNET);
                }
                else {
                    // Se trata de user con pin corporativo
                    // A la tercera, s�lo se bloquear�
                    // Mientras, devuelve la fecha de Ultimo cambio en el cic
                    String fecha = data.identInternaBCI.trim();
                    fecha=fecha.substring(POSICION_INICIAL_ANO, POSICION_FINAL_ANO)+"/"
                    +fecha.substring(POSICION_INICIAL_MES, POSICION_FINAL_MES)+"/"
                    +fecha.substring(POSICION_INICIAL_DIA, POSICION_FINAL_DIA);
                    throw new SeguridadException("USERLOGIN", 
                            TablaValores.getValor("errores.codigos","AUT-cambioClave", "Desc") + fecha);
                }
            }
            else {
                if (data.tipoUsuario!='E') {
                    data.cambioClave = ((String)resultado.getValue("cambioClave", 0, null)).
                    compareTo("S") == 0? true : false;
                    data.primeraConexion = ((String)resultado.getValue("primeraConexion", 0, null))
                    .compareTo("S") == 0 ? true : false;
                    data.restriccionClave = ((String)resultado.getValue("restriccionClave", 0, null))
                    .compareTo("01") == 0 ? true : false;

                    //Para el proyecto de cambio de clave con validacion de PAN
                    data.setEsCuentaCorrentista(((String)resultado.getValue("ctaCte", 0, null))
                            .compareTo("S") == 0 ? true : false);

                    if (canal.equals(PIN_WEBTBANC) | canal.equals(PIN_WAPTBANC) ){
                        data.restriccionClave = false;
                    }
                }
            }
        }
        catch (ApplicationException e) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[checkPinCorporativo][ApplicationException][BCI_FINEX]["
                 +rut+"]Se produjo una Exception", e);
            }

            // Errores Aplicativos
            resultado = e.getResult();
            if (resultado.getApplicationCode() == 0) {
                
                String codError = (String) resultado.getValue("codigoError", 0, "DESC");
                if (codError.equals(COD_ERR_CLAVE_BLOQUEADA)) {
                    if (this.enviarEmailClaveBloqueada(rut, dig, canal) != COD_ENVIO_CORREO_OK) {
                        if (getLogger().isEnabledFor(Level.ERROR)) {
                             getLogger().error("[checkPinCorporativo][BCI_FINEX]["
                               +rut+"] Hubo un error al enviar el correo de Desbloqueo de clave.");
                        }
                    }
                    else {
                        if (getLogger().isDebugEnabled()) {
                             getLogger().debug("[checkPinCorporativo]["
                              +rut+"] El correo de Desbloqueo de clave fue enviado con �xito.");
                        }
                    }
                }
                
                //Error de Negocio Seguridad
                throw new wcorp.serv.seguridad.SeguridadException(
                        (String)resultado.getValue("codigoError", 0, "DESC"),
                        (String)resultado.getValue("descripcionError",0,"[Sin Informacion]"));
            }
            else {
                // Error Tuxedo
                throw new wcorp.util.com.TuxedoException(
                        (String)(resultado.getApplicationCode() == 1 
                                ? resultado.getValue("codigoError", 0, "DESC") : "TUX") );
            }
        }
        //Se agrega la captura de las SeguridadException, 
        //para no confundir a los clientes con el mensaje desplegado
        catch (SeguridadException e) {
            if(getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[checkPinCorporativo][SeguridadException][BCI_FINEX]["
                           +rut+"]SeguridadException: ",e);
            }            
            throw e;
        }
        //Se debe agregar la captura de esta excepcion para poder asociar la excepcion a una de seguridad
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[checkPinCorporativo][Exception][BCI_FINEX]["+rut+"]Exception: ",e);
            }

            throw new wcorp.serv.seguridad.SeguridadException("BADMSG", e.getMessage());
        }
        if(getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[checkPinCorporativo][BCI_FINOK]["+rut+"]Return: ["+data+"]");
        }
        return data;
    }

    /**
     * A partir del estado de la solicitud y del estado del dispositivo (ACE Server)
     * se determina un estado del dispositivo. Este estado ser� entregado al cliente
     * a trav�s de una interfaz apropiada.
     *
     * <b>Registro de versiones:</b>
     * <ul>
     * 	<li>1.0 16/05/2011 Jorge G�mez O. (SEnTRA): Se copia m�todo desde ServiciosSeguridadBean.
     * </ul>
     *
     * @param rut RUT del cliente
     * @param digito d�gito verificador del RUT del cliente
     * @return C�digo del estado, el cual debe ser interpretado a trav�s de la tabla Seguridad.parametros
     * @throws TuxedoException en caso de error.
     * @throws SeguridadException en caso de error.
     * @throws RemoteException en caso de error.
     * @since 1.0
     */
    public String determinaEstadoToken(long rut, char digito)
    throws TuxedoException, SeguridadException, RemoteException {

    	if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[determinaEstadoToken][BCI_INI]["+rut+"]Consultar� por solicitud de Token");
        }
        String estadoSolicitud = null;
        wcorp.serv.seguridad.EstadoToken estadoToken = null;

        //obtener estado de solicitud token
        estadoSolicitud = consultaSolicitudToken(rut, digito);

        if (!estadoSolicitud.equalsIgnoreCase("NOK") && !estadoSolicitud.equalsIgnoreCase("OK")) {
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[determinaEstadoToken]["+rut+"]Estado de solicitud no es v�lido");
            }
            throw new SeguridadException("ESPECIAL", "Servicio no disponible");
        }

        if ( estadoSolicitud.equalsIgnoreCase("NOK") ) {
            //No tiene solicitud
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[determinaEstadoToken]["+rut+"]No tiene solicitud");
            }
            return "TOKENST01";
        }
        if(getLogger().isDebugEnabled()){
            getLogger().debug( "[determinaEstadoToken]["+rut+"]Tiene solicitud");
        }

        //obtener estado de dispositivo token
        try {
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[determinaEstadoToken]["+rut+"]Se consultar� por estado de Token");
            }
            estadoToken = obtieneEstadoToken(rut, digito);

        }
        catch (GeneralException ge) {
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[determinaEstadoToken][GeneralException][BCI_FINEX]["+rut
                  +"]GeneralExcepci�n, Determinar� si usuario no tiene token",ge);
            }
            if (ge.getCodigo().trim().equalsIgnoreCase("TK05")){
                //Solicitud ingresada
                if(getLogger().isEnabledFor(Level.INFO)){
                    getLogger().info("[determinaEstadoToken]["+rut+"]Solicitud 'ingresada'");
                }
                return "TOKENST02";
            } 
            else {
                if(getLogger().isDebugEnabled()){
                    getLogger().debug( "[determinaEstadoToken]["+rut
                    	+"]Excepci�n no esperada: ["+ ge.getMessage()+"]CODIGO=<"+ge.getCodigo()+">");
                }
                throw new SeguridadException(ge.getFullMessage(),ge.getCodigo());
            }
        }
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[obtieneEstadoToken][Exception][BCI_FINEX]Rut: ["
                 +rut+"]ServiceException: ",e);
            }
            throw new wcorp.serv.seguridad.SeguridadException("TK02", e.getMessage());
        }
        if (!estadoToken.habilitado && estadoToken.newPin) {
            //Solicitud registrada
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[determinaEstadoToken][BCI_FINOK]["+rut+"]Solicitud 'registrada'");
            }
            return "TOKENST03";
        }

        if (estadoToken.habilitado && estadoToken.newPin) {
            //Solicitud verificada
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[determinaEstadoToken][BCI_FINOK]["+rut+"]Solicitud 'verificada'");
            }
            return "TOKENST04";
        }

        if (estadoToken.habilitado && !estadoToken.newPin) {
            //Token activo
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[determinaEstadoToken][BCI_FINOK]["+rut+"]Token 'activo'");
            }
            return "TOKENST05";
        }

        if (estadoToken.habilitado && !estadoToken.newPin && estadoToken.nextCodeStatus > 0) {
            //Token en estado incorrecto (bloqueado)
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info( "[determinaEstadoToken[BCI_FINOK]]["+rut
                		+"] Token 'bloqueado' por 'nextCodeStatus'");
            }
            return "TOKENST06";
        }

        if (!estadoToken.habilitado && !estadoToken.newPin) {
            //Token en estado incorrecto (bloqueado)
            if(getLogger().isEnabledFor(Level.INFO)){
                getLogger().info("[determinaEstadoToken][BCI_FINOK]["+rut+"] Token 'bloqueado'");
            }
            return "TOKENST07";
        }

        //Estado desconocido
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[determinaEstadoToken][BCI_FINOK]Rut: ["+rut+"] Token en estado desconocido");
        }
        return "TOKENST00";
    }

    /**
     * Obtiene el estado de un dispositivo Token asociado a un cliente
     *
     * <b>Registro de versiones:</b>
     * <ul>
     * 	<li>1.0 16/05/2011 Jorge G�mez O. (SEnTRA): Se copia m�todo desde ServiciosSeguridadBean.
     * <li>1.1 01/08/2012 Rodrigo Rodr�guez H.(BCI): Migraci�n ACE SERVER - Authentication Manager. Se interviene 
     *     el m�todo para que lea desde la tabla de par�metros <tt>authentication_manager</tt>, 
     *     clave:<tt>operacion</tt>  y propiedad <tt>am_habilitado</tt>. Si esta propiedad contiene el valor: 
     *     <tt>0</tt> operar� con el cliente en la nueva plataforma AM.<p>
     *     NOTA:<ul><li>Una vez puesta en producci�n la plataforma nueva, pasado un tiempo prudente, 
     *          debe intervenirse  el m�todo para eliminar: <ol><li>Flag para operar con la plataforma AM.
     *     <li><code> else </code> que s�lo ha quedado como estrategia de vuelta atr�s para seguir operando 
     *     con el ace server.</ol><li><b>Si la plataforma AM ha quedado operativa, no puede volver aplicarse 
     *     el switch de vuelta atr�s por severa inconsistencia de datos y tratarse de plataformas aplicativas 
     *     diferentes.</b>
     *     </ul> 
     * <li>1.3 02/07/2013 Carlos Cerda I.(Kubos): Se remueve la l�gica TUXEDO/ACE SERVER, solo se manejar� la 
     * l�gica hacia el Authentication Manager.
     * </ul>
     *
     * @param rut Rut del cliente
     * @param digito D�gito verificador del cliente
     * @return El estado del token
     * @throws wcorp.util.com.TuxedoException 
     * @throws wcorp.serv.seguridad.SeguridadException 
     * @since 1.0
     */
    public EstadoToken obtieneEstadoToken(long rut, char digito)
            throws wcorp.util.com.TuxedoException, wcorp.serv.seguridad.SeguridadException {

            if( securIdAdapter == null ){
            inicializaAdapterAM();
            }
            return securIdAdapter.obtieneEstadoToken( rut, digito );
        }
        
    /**
     * M�todo que consulta, si el cliente tiene una solicitud de multipass, si no tiene puede devolver una 
     * excepci�n como tambien un "NOK", para cualquier otro caso devuelve "OK"
     *
     * <b>Registro de versiones:</b>
     * <ul>
     * 	<li>1.0 16/05/2011 Jorge G�mez O. (SEnTRA): Se copia m�todo desde ServiciosSeguridadBean.
     * </ul>
     *
     * @param rut Rut del cliente
     * @param digitoVerificador D�gito Verificador del rut del cliente
     * @return resultado de la consulta solicitud de token
     * @throws wcorp.util.com.TuxedoException 
     * @throws wcorp.serv.seguridad.SeguridadException 
     * @since 1.0
     */
    public String consultaSolicitudToken(long rut,char digitoVerificador)
    throws wcorp.util.com.TuxedoException, wcorp.serv.seguridad.SeguridadException
    {
        ServletSessionPool sesion = joltPool.getSesion(ejbName);
        String respuesta = null;
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[consultaSolicitudToken][BCI_INI]["+rut+"]consulta de solicitud de token..");
        }
        DataSet parametros = new DataSet();
        parametros.setValue("rut"              , String.valueOf(rut));
        parametros.setValue("digitoVerificador", String.valueOf(digitoVerificador));
        
        Result resultado = null;

        try{
            resultado = sesion.call("SegConSolClaTok", parametros, null);
            respuesta = (String)resultado.getValue("codRespuesta", 0, null);
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[consultaSolicitudToken]["+rut
                 +"]resultado, SegConSolClaTok: ["+resultado+"]");
            }
            if (respuesta.trim().equals("NOK")){
                if(getLogger().isEnabledFor(Level.INFO)){
                    getLogger().info("[consultaSolicitudToken][BCI_FINOK]["+rut
                      +"]return NOK por defecto..");
                }
                return "NOK";
            }

        }
        catch (ApplicationException e){
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[consultaSolicitudToken][ApplicationException][BCI_FINEX]["+rut
                		+"]Se produjo una exception:",e);
            }
            resultado = e.getResult();
            if(getLogger().isEnabledFor(Level.ERROR)){
            	getLogger().error("[consultaSolicitudToken]["+rut
            			+"]ApplicationCode: ["+resultado.getApplicationCode()+"]");
            }
            if (resultado.getApplicationCode()==0){
                //Error de Negocio Seguridad
                if(resultado.getValue("codigoError", 0, "[Sin Codigo Error]").equals("0073")){
                    return "NOK";
                }
                throw new wcorp.serv.seguridad.SeguridadException(
                        (String)resultado.getValue("codigoError",0,"DESC"),
                        (String)resultado.getValue("descripcionError",0,"[Sin Informacion]"));

            }
            else{ 
                // Error Tuxedo
                throw new wcorp.util.com.TuxedoException(
                        (String)(resultado.getApplicationCode()==1 
                                ? resultado.getValue("codigoError",0,"DESC") : "TUX") );
            }
        }
        //Se debe agregar la captura de esta excepcion para poder asociar la excepcion a una de seguridad
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[checkPinCorporativo][Exception][BCI_FINEX]["
                     +rut+"]Exception:",e);
            }
            throw new wcorp.serv.seguridad.SeguridadException("BADMSG", e.getMessage());
        }
        //OK o NOK
        if(getLogger().isEnabledFor(Level.INFO)){
        	getLogger().info("[consultaSolicitudToken][BCI_FINOK]Rut: ["
              +rut+"]Consulta Token retorna OK");
        }
        return "OK";
    }

    /**
     * Obtenci�n del estado del PIN
     *
     * <b>Registro de versiones:</b>
     * <ul>
     * 	<li>1.0 16/05/2011 Jorge G�mez O. (SEnTRA): Se copia m�todo desde ServiciosSeguridadBean.
     * </ul>
     *
     * @param codigo a consultar
     * @param rut RUT del cliente
     * @param dv d�gito verificador del RUT del cliente
     * @param grupoCanal grupo canal
     * @return estado del PIN
     * @throws wcorp.util.com.TuxedoException 
     * @throws wcorp.serv.seguridad.SeguridadException 
     * @since 1.0
     */
    public EstadoPin estadoPin(String codigo, long rut, char dv, String grupoCanal)
    throws wcorp.util.com.TuxedoException, wcorp.serv.seguridad.SeguridadException
    {
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[estadoPin][BCI_INI]["+rut+"]");
        }
    	ServletSessionPool sesion = joltPool.getSesion(ejbName);
        EstadoPin respuesta = new EstadoPin();

        DataSet parametros = new DataSet();
        parametros.setValue("codigo"           , codigo);
        parametros.setValue("rut"              , String.valueOf(rut));
        parametros.setValue("digitoVerificador", String.valueOf(dv));
        parametros.setValue("idAgrupCanal"     , grupoCanal);

        if(getLogger().isDebugEnabled()){
        	getLogger().debug("[estadoPin]["+rut+"]Codigo ["+codigo
        	 +"]idAgrupCanal ["+ grupoCanal + "]");
        }

        Result resultado = null;

        try{
            resultado = sesion.call("SegEstPinPassc", parametros, null);
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[estadoPin]["+rut+"]Resultado, SegEstPinPassc: ["+resultado+"]");
            }
            respuesta.cantidadMov = new Integer((String)resultado.getValue("cantidadMov", 0, null)).intValue();

            if (respuesta.cantidadMov > 0){
                respuesta.idAgrupCanal   = (String)resultado.getValue("idAgrupCanal", 0, null);
                respuesta.clave          = (String)resultado.getValue("clave", 0, null);
                respuesta.estado         = (String)resultado.getValue("estado", 0, null);
                respuesta.motivo         = (String)resultado.getValue("motivo", 0, null);
                String fecModEst = (String)resultado.getValue("fechaModEstado", 0, null);
                respuesta.fechaModEstado   = new SimpleDateFormat("yyyyMMddhhmmss").
                parse(fecModEst != null && !fecModEst.trim().equals("")
                        ? fecModEst : "19700101000000", new ParsePosition(0));
                respuesta.estado2        = (String)resultado.getValue("estado2", 0, null);
                respuesta.indPrimeraConex  = (String)resultado.getValue("indPrimeraConex", 0, null);
                respuesta.indCambioClave   = (String)resultado.getValue("indCambioClave", 0, null);
                respuesta.indClaveTransit  = (String)resultado.getValue("indClaveTransit", 0, null);
                respuesta.ingresoErrDia    = (String)resultado.getValue("ingresoErrDia", 0, null);
                respuesta.ingresoErrAcum   = (String)resultado.getValue("ingresoErrAcum", 0, null);
                String fecUltIng = (String)resultado.getValue("fecUltimoIngreso", 0, null);
                respuesta.fecUltimoIngreso = new SimpleDateFormat("yyyyMMddhhmmss").
                parse(fecUltIng != null && !fecUltIng.trim().equals("")
                        ? fecUltIng : "19700101000000", new ParsePosition(0));
                String fecCreac = (String)resultado.getValue("fecCreacion", 0, null);
                respuesta.fecCreacion      = new SimpleDateFormat("yyyyMMddhhmmss").
                parse(fecCreac != null && !fecCreac.trim().equals("")
                        ? fecCreac : "19700101000000", new ParsePosition(0));
                String fecExp = (String)resultado.getValue("fecExpiracion", 0, null);
                respuesta.fecExpiracion    = new SimpleDateFormat("yyyyMMddhhmmss").
                parse(fecExp != null && !fecExp.trim().equals("")
                        ? fecExp : "19700101000000", new ParsePosition(0));
                String fecUltMod = (String)resultado.getValue("fecUltimaModif", 0, null);
                respuesta.fecUltimaModif   = new SimpleDateFormat("yyyyMMddhhmmss").
                parse(fecUltMod != null && !fecUltMod.trim().equals("")
                        ? fecUltMod : "19700101000000", new ParsePosition(0));
                respuesta.tipoUsuario      = (String)resultado.getValue("tipoUsuario", 0, null);
                respuesta.cicCliente       = (String)resultado.getValue("cicCliente", 0, null);
                respuesta.oficina        = (String)resultado.getValue("codigo", 0, null);
                respuesta.numSolicitud   = (String)resultado.getValue("numSolicitud", 0, null);
            }

            if ((respuesta.estado.equals("  ")) || (respuesta.estado.equals("01") 
                    && respuesta.estado2.equals("  ") && respuesta.motivo.equals("05")))
                respuesta.modalidad = 'N';

            else
                respuesta.modalidad = 'S';

            //Identificaci�n de las glosas de los estados de la clave
            if ("00".equals(respuesta.estado)){
                respuesta.setEstadoGlosa("Activa");
            }
            else if ("01".equals(respuesta.estado)){
                respuesta.setEstadoGlosa("Bloqueada");
            }
            else if ("02".equals(respuesta.estado)){
                respuesta.setEstadoGlosa("Restringida");
            }
            else if ("03".equals(respuesta.estado)){
                respuesta.setEstadoGlosa("Eliminada");
            }
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[estadoPin]["+rut+"]Estado: ["+respuesta.estado+"]");
            }
        }
        catch (ApplicationException e){
            // Errores Aplicativos
            resultado = e.getResult();
            if (resultado.getApplicationCode()==0){ 
                //Error de Negocio Seguridad
                if(getLogger().isEnabledFor(Level.ERROR)){
                    getLogger().error("[estadoPin][ApplicationException][BCI_FINEX]["+rut
                      +"] Error de Negocio ["+e.getMessage()+"]",e);
                }
                throw new wcorp.serv.seguridad.SeguridadException(
                        (String)resultado.getValue("codigoError",0,"DESC"),
                        (String)resultado.getValue("descripcionError",0,"[Sin Informacion]"));
            }
            else{ 
                // Error Tuxedo
                if(getLogger().isEnabledFor(Level.ERROR)){
                    getLogger().error("[estadoPin][TuxedoError][BCI_FINEX]["+rut+"]["
                            +e.getMessage()+"]",e);
                }
                throw new wcorp.util.com.TuxedoException(
                        (String)(resultado.getApplicationCode()==1 
                                ? resultado.getValue("codigoError",0,"DESC") : "TUX") );
            }
        }
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[estadoPin][BCI_FINOK]["+rut+"]Return: ["+respuesta+"]");
        }
        return respuesta;
    }

    /**
     * Permite consultar si el token se encuentra opcional para el login del banco enlinea, esto
     * significa que para un cliente con token este no se pedira al momento del login en el banco enlinea.
     *
     * <b>Registro de versiones:</b>
     * <ul>
     * 	<li>1.0 16/05/2011 Jorge G�mez O. (SEnTRA): Se copia m�todo desde ServiciosSeguridadBean.
     *	<li>1.1 15/09/2011, Gustavo L�pez S. (Orand): Se agrega el parametro canal al m�todo para 
     * identificar desde donde se esta realizando la consulta.
     * 	<li>2.0 06/11/2012 Christopher Finch U. (BCI): Se agrega opcionalidad inversa para canales m�viles,
     * debido a que estos canales, por defecto, no solicitan multipass como si se hace en la web.
     * </ul>
     *
     * @param rut del cliente
     * @param digitoVerificador del cliente
     * @param canal por el cual se consultara el estado
     * @return verdadero si el token esta opcional para el login
     * @exception TuxedoException en caso de error.
     * @exception SeguridadException en caso de error.
     * @exception RemoteException en caso de error.
     * @since 1.0
     */
    public boolean consultaOpcionalidadToken(long rut, String digitoVerificador, String canal) 
    throws TuxedoException, SeguridadException, RemoteException{

        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[consultaOpcionalidadToken]["+rut+"]Canal: ["+canal+"]");
        }
        
        ServletSessionPool sesion = joltPool.getSesion(ejbName);
        DataSet parametros = new DataSet();
        Result resultado = null;
        boolean respuesta;

        parametros.setValue("rut",""+rut);
        parametros.setValue("digitoVerificador",digitoVerificador);
        parametros.setValue("canal", canal);

        try{
            
            resultado = sesion.call("SegConSolClaTok",parametros,null);
            
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[consultaOpcionalidadToken]["+rut
                		+"]Se realiza el llamdo a 'SegConSolClaTok : ["+resultado+"]");
            }
            respuesta = ((String)resultado.getValue("estadoMost", 0, null)).trim().equals("SI");
            
            if(getLogger().isDebugEnabled()){
                getLogger().debug("[consultaOpcionalidadToken]["+rut+"]respuesta: ["+respuesta+"]");
            }
			respuesta = ((TablaValores.getValor(TABLA_OPCIONALIDAD_MULTIPASS, "canales", "valor").indexOf(canal) < 0) ? respuesta : !respuesta );
			
			if(getLogger().isDebugEnabled()){
                getLogger().debug("[consultaOpcionalidadToken]["+rut
                		+"]Respuesta: ["+respuesta+"]respuesta con opcionalidad invertida..");
            }

        }
        catch(ApplicationException e) {
            //Error Aplicativo
            resultado = e.getResult();
            if(getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[consultaOpcionalidadToken][ApplicationException][BCI_FINEX]["+rut
                  +"]ApplicationException: ["+resultado.getApplicationCode()+"]",e);
            }
            if (resultado.getApplicationCode() == 0){
                throw new SeguridadException(
                        (String)resultado.getValue("codigoError",0,"DESC"),
                        (String)resultado.getValue("descripcionError",0,"[Sin Informacion]"));
            }
            else {
                throw new TuxedoException(
                        (String)(resultado.getApplicationCode()==1 
                                ? resultado.getValue("codigoError",0,"DESC") : "TUX") );
            }
        }
        if(getLogger().isEnabledFor(Level.INFO)){
            getLogger().info("[consultaOpcionalidadToken][BCI_FINOK]["+rut+"]return: ["+respuesta+"]");
        }
        return respuesta;
    }

    /**
     * M�todo que obtiene el numero serial de un dispositivo Token asociado a un cliente
     *
     * <b>Registro de versiones:</b>
     * <ul>
     * 	<li>1.0 16/05/2011 Jorge G�mez O. (SEnTRA): Se copia m�todo desde ServiciosSeguridadBean.
     * <li>1.1 01/08/2012 Rodrigo Rodr�guez H.(BCI): Migraci�n ACE SERVER - Authentication Manager. Se interviene 
     *     el m�todo para que lea desde la tabla de par�metros <tt>authentication_manager</tt>, 
     *     clave:<tt>operacion</tt>  y propiedad <tt>am_habilitado</tt>. Si esta propiedad contiene el valor: 
     *     <tt>0</tt> operar� con el cliente en la nueva plataforma AM.<p>
     *     NOTA:<ul><li>Una vez puesta en producci�n la plataforma nueva, pasado un tiempo prudente, 
     *          debe intervenirse  el m�todo para eliminar: <ol><li>Flag para operar con la plataforma AM.
     *     <li><code> else </code> que s�lo ha quedado como estrategia de vuelta atr�s para seguir operando 
     *     con el ace server.</ol><li><b>Si la plataforma AM ha quedado operativa, no puede volver aplicarse 
     *     el switch de vuelta atr�s por severa inconsistencia de datos y tratarse de plataformas aplicativas 
     *     diferentes.</b>
     *     </ul>
     * <li>1.2 02/07/2013 Carlos Cerda I.(Kubos): Se remueve la l�gica TUXEDO/ACE SERVER, solo se manejar� la 
     * l�gica hacia el Authentication Manager.
     * </ul>
     *
     * @param rut Rut del cliente
     * @param digito D�gito verificador del cliente
     * @param clave Clave token
     * @return El n�mero serial del token
     * @throws wcorp.util.com.TuxedoException 
     * @throws wcorp.serv.seguridad.SeguridadException 
     * @since 1.0
     */
    public boolean autenticaToken(long rut, char digito, String clave)
            throws wcorp.util.com.TuxedoException, wcorp.serv.seguridad.SeguridadException {
            if( securIdAdapter == null ){
            inicializaAdapterAM();
            }
            if (log.isDebugEnabled())log.debug("[autenticaToken]: Inicio");
            return securIdAdapter.autenticaToken( rut, digito, clave );
        }
        
    /**
     * M�todo registra en BD el horario de ingreso del cliente
     *
     * <b>Registro de versiones:</b>
     * <ul>
     * 	<li>1.0 16/05/2011 Hector Hernandez O. (SEnTRA)
     * </ul>
     *
     * @param rut Rut del cliente
     * @param digito D�gito verificador del cliente
     * @param canal codigo canal de ingreso del cliente
     * @return resultado de la operacion
     * @throws Exception en caso de error.
     * @since 1.0
     */
    public boolean registraLogin(long rut, char digito, String canal)	throws Exception
    {
        if(getLogger().isEnabledFor(Level.INFO)) {
            getLogger().info("[registraLogin][BCI_INI]["+rut+"]Canal: ["+canal+"]");
        }
        try {
            ServiciosAutenticacionDAO dao = new ServiciosAutenticacionDAO();
            if(getLogger().isEnabledFor(Level.INFO)) {
                getLogger().info("[registraLogin][BCI_FINOK]["+rut+"]");
            }    
            return dao.registraLogin(rut,digito, canal);
        }
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[registraLogin][Exception][BCI_FINEX]["+rut+"]Exception:",e);
            }
            throw new Exception(e.getMessage());
        }
    }

    /**
     * M�todo consulta la fecha y hora del ultimo logeo del cliente.
     *
     * <b>Registro de versiones:</b>
     * <ul>
     * <li>1.0 16/05/2011 Hector Hernandez O. (SEnTRA)
     * </ul>
     *
     * @param rut Rut del cliente
     * @param digito D�gito verificador del cliente
     * @param canal codigo canal de ingreso del cliente
     * @return resultado de la operacion
     * @throws Exception en caso de error.
     * @since 1.0
     */
    public Date consultaUltimoLogin(long rut, char digito, String canal)	throws Exception
    {
        if (getLogger().isEnabledFor(Level.INFO)){
        	getLogger().info("[consultaUltimoLogin][BCI_INI]["+rut+"]Canal: ["+canal+"]");
        }
        try {
            ServiciosAutenticacionDAO dao = new ServiciosAutenticacionDAO();
            if (getLogger().isEnabledFor(Level.INFO)){
            	getLogger().info("[consultaUltimoLogin][BCI_FINOK]["+rut+"]");
            }
            return dao.consultaUltimoLogin(rut,digito, canal);
        }
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[consultaUltimoLogin][Exception][BCI_FINEX]["+rut+"]Exception:",e);
            }
            throw new Exception(e.getMessage());
        }
    }

    /**
     * M�todo que obtiene el listado de convenios para un tipo de empresa en particular.
     * <p>
     *
     * Registro de versiones:<UL>
     *
     * <LI>1.0 28/02/2012 Sergio Cuevas Diaz (Sentra): versi�n inicial.
     * <LI>2.0 18/10/2012 H�ctor Hern�ndez Orrego (Sentra): Se modifica la firma de m�todo eliminando par�metro
     * indicador, dado que no es necesario por nueva funcionalidad de m�todo que debe obtener todas las empresas 
     * asociadas al usuario. Se modifica el nombre del m�todo de acuerdo a la nueva funci�n. 
     * 
     * @param rutUsuario rut del usuario a consultar.
     * @param codigoOrigen codigo de origen donde se esta realizando la consulta (1 Extranet EXT 2 intranet INT).
     * @param indicador indica el tipo de empresa a consultar.
     * @return obtiene lista de convenios.
     * @throws GeneralException en caso de error.
     * @since 1.2
     * </UL><P>
     *
     */
    public ListaConvenios[] obtenerConveniosEmpresas(long rutUsuario, String codigoOrigen)
    throws GeneralException{
        if (getLogger().isEnabledFor(Level.INFO)){
        	 getLogger().info("[obtenerConveniosEmpresas][BCI_INI]["+rutUsuario
        	   +"]");
        }
    	try{
            ServiciosAutenticacionDAO servicioSeguridad = new ServiciosAutenticacionDAO();
            if (getLogger().isEnabledFor(Level.INFO)){
            	 getLogger().info("[obtenerConveniosEmpresas][BCI_FINOK]["+rutUsuario
            	   +"]return: ["+codigoOrigen+"]");
            }
            return servicioSeguridad.obtenerConveniosEmpresas(rutUsuario, codigoOrigen);
        }
        catch(Exception e){
            if (getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[obtenerConveniosEmpresas][Exception][BCI_FINEX]["
                  +rutUsuario+"]Exception: ", e);
            }
            throw new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }
    }

    /**
     * 
     * Carga las propiedades para el contenedor Authentication Manager.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 01/08/2012 Rodrigo Rodr�guez H.(BCI): versi�n inicial.
     * <li>1.1 01/07/2012 Rodrigo Rodr�guez H.(BCI): Se modifica el m�todo, eliminado
     *                    los flag de las tablas. 
     * </ul>
     * <p>
     * @throws SeguridadException TK00 Error en la inicializaci�n de securIdAdapter 
     *
     * @since 4.2
     */
    private void inicializaAdapterAM() throws SeguridadException {
    	try {
                securIdAdapter = new SecurIdAdapter( JNDIConfig.getInitialContext() );
            }
            catch ( Exception ex ) {
            	 if (getLogger().isEnabledFor(Level.ERROR)){
                     getLogger().error("[inicializaAdapterAM] No se instancia adapter AM:" , ex);
                 }
            throw new SeguridadException( "TK00", ErroresUtil.extraeStackTrace( ex ) );
        }
    }   
     /**
     * M�todo que obtiene el listado de convenios asociados a un rut de empresa.
     * <p>
     *
     * Registro de versiones:<UL>
     *
     * <LI>1.0 31/05/2013, Gonzalo Cofr� Guzm�n. (SEnTRA) - Versi�n inicial
     * 
     * @param rutEmpresa rut de la empresa a consultar.
     * @return obtiene lista de convenios por rut empresa.
     * @throws GeneralException en caso de error.
     * @since 1.2
     * </UL><P>
     *
     */
    public ListaConvenios[] obtenerConveniosPorRutEmpresas(long rutEmpresa)
    throws GeneralException{
    	 if (getLogger().isEnabledFor(Level.INFO)){
             getLogger().info("[obtenerConveniosPorRutEmpresas][BCI_INI]["+rutEmpresa+"]");
         }
    	try{
            ServiciosAutenticacionDAO servicioSeguridad = new ServiciosAutenticacionDAO();
            if (getLogger().isEnabledFor(Level.INFO)){
                 getLogger().info("[obtenerConveniosPorRutEmpresas][BCI_FINOK]["+rutEmpresa
                  +"]return: ["+servicioSeguridad.obtenerConveniosPorRutEmpresas(rutEmpresa)+"]");
            }  
            return servicioSeguridad.obtenerConveniosPorRutEmpresas(rutEmpresa);
        }
        catch(Exception e){
            if (getLogger().isEnabledFor(Level.ERROR)){
                getLogger().error("[obtenerConveniosPorRutEmpresas][Exception][BCI_FINEX]["
                   +rutEmpresa+"] Exception : ", e);
            }
            throw new GeneralException("ESPECIAL", ErroresUtil.extraeStackTrace(e));
        }
    } 
    /**
     * Env�a un correo HTML a los clientes que tienen su clave bloqueda.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 19/04/2013 Eduardo Mascayano (TInet): versi�n inicial.
     * </ul>
     * <p>
     * 
     * @param rut Rut del cliente.
     * @param dv D�gito verificador del cliente.
     * @param canal del cliente.
     * @return Valor que indica si el correo fue enviado con �xito (1) o fall� (0).
     * @since 1.5
     */
    private int enviarEmailClaveBloqueada(long rut, char dv, String canal) {

        if(getLogger().isEnabledFor(Level.INFO)){
        	getLogger().info("[enviarEmailClaveBloqueada][BCI_INI]["+rut+"]");
        }
        String nombreCompleto = "";
        String email = "";
        int resultado = 0;
       
        nombreCompleto = this.obtenerNombreCliente(rut, dv);
        email = this.obtenerEmailCliente(rut, dv);

        if (email != null && !email.equals("")) {

            String refTemplate = TablaValores.getValor(TABLA_SEGURIDAD, "EMAIL-DESBLOQ-CLAVE-INT-TEMPL",
                "TEMPLATE-" + canal);

            String loginEnterprise = TablaValores.getValor(TABLA_SEGURIDAD, "EMAIL-DESBLOQ-CLAVE-INT-DATOS",
                "LOGIN");
            String refContract = TablaValores.getValor(TABLA_SEGURIDAD, "EMAIL-DESBLOQ-CLAVE-INT-DATOS",
                "CONTRACT");
            String fromName = TablaValores.getValor(TABLA_SEGURIDAD, "EMAIL-DESBLOQ-CLAVE-INT-DATOS", "FROM-NAME-"
                + canal);
            String asunto = TablaValores.getValor(TABLA_SEGURIDAD, "EMAIL-DESBLOQ-CLAVE-INT-DATOS", "ASUNTO");

            if(getLogger().isDebugEnabled()){
            	getLogger().debug("[enviarEmailClaveBloqueada]["+rut+"]refTemplate: ["+refTemplate
            			+"]refContract: ["+refContract+"]fromName: ["+fromName
            			+"]asunto: ["+asunto+"]nombreCompleto: ["+nombreCompleto+"]email: ["+email+"]");
            }

            Map paramsMail = new HashMap();
            paramsMail.put("fromName:", fromName);
            paramsMail.put("to", email);
            paramsMail.put("subject", asunto);
            paramsMail.put("body", "");

            Map paramsTemplate = new HashMap();
            paramsTemplate.put("nombreCompleto", nombreCompleto);

            if(getLogger().isDebugEnabled()){
            	getLogger().debug("[enviarEmailClaveBloqueada]["+rut+"]Obtendiendo contexto EJB..");
            }
            String contexto = TablaValores.getValor("JNDIConfig.parametros", "cluster", "param");

            if(getLogger().isDebugEnabled()){
            	getLogger().debug("[enviarEmailClaveBloqueada]["+rut+"]Contexto: ["+contexto
            	  +"]Obteniendo instancia del servicio..");
            } 
            ConectorServicioEnvioSDP instance = new ConectorServicioEnvioSDP(contexto);
            
            if(getLogger().isDebugEnabled()){
            	getLogger().debug("[enviarEmailClaveBloqueada]["+rut
            	 +"]Invocando al m�todo del servicio para enviar correo simple..");
            }
            resultado = instance.sendMail(loginEnterprise, refContract, refTemplate, paramsMail, paramsTemplate);

            if (getLogger().isDebugEnabled()) {
                getLogger().debug("[enviarEmailClaveBloqueada]["+rut+"]Resutado de e-mail es: ["+resultado+"]");
            }
        }
        if(getLogger().isEnabledFor(Level.INFO)){
        	getLogger().info("[enviarEmailClaveBloqueada][BCI_FINOK]["+rut+"]Return: ["+resultado+"]");
        }
        return resultado;

    }
    
    /**
     * Obtiene el nombre completo del cliente.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 19/04/2013 Eduardo Mascayano (TInet): versi�n inicial.
     * </ul>
     * <p>
     * 
     * @param rut Rut del cliente.
     * @param dv D�gito verificador del cliente.
     * @return Nombre completo del cliente.
     * @since 1.5
     */
    private String obtenerNombreCliente(long rut, char dv) {
        
        if(getLogger().isEnabledFor(Level.INFO)){
        	getLogger().info("[obtenerNombreCliente][BCI_INI]["+rut+"]");
        }
        Cliente cliente = null;
        String nombreCompleto = "";

        try {
            cliente = new Cliente(rut, dv);
            cliente.setDatosBasicos();
            nombreCompleto = cliente.getFullName();
        }
        catch (RemoteException e) {
            if(getLogger().isEnabledFor(Level.ERROR)){
            	getLogger().error("[obtenerNombreCliente][RemoteException][BCI_FINEX]["+rut
            			+"]RemoteException al consultar el nombre del cliente",e);
            }
        }
        catch (NamingException e) {
            if(getLogger().isEnabledFor(Level.ERROR)){
            	getLogger().error("[obtenerNombreCliente][NamingException][BCI_FINEX]["+rut
            	  +"]NamingException al consultar el nombre del cliente",e);
            }
        }
        catch (CreateException e) {
            if(getLogger().isEnabledFor(Level.ERROR)){
            	getLogger().error("[obtenerNombreCliente][CreateException][BCI_FINEX]["+rut
            	 +"]CreateException al consultar el nombre del cliente",e);
            }
        }
        catch (GeneralException e) {
            if(getLogger().isEnabledFor(Level.ERROR)){
            	getLogger().error("[obtenerNombreCliente][GeneralException][BCI_FINEX]["+rut
            			+"] GeneralException al consultar el nombre del cliente");
            }
        }
        if(getLogger().isEnabledFor(Level.INFO)){
        	getLogger().info("[obtenerNombreCliente][BCI_FINOK]["+rut+"]Return: ["+nombreCompleto+"]");
        }

        return nombreCompleto;

    }
    
    
    /**
     * Obtiene el email particular del cliente.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 19/04/2013 Eduardo Mascayano (TInet): versi�n inicial.
     * </ul>
     * <p>
     * 
     * @param rut Rut del cliente.
     * @param dv D�gito verificador del cliente.
     * @return Email particular del cliente.
     * @since 1.5
     */
    private String obtenerEmailCliente(long rut, char dv) {

        if(getLogger().isEnabledFor(Level.INFO)){
        	getLogger().info("[obtenerEmailCliente][BCI_INI]["+rut+"]");
        }
    	String email = "";
        DireccionesProcessDelegate dirDelegate = new DireccionesProcessDelegate();

        try {
            DireccionClienteBci[] direcciones = dirDelegate.getAddressBci(rut);

            if (direcciones != null && direcciones.length > 0) {

                int i = 0;
                boolean encontrado = false;
                while (i < direcciones.length && !encontrado) {
                    if (direcciones[i].direccion != null && !direcciones[i].direccion.equals("")) {
                        if (direcciones[i].getTipoDireccion() == TIPO_DIR_EMAIL) {
                            email = direcciones[i].getDireccion();
                            encontrado = true;
                        }
                    }
                    i++;
                }
            }
        }
        catch (Exception e) {
            /* Se atrapa Exception ya que el m�todo de negocio existente lo lanza. */
        	if(getLogger().isEnabledFor(Level.ERROR)){
        		getLogger().error("[obtenerEmailCliente][Exception][BCI_FINEX]["+rut
        			+"]Exception al consultar el email del cliente: ",e);
        	}
        }
        if(getLogger().isEnabledFor(Level.INFO)){
        	getLogger().info("[obtenerEmailCliente][BCI_FINOK]["+rut+"]Return: ["+email+"]");
        }

        return email;
    }

    /**
     * Consulta si el cliente tiene (o no) opcional la segunda clave de autenticaci�n. Adem�s, si el dispositivo
     * de segunda clave consultado se encuentra en la tabla canales_opcionalidad_inversa.parametros, se invertir�
     * el resultado retornado.
     * <p>
     * Registro de versiones:
     * <ul>
     * <li>1.0 14/08/2013 Rodrigo Pino (SEnTRA): versi�n inicial.
     * </ul>
     * <p>
     *
     * @param opcionalidadSegundaClaveTO Estructura de un cliente para SafeSigner.
     * @return boolean true para indicar que el dispositivo de segunda clave se encuentra opcional.
     * @throws TuxedoException
     * @throws SeguridadException
     * @since 1.9
     */
    public boolean consultaOpcionalidadSegundaClave(OpcionalidadSegundaClaveTO opcionalidadSegundaClaveTO)
    throws TuxedoException, SeguridadException, RemoteException{

        if(getLogger().isDebugEnabled())
            getLogger().debug("[consultaOpcionalidadSegundaClave] Inicio " + opcionalidadSegundaClaveTO);
        
        ServletSessionPool sesion = joltPool.getSesion(ejbName);
        DataSet parametros = new DataSet();
        Result resultado = null;
        boolean respuesta;

        if(getLogger().isDebugEnabled()){
            getLogger().debug("[consultaOpcionalidadSegundaClave]::toString :"
                    + opcionalidadSegundaClaveTO.toString());
        }

        parametros.setValue("rut", "" + opcionalidadSegundaClaveTO.getRut());
        parametros.setValue("digitoVerificador", "" + opcionalidadSegundaClaveTO.getDv());
        parametros.setValue("canal", opcionalidadSegundaClaveTO.getCanal());
        parametros.setValue("identCanal", TablaValores.getValor(TABLA_SEGURIDAD
                , "Op_" + opcionalidadSegundaClaveTO.getIdSegundaClave(), "codigo"));
        try{

            if(getLogger().isDebugEnabled()){
                getLogger().debug("[consultaOpcionalidadSegundaClave] Antes de llamar a 'SegConSolClaTok'");
                getLogger().debug("[consultaOpcionalidadSegundaClave] identCanal: "
                        + TablaValores.getValor(TABLA_SEGURIDAD, "Op_"
                        + opcionalidadSegundaClaveTO.getIdSegundaClave(), "codigo") );
            }

            resultado = sesion.call("SegConSolClaTok", parametros, null);

            if(getLogger().isDebugEnabled()) {
                getLogger().debug("[consultaOpcionalidadSegundaClave] Despues de llamar a 'SegConSolClaTok'");
                getLogger().debug("[consultaOpcionalidadSegundaClave]::Resultado:("+resultado+")");
                getLogger().debug("[consultaOpcionalidadSegundaClave]::Se debe invertir la respuesta["
                        +(TablaValores.getValor(TABLA_OPCIONALIDAD_MULTIPASS, "dispositivos", "valor")
                                .indexOf(opcionalidadSegundaClaveTO.getIdSegundaClave()) >= 0));
            }

            respuesta = ((String)resultado.getValue("estadoMost", 0, null)).trim().equals("SI");

            if(getLogger().isDebugEnabled())
                getLogger().debug("[consultaOpcionalidadSegundaClave]::respuesta("+""+respuesta+")");

            if ((TablaValores.getValor(TABLA_OPCIONALIDAD_MULTIPASS, "dispositivos", "valor")
                  .indexOf(opcionalidadSegundaClaveTO.getIdSegundaClave()) < 0)) {
                respuesta = ((TablaValores.getValor(TABLA_OPCIONALIDAD_MULTIPASS, "canales", "valor")
                    .indexOf(opcionalidadSegundaClaveTO.getCanal()) < 0) ? respuesta : !respuesta );
            }

            if(getLogger().isDebugEnabled())
                getLogger().debug("[consultaOpcionalidadSegundaClave]::respuesta con " +
                        "opcionalidad invertida("+respuesta+")");
        }
        catch(ApplicationException e){
            //Error Aplicativo
            resultado = e.getResult();
            if(getLogger().isDebugEnabled())
                getLogger().debug("[consultaOpcionalidadSegundaClave]::APPLICATION CODE::"
                        + resultado.getApplicationCode());

            if (resultado.getApplicationCode() == 0){
                throw new SeguridadException(
                        (String)resultado.getValue("codigoError",0,"DESC"),
                        (String)resultado.getValue("descripcionError",0,"[Sin Informacion]"));
            }
            else {
                throw new TuxedoException(
                        (String)(resultado.getApplicationCode()==1
                                ? resultado.getValue("codigoError",0,"DESC") : "TUX") );
            }

        }
        catch (Exception e) {
            if(getLogger().isEnabledFor(Level.ERROR)) {
                getLogger().error("[consultaOpcionalidadSegundaClave] rut: " + opcionalidadSegundaClaveTO.getRut()
                        + " Exception: " + e);
            }
            throw new wcorp.serv.seguridad.SeguridadException("BADMSG", e.getMessage());
        }

        return respuesta;
	}

	/**
	 * Permite ingresar o eliminar la opcionalidad de solicitud de segunda clave al momento
	 * de hacer login.
	 *
	 * <p>
	 * Registro de versiones:<ul>
	 * <li>1.0  (07/10/2013, Yon Sing Sius(ExperimentoSocial)): versi�n inicial
	 * </ul>
	 * <p>
	 * @param opcionalidad indica si la clave se deja opcional o no.
	 * @exception wcorp.util.com.TuxedoException
	 * @exception wcorp.serv.seguridad.SeguridadException
	 * @since 1.8
	 */
	public void ingresoOpcionalidadSegundaClave(OpcionalidadSegundaClaveTO opcionalidad) 
			throws TuxedoException, SeguridadException, RemoteException{
		
		if(getLogger().isEnabledFor(Level.INFO)){
        	getLogger().info("[ingresoOpcionalidadSegundaClave][BCI_INI]["+opcionalidad+"]");
        }
		
		ServletSessionPool sesion = joltPool.getSesion(ejbName);
		DataSet parametros = new DataSet();
		Result resultado = null;
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

		parametros.setValue("rut", String.valueOf(opcionalidad.getRut()));
		parametros.setValue("digitoVerificador", String.valueOf(opcionalidad.getDv()));
		parametros.setValue("canal", String.valueOf(opcionalidad.getCanal()));
		parametros.setValue("fechaInicio",sdf.format(new Date()));
        parametros.setValue("identCanal", TablaValores.getValor(TABLA_SEGURIDAD
                ,"Op_" + opcionalidad.getIdSegundaClave(), "codigo")); 

		try{
			getLogger().debug("[ingresoOpcionalidadSegundaClave]::antes de llamar a 'SegInsOpcTok'");
			resultado = sesion.call("SegInsOpcTok",parametros,null);
			getLogger().debug("[ingresoOpcionalidadSegundaClave]::despues de llamar a 'SegInsOpcTok'");
			
			if(getLogger().isEnabledFor(Level.INFO)){
	        	getLogger().info("[ingresoOpcionalidadSegundaClave][BCI_FINOK]["+resultado.getApplicationCode()+"]");
	        }
		} catch(ApplicationException e) {
			//Error Aplicativo
			resultado = e.getResult();
			if(getLogger().isEnabledFor(Level.ERROR)){
				getLogger().error("[ingresoOpcionalidadToken]::APPLICATION CODE::" + resultado.getApplicationCode());
			}
			if (resultado.getApplicationCode() == 0){
				throw new SeguridadException ((String)resultado.getValue("codigoError",0,"DESC"),(String)resultado.getValue("descripcionError",0,"[Sin Informacion]"));
			} else {
				throw new TuxedoException ((String)(resultado.getApplicationCode()==1 ? resultado.getValue("codigoError",0,"DESC") : "TUX") );
			}
		}
	}
}